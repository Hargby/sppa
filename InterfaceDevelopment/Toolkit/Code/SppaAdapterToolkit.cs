﻿/////////////////////////////////////////////////////////////////////////////
//
//							  COPYRIGHT (c) 2020
//								HONEYWELL LTD.
//							  ALL RIGHTS RESERVED
//
//  This software is a copyrighted work and/or information protected as a
//  trade secret. Legal rights of Honeywell Ltd. in this software is distinct
//  from ownership of any medium in which the software is embodied. Copyright
//  or trade secret notices included must be reproduced in any copies
//  authorised by Honeywell Ltd.
//
/////////////////////////////////////////////////////////////////////////////
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GenericParsing;
using System.Collections;

namespace SppaAdapter_Toolkit
{
    /// <summary>
    /// This is a simple toolkit that takes a formatted input file and generates a IOMap file, .dcsAdapter file, and a blank IC file
    /// </summary>
    public partial class SppaAdapterToolkit : Form
    {
        bool _inputFileLoaded = false;
        private bool _outputFolderLoaded = false;
        private bool _dllLoaded = false;
        private bool _writeIomapFailed = false;
        private bool _writeRangeFailed = false;
        private StreamWriter _iomapWriter = null;
        private StreamWriter _rangeWriter = null;
        private StringBuilder _strMsg = new StringBuilder();
        private List<CustomProperties> _properties = new List<CustomProperties>();
        private bool _errorsFound = false;
        private StreamWriter _errorLogWriter = null;
        private bool _logFileValid = false;

        /// <summary>
        /// This is the default constructor
        /// </summary>
        public SppaAdapterToolkit()
        {
            InitializeComponent();
            SetSensitivity();
        }
        /// <summary>
        /// Get the input file with the I/O points and display the file name in the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _bnInputFile_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _dlgInputFile.ShowDialog())
            {
                _txtInputFile.Text = _dlgInputFile.FileName;
                _inputFileLoaded = true;
            }
            SetSensitivity();
        }
        /// <summary>
        /// When all conditions are satisfied then enable the translation button
        /// </summary>
        private void SetSensitivity()
        {
            if (_dllLoaded && _inputFileLoaded && _outputFolderLoaded && (_txtDcsAdapterName.Text != ""))
            {
                _bnTranslate.Enabled = true;
            }
            else
            {
                _bnTranslate.Enabled = false;
            }
        }
        /// <summary>
        /// Get the folder where the generated files will be placed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _bnOutputFolder_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _dlgOutputFolder.ShowDialog())
            {
                _txtOutputFolder.Text = _dlgOutputFolder.SelectedPath;
                _outputFolderLoaded = true;

                // If the properties file is found in the output folder load it automatically
                string strPropFile = $"{_txtOutputFolder.Text}\\Properties.txt";
                FileInfo fi = new FileInfo(strPropFile);
                if (fi.Exists)
                {
                    PopulatePropertyGrid(strPropFile);
                }
            }
            SetSensitivity();
        }
        /// <summary>
        /// Get the specific adapter and path that will be used
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _bnAdapterDll_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _dlgAdapterDll.ShowDialog())
            {
                _txtAdapterDll.Text = _dlgAdapterDll.FileName;
                _dllLoaded = true;
            }
            SetSensitivity();
        }
        /// <summary>
        /// Perform the translation.  Note that if you want to put your own logic for the translation then simply
        /// replace this logic with your own to parse the input file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _bnTranslate_Click(object sender, EventArgs e)
        {
            int index = 1;
            string strDate = DateTime.Now.ToString("MMdd");
            string strTime = DateTime.Now.ToString("HHMMs");
            string logFile = string.Format("{0}\\Translation-{1}{2}.log", _txtOutputFolder.Text, strDate, strTime);
            object[] fields = null;
            ArrayList indices = new ArrayList();
            int pcIndex = -1;
            int apIndex = -1;
            int rackIndex = -1;
            int slotIndex = -1;
            int channelIndex = -1;
            int typeIndex = -1;
            int ifTypeIndex = -1;
            int tagIndex = -1;
            int euHiIndex = -1;
            int euLoIndex = -1;
            int rangeIndex = -1;
            int tagItemIndex = -1;
            int signalIndex = -1;
            int moduleDescriptionIndex = -1;
            int fumDcmIndex = -1;
            string connectionPoint;
            int computer;
            int applicationProcessor;
            int ioRack;
            int ioSlot;
            int ioChannel;
            int signalType;
            bool stat;

            _errorLogWriter = new StreamWriter(logFile);
            if (_errorLogWriter != null)
            {
                _logFileValid = true;
                _errorLogWriter.AutoFlush = true;
            }
            using (GenericParserAdapter gpa = new GenericParserAdapter(_txtInputFile.Text))
            {
                gpa.FirstRowHasHeader = true;
                gpa.ColumnDelimiter = ';';

                DataTable dt = gpa.GetDataTable();

                if (_logFileValid)
                {
                    _errorLogWriter.WriteLine($"Processing input File: !{_txtInputFile.Text}");
                }

                pcIndex = gpa.GetColumnIndex("PC");
                if (pcIndex == -1)
                {
                    _errorLogWriter.WriteLine("PC field not found");
                }
                apIndex = gpa.GetColumnIndex("AP");
                if (apIndex == -1)
                {
                    _errorLogWriter.WriteLine("AP field not found");
                }
                rackIndex = gpa.GetColumnIndex("RackNo");
                if (rackIndex == -1)
                {
                    _errorLogWriter.WriteLine("RackNo field not found");
                }
                slotIndex = gpa.GetColumnIndex("SlotNo");
                if (slotIndex == -1)
                {
                    _errorLogWriter.WriteLine("SlotNo field not found");
                }
                channelIndex = gpa.GetColumnIndex("ChannelNo");
                if (channelIndex == -1)
                {
                    _errorLogWriter.WriteLine("ChannelNo field not found");
                }
                typeIndex = gpa.GetColumnIndex("Type");
                if (typeIndex == -1)
                {
                    _errorLogWriter.WriteLine("Type field not found");
                }
                ifTypeIndex = gpa.GetColumnIndex("IFType");
                if (ifTypeIndex == -1)
                {
                    _errorLogWriter.WriteLine("IFType field not found");
                }
                tagIndex = gpa.GetColumnIndex("Tag");
                if (tagIndex == -1)
                {
                    _errorLogWriter.WriteLine("Tag field not found");
                }
                tagItemIndex = gpa.GetColumnIndex("Tag_Item");
                if (tagItemIndex == -1)
                {
                    _errorLogWriter.WriteLine("Tag_Item field not found");
                }
                signalIndex = gpa.GetColumnIndex("Sig");
                if (signalIndex == -1)
                {
                    _errorLogWriter.WriteLine("Sig field not found");
                }
                euLoIndex = gpa.GetColumnIndex("UL");
                if (euLoIndex == -1)
                {
                    _errorLogWriter.WriteLine("UL field not found");
                }
                euHiIndex = gpa.GetColumnIndex("LL");
                if (euHiIndex == -1)
                {
                    _errorLogWriter.WriteLine("LL field not found");
                }
                rangeIndex = gpa.GetColumnIndex("RANGE");
                if (rangeIndex == -1)
                {
                    _errorLogWriter.WriteLine("RANGE field not found");
                }
                moduleDescriptionIndex = gpa.GetColumnIndex("AFIModuleDesignation");
                if (moduleDescriptionIndex == -1)
                {
                    _errorLogWriter.WriteLine("AFIModuleDesignation field not found");
                }
                fumDcmIndex = gpa.GetColumnIndex("FUMDCM");
                if (fumDcmIndex == -1)
                {
                    _errorLogWriter.WriteLine("FUMDCM field not found");
                }
                fields = null;

                index = 0;
                foreach (DataRow row in dt.Rows)
                {
                    fields = row.ItemArray;

                    if ((Convert.ToString(fields[typeIndex]).ToUpper() == "AI") || (Convert.ToString(fields[typeIndex]).ToUpper() == "AO"))
                    {
                        stat = int.TryParse(Convert.ToString(fields[pcIndex]), out computer);
                        if (!stat)
                        {
                            _errorsFound = true;
                            _errorLogWriter.WriteLine($"Invalid PC information found for {fields[signalIndex]}: Value = {fields[pcIndex]}");
                            return;
                        }
                        // Convert the input objects
                        stat = int.TryParse(Convert.ToString(fields[apIndex]), out applicationProcessor);
                        if (!stat)
                        {
                            _errorsFound = true;
                            _errorLogWriter.WriteLine($"Invalid AP found for {fields[signalIndex]}: Value = {fields[apIndex]}");
                            return;
                        }
                        stat = int.TryParse(Convert.ToString(fields[rackIndex]), out ioRack);
                        if (!stat)
                        {
                            _errorsFound = true;
                            _errorLogWriter.WriteLine($"Invalid Rack found for {fields[signalIndex]}: Value = {fields[rackIndex]}");
                            return;
                        }
                        stat = int.TryParse(Convert.ToString(fields[slotIndex]), out ioSlot);
                        if (!stat)
                        {
                            _errorsFound = true;
                            _errorLogWriter.WriteLine($"Invalid Slot found for {fields[signalIndex]}: Value = {fields[slotIndex]}");
                            return;
                        }
                        stat = int.TryParse(Convert.ToString(fields[channelIndex]), out ioChannel);
                        if (!stat)
                        {
                            _errorsFound = true;
                            _errorLogWriter.WriteLine($"Invalid Channel found for {fields[signalIndex]}: Value = {fields[channelIndex]}");
                            return;
                        }
                        stat = int.TryParse(Convert.ToString(fields[ifTypeIndex]), out signalType);
                        if (!stat)
                        {
                            _errorsFound = true;
                            _errorLogWriter.WriteLine($"Invalid IFType found for {fields[signalIndex]}: Value = {fields[ifTypeIndex]}");
                            return;
                        }
                        connectionPoint = string.Format("PC{0:D2}AP{1:D3}R{2:D3}S{3:D3}C{4:D2}T{5:D1}", computer, applicationProcessor, ioRack, ioSlot, ioChannel, signalType);
                        // Write to the ranges file
                        WriteRangeLine(fields[tagItemIndex], fields[signalIndex], fields[euLoIndex], fields[euHiIndex], fields[rangeIndex], connectionPoint, fields[typeIndex]);
                    }

                    // Send the fields to WriteIOMapLine
                    WriteIOMapLine(fields[pcIndex], fields[apIndex], fields[rackIndex], fields[slotIndex], fields[channelIndex], fields[typeIndex], fields[ifTypeIndex], fields[tagIndex], fields[tagItemIndex], fields[signalIndex], fields[moduleDescriptionIndex], fields[fumDcmIndex], index);
                }

                CreateDcsAdapterFile();

                // Create the blank IC file
                FileInfo fi = new FileInfo($"{_txtOutputFolder.Text}\\Newly Configured Initial Conditions.sic");
                if (!fi.Exists)
                {
                    fi.Create();
                    fi = null;
                }

                // If there were errors inform the user
                if (_errorsFound)
                {
                    MessageBox.Show($"Translation completed with errors.  Open the log file {logFile} to fix the errors.");
                }
                else
                {
                    MessageBox.Show($"Translation completed successfully.");
                }
            }
        }
        /// <summary>
        /// Write the information for Analog Points
        /// </summary>
        /// <param name="tag">tag name</param>
        /// <param name="euLo">engineering units low value</param>
        /// <param name="euHi">engineering units high value</param>
        /// <param name="range">I/O type.  see switch for possible values</param>
        private void WriteRangeLine(object tagItem, object signal, object euLo, object euHi, object range, string connectionPoint, object signalType)
        {
            string strLine = string.Empty;
            string strEuLo;
            string strEuHi;
            string strSignalType;
            string tagName;
            int ioType;
            bool skipWrite = false;

            // If the IOMap has not been opened yet do it now
            if (_rangeWriter == null)
            {
                try
                {
                    _rangeWriter = new StreamWriter(string.Format("{0}\\{1}_ranges.txt", _dlgOutputFolder.SelectedPath, _txtDcsAdapterName.Text, false));
                    _rangeWriter.AutoFlush = true;
                    // Add the iomap header
                    strLine = "# TagName, EU Hi, EU Lo, I/O Type, ConnectionPoint, Tag Type";
                    _rangeWriter.WriteLine(strLine);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error opening range file: " + e.Message);
                    if (_logFileValid)
                    {
                        _errorLogWriter.WriteLine($"Error opening !{_txtDcsAdapterName.Text}_ranges.txt.  Error is !{e.Message}");
                    }
                    _writeRangeFailed = true;
                }
            }

            // If the file can't be written don't bother processing
            if (_writeRangeFailed)
            {
                return;
            }

            strEuLo = Convert.ToString(euLo);
            if (strEuLo == "")
            {
                strEuLo = "0.0";
            }
            strEuHi = Convert.ToString(euHi);
            if (strEuHi == "")
            {
                strEuHi = "100.0";
            }
            strSignalType = Convert.ToString(signalType);

            tagName = $"{tagItem}_{signal}";
            bool stat = int.TryParse(Convert.ToString(range), out ioType);
            if (!stat)
            {
                _errorsFound = true;
                _errorLogWriter.WriteLine($"Invalid range information found for {tagName}: ioType = {range.ToString()}");
                return;
            }

            if (stat && (ioType > 0))
            {
                switch (ioType)
                {
                    case 1:
                        strLine = $"{tagName},{strEuLo},{strEuHi},0-20mA,{connectionPoint},{strSignalType}";
                        break;
                    case 2:
                        strLine = $"{tagName},{strEuLo},{strEuHi},4-20mA,{connectionPoint},{strSignalType}";
                        break;
                    case 3:
                        strLine = $"{tagName},{strEuLo},{strEuHi},fieldValue,{connectionPoint},{strSignalType}";
                        break;
                    case 4:
                        strLine = $"{tagName},{strEuLo},{strEuHi},FlashingDeskTile,{connectionPoint},{strSignalType}";
                        break;
                    case 5:
                        strLine = $"{tagName},{strEuLo},{strEuHi},-20mA-20mA,{connectionPoint},{strSignalType}";
                        break;
                    default:
                        _errorsFound = true;
                        skipWrite = true;
                        _errorLogWriter.WriteLine($"Invalid iotype information found for {tagName}: ioType = {ioType}");
                        break;
                }

                if (!skipWrite)
                {
                    _rangeWriter.WriteLine(strLine);
                }
            }
        }
        /// <summary>
        /// Take the input line and format it into an IOMap entry
        /// </summary>
        /// <param name="pc">rmulatio computer number</param>
        /// <param name="ap">Application Processor number</param>
        /// <param name="rack">rack number</param>
        /// <param name="slot">slot number</param>
        /// <param name="channel">channel number</param>
        /// <param name="type">data type</param>
        /// <param name="ifType">i/o type bytes used</param>
        /// <param name="tag">the tag name</param>
        /// <param name="lineNumber">line number being processed</param>
        private void WriteIOMapLine(object pc, object ap, object rack, object slot, object channel, object type, object ifType, object tag, object tagItem, object signal, object description, object fumDcm, int lineNumber)
        {
            int computer;
            int applicationProcessor;
            int ioRack;
            int ioSlot;
            int ioChannel;
            int signalType;
            string ioType;
            string ioTagName;
            string fullTagName;
            bool stat;
            string iomapLine = string.Empty;
            string connectionPoint = string.Empty;

            if (_writeIomapFailed)
            {
                return;
            }

            // Convert the input objects
            ioTagName = Convert.ToString(tag);
            ioTagName = ioTagName.Trim();
            fullTagName = $"{tagItem}_{signal}_[{fumDcm}] -> {description}";
            fullTagName = fullTagName.Trim();
            stat = int.TryParse(Convert.ToString(pc), out computer);
            if (!stat)
            {
                _errorsFound = true;
                _errorLogWriter.WriteLine($"Invalid PC information found for {ioTagName}: Value = {pc.ToString()}");
                return;
            }
            // Convert the input objects
            stat = int.TryParse(Convert.ToString(ap), out applicationProcessor);
            if (!stat)
            {
                _errorsFound = true;
                _errorLogWriter.WriteLine($"Invalid AP found for {ioTagName}: Value = {ap.ToString()}");
                return;
            }
            stat = int.TryParse(Convert.ToString(rack), out ioRack);
            if (!stat)
            {
                _errorsFound = true;
                _errorLogWriter.WriteLine($"Invalid Rack found for {ioTagName}: Value = {rack.ToString()}");
                return;
            }
            stat = int.TryParse(Convert.ToString(slot), out ioSlot);
            if (!stat)
            {
                _errorsFound = true;
                _errorLogWriter.WriteLine($"Invalid Slot found for {ioTagName}: Value = {slot.ToString()}");
                return;
            }
            stat = int.TryParse(Convert.ToString(channel), out ioChannel);
            if (!stat)
            {
                _errorsFound = true;
                _errorLogWriter.WriteLine($"Invalid Channel found for {ioTagName}: Value = {channel.ToString()}");
                return;
            }
            stat = int.TryParse(Convert.ToString(ifType), out signalType);
            if (!stat)
            {
                _errorsFound = true;
                _errorLogWriter.WriteLine($"Invalid IFType found for {ioTagName}: Value = {ifType.ToString()}");
                return;
            }
            ioType = Convert.ToString(type);
            if (!stat)
            {
                _errorsFound = true;
                _errorLogWriter.WriteLine($"Invalid Type found for {ioTagName}: Value = {type.ToString()}");
                return;
            }

            // If the IOMap has not been opened yet do it now
            if (_iomapWriter == null)
            {
                try
                {
                    _iomapWriter = new StreamWriter(string.Format("{0}\\{1}_iomap.csv", _dlgOutputFolder.SelectedPath, _txtDcsAdapterName.Text, false));
                    _iomapWriter.AutoFlush = true;
                    // Add the iomap header
                    _iomapWriter.WriteLine("# USO IOMap version 2.0:  TagName,ConnectionName,TagAlias,TagDirection,TagPointType,Gain,Bias,TagDescription");
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error opening iomap file: " + e.Message);
                    if (_logFileValid)
                    {
                        _errorLogWriter.WriteLine($"Error opening !{_txtDcsAdapterName.Text}_iomap.csv.  Error is !{e.Message}");
                    }
                    _writeIomapFailed = true;
                }
            }
            
            // Form the connectionPoint
            connectionPoint = string.Format("PC{0:D2}AP{1:D3}R{2:D3}S{3:D3}C{4:D2}T{5:D1}", computer, applicationProcessor, ioRack, ioSlot, ioChannel, signalType);
            if (_iomapWriter != null)
            {
                // The format of the line is TagName (space or tab) TagType (space or tab) IODirection ConnectionPoint
                if ((ioType == "DI") || (ioType == "DO"))
                {
                    // W becomes r in the IOMap file
                    if (ioType == "DI")
                    {
                        // If there are 4 tokens then there is a connection tag in the input file
                        iomapLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", ioTagName, connectionPoint, "", "r", "INTEGER", "1.0", "0.0", fullTagName);
                    }
                    else
                   {
                        iomapLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", ioTagName, connectionPoint, "", "w", "INTEGER", "1.0", "0.0", fullTagName);
                    }
                }
                else
               {
                    // W becomes r in the IOMap file
                    if (ioType == "AI")
                   {
                        iomapLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", ioTagName, connectionPoint, "", "r", "FLOAT", "1.0", "0.0", fullTagName);
                    }
                    else
                   {
                        iomapLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", ioTagName, connectionPoint, "", "w", "FLOAT", "1.0", "0.0", fullTagName);
                    }
                }
                _iomapWriter.WriteLine(iomapLine);
            }
        }
        /// <summary>
        /// Create the DCSAdapter file and the blank IC file
        /// </summary>
        private void CreateDcsAdapterFile()
        {
            string property;
            string value;
            string fileName = string.Format("{0}\\{1}.dcsAdapter", _txtOutputFolder.Text, _txtDcsAdapterName.Text);
            FileInfo adapterFi = new FileInfo(fileName);
            bool createAdapter = true;

            // If the dcsAdapter file exists, make sure the user wants to overwrite it
            if (adapterFi.Exists)
            {
                string strError = "DCS Adapter file already exist in the output directory. Do you want to overwrite the existing file?";
                if (DialogResult.No == MessageBox.Show(strError, "GAF Toolkit", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    createAdapter = false;
                }
            }

            // Create the .dcsAdapter if it's not there or the user says to overwrite
            if (createAdapter)
            {
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    sw.AutoFlush = true;

                    sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    sw.WriteLine(string.Format("<!--UniSim Framework Adapter file generated {0}-->", DateTime.Now.ToString("g")));
                    sw.WriteLine("<Adapter>");
                    sw.WriteLine(" 	<AdapterName>GenericFrameworkAdapter</AdapterName>");
                    sw.WriteLine("	<properties>");
                    sw.WriteLine(string.Format("		<PROJECT_SPECIFIC_ADAPTER>{0}</PROJECT_SPECIFIC_ADAPTER>", _txtAdapterDll.Text));
                    sw.WriteLine(string.Format("		<StepSize>{0}</StepSize>", _txtStepSize.Text));
                    sw.WriteLine("		<StubMode>NO</StubMode>");
                    // Now add any custom properties
                    for (int rows = 0; rows < _PropertiesGridView.Rows.Count; rows++)
                    {
                        if (_PropertiesGridView.Rows[rows].Cells[0].Value != null)
                        {
                            property = _PropertiesGridView.Rows[rows].Cells[0].Value.ToString();
                            value = _PropertiesGridView.Rows[rows].Cells[1].Value.ToString();
                            sw.WriteLine(string.Format("		<{0}>{1}</{0}>", property, value));
                        }
                    }
                    sw.WriteLine("	</properties>");
                    sw.Write("</Adapter>");
                }
            }

            // Now create the blank IC
            fileName = string.Format("{0}\\Newly Configured Initial Condition.sic", _txtOutputFolder.Text);
            FileInfo fi = new FileInfo(fileName);

            // Only create if it does not exist
            if (!fi.Exists)
            {
                fi.Create();
            }
        }
        /// <summary>
        /// Detect when a character is typed in the dcsAdapter file name and allow the translate button if conditions are met
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _txtDcsAdapterName_TextChanged(object sender, EventArgs e)
        {
            SetSensitivity();
        }
        /// <summary>
        /// This method will load the properties file for this adapter.  The properties get put into a list and will be displayed
        /// in a DataGridView that can be modified by the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _bnLoadProperties_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _dlgLoadProperties.ShowDialog())
            {
                PopulatePropertyGrid(_dlgLoadProperties.FileName);
            }
        }
        /// <summary>
        /// Populate the property grid with the contents of the specified properties file
        /// </summary>
        /// <param name="fileName"></param>
        private void PopulatePropertyGrid(string fileName)
        {
            using (StreamReader sr = new StreamReader(fileName))
            {
                string strLine;
                List<string> tokenizedLine;

                strLine = sr.ReadLine();
                while (strLine != null)
                {
                    tokenizedLine = new List<string>(strLine.Split(' ', '\t', ','));
                    for (int i = 0; i < tokenizedLine.Count; i++)
                    {
                        if (tokenizedLine[i].Length == 0)
                        {
                            tokenizedLine.RemoveAt(i);
                            i--;
                        }
                    }
                    if (tokenizedLine.Count >= 2)
                    {
                        CustomProperties prop = new CustomProperties();
                        prop.Property = tokenizedLine[0];
                        prop.Value = tokenizedLine[1];
                        _properties.Add(prop);
                    }
                    tokenizedLine.Clear();
                    strLine = sr.ReadLine();
                }
                // Now dereference the binding source to refresh the grid with the loaded properties
                _bindingSourceProperties.DataSource = null;
                _bindingSourceProperties.DataSource = _properties;
            }
        }
        /// <summary>
        /// Save the properties from the DataGridView to a file for later loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _bnSaveProperites_Click(object sender, EventArgs e)
        {
            string property;
            string value;

            _dlgSaveProperties.InitialDirectory = _txtOutputFolder.Text;
            if (DialogResult.OK == _dlgSaveProperties.ShowDialog())
            {
                using (StreamWriter sw = new StreamWriter(_dlgSaveProperties.FileName))
                {
                    sw.AutoFlush = true;
                    for (int rows = 0; rows < _PropertiesGridView.Rows.Count; rows++)
                    {
                        if (_PropertiesGridView.Rows[rows].Cells[0].Value != null)
                        {
                            property = _PropertiesGridView.Rows[rows].Cells[0].Value.ToString();
                            value = _PropertiesGridView.Rows[rows].Cells[1].Value.ToString();
                            sw.WriteLine(string.Format("{0}\t{1}", property, value));
                        }
                    }
                }
            }
        }
    }
}
