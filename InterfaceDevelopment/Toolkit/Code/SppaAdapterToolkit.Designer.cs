﻿namespace SppaAdapter_Toolkit
{
    partial class SppaAdapterToolkit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this._txtInputFile = new System.Windows.Forms.TextBox();
            this._bnInputFile = new System.Windows.Forms.Button();
            this._bnOutputFolder = new System.Windows.Forms.Button();
            this._txtOutputFolder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._txtAdapterDll = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._bnAdapterDll = new System.Windows.Forms.Button();
            this._txtDcsAdapterName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._txtStepSize = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._bnTranslate = new System.Windows.Forms.Button();
            this._dlgInputFile = new System.Windows.Forms.OpenFileDialog();
            this._dlgAdapterDll = new System.Windows.Forms.OpenFileDialog();
            this._dlgOutputFolder = new System.Windows.Forms.FolderBrowserDialog();
            this._PropertiesGridView = new System.Windows.Forms.DataGridView();
            this.propertyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._bindingSourceProperties = new System.Windows.Forms.BindingSource(this.components);
            this._bnSaveProperites = new System.Windows.Forms.Button();
            this._bnLoadProperties = new System.Windows.Forms.Button();
            this._dlgLoadProperties = new System.Windows.Forms.OpenFileDialog();
            this._dlgSaveProperties = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this._PropertiesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSourceProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input file";
            // 
            // _txtInputFile
            // 
            this._txtInputFile.Location = new System.Drawing.Point(16, 30);
            this._txtInputFile.Name = "_txtInputFile";
            this._txtInputFile.ReadOnly = true;
            this._txtInputFile.Size = new System.Drawing.Size(161, 20);
            this._txtInputFile.TabIndex = 1;
            // 
            // _bnInputFile
            // 
            this._bnInputFile.Location = new System.Drawing.Point(175, 30);
            this._bnInputFile.Name = "_bnInputFile";
            this._bnInputFile.Size = new System.Drawing.Size(26, 23);
            this._bnInputFile.TabIndex = 2;
            this._bnInputFile.Text = "...";
            this._bnInputFile.UseVisualStyleBackColor = true;
            this._bnInputFile.Click += new System.EventHandler(this._bnInputFile_Click);
            // 
            // _bnOutputFolder
            // 
            this._bnOutputFolder.Location = new System.Drawing.Point(175, 79);
            this._bnOutputFolder.Name = "_bnOutputFolder";
            this._bnOutputFolder.Size = new System.Drawing.Size(26, 23);
            this._bnOutputFolder.TabIndex = 5;
            this._bnOutputFolder.Text = "...";
            this._bnOutputFolder.UseVisualStyleBackColor = true;
            this._bnOutputFolder.Click += new System.EventHandler(this._bnOutputFolder_Click);
            // 
            // _txtOutputFolder
            // 
            this._txtOutputFolder.Location = new System.Drawing.Point(16, 79);
            this._txtOutputFolder.Name = "_txtOutputFolder";
            this._txtOutputFolder.ReadOnly = true;
            this._txtOutputFolder.Size = new System.Drawing.Size(161, 20);
            this._txtOutputFolder.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Output Directory";
            // 
            // _txtAdapterDll
            // 
            this._txtAdapterDll.Location = new System.Drawing.Point(16, 131);
            this._txtAdapterDll.Name = "_txtAdapterDll";
            this._txtAdapterDll.ReadOnly = true;
            this._txtAdapterDll.Size = new System.Drawing.Size(161, 20);
            this._txtAdapterDll.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Adapter DLL";
            // 
            // _bnAdapterDll
            // 
            this._bnAdapterDll.Location = new System.Drawing.Point(175, 131);
            this._bnAdapterDll.Name = "_bnAdapterDll";
            this._bnAdapterDll.Size = new System.Drawing.Size(26, 23);
            this._bnAdapterDll.TabIndex = 8;
            this._bnAdapterDll.Text = "...";
            this._bnAdapterDll.UseVisualStyleBackColor = true;
            this._bnAdapterDll.Click += new System.EventHandler(this._bnAdapterDll_Click);
            // 
            // _txtDcsAdapterName
            // 
            this._txtDcsAdapterName.Location = new System.Drawing.Point(16, 186);
            this._txtDcsAdapterName.Name = "_txtDcsAdapterName";
            this._txtDcsAdapterName.Size = new System.Drawing.Size(161, 20);
            this._txtDcsAdapterName.TabIndex = 10;
            this.toolTip1.SetToolTip(this._txtDcsAdapterName, "The name of the .dcsAdapter.  This will be the model name");
            this._txtDcsAdapterName.TextChanged += new System.EventHandler(this._txtDcsAdapterName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "DcsAdapter Name";
            this.toolTip1.SetToolTip(this.label4, "The name of the .dcsAdapter.  This will be the model name\r\nin a USO session");
            // 
            // _txtStepSize
            // 
            this._txtStepSize.Location = new System.Drawing.Point(16, 235);
            this._txtStepSize.Name = "_txtStepSize";
            this._txtStepSize.Size = new System.Drawing.Size(49, 20);
            this._txtStepSize.TabIndex = 13;
            this._txtStepSize.Text = "1.0";
            this.toolTip1.SetToolTip(this._txtStepSize, "The name of the .dcsAdapter.  This will be the model name");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Step Size";
            this.toolTip1.SetToolTip(this.label5, "The name of the .dcsAdapter.  This will be the model name\r\nin a USO session");
            // 
            // _bnTranslate
            // 
            this._bnTranslate.Location = new System.Drawing.Point(52, 283);
            this._bnTranslate.Name = "_bnTranslate";
            this._bnTranslate.Size = new System.Drawing.Size(75, 23);
            this._bnTranslate.TabIndex = 11;
            this._bnTranslate.Text = "Translate";
            this._bnTranslate.UseVisualStyleBackColor = true;
            this._bnTranslate.Click += new System.EventHandler(this._bnTranslate_Click);
            // 
            // _dlgInputFile
            // 
            this._dlgInputFile.Filter = "Csv file|*.csv";
            this._dlgInputFile.Title = "Select Input File";
            // 
            // _dlgAdapterDll
            // 
            this._dlgAdapterDll.Filter = "dll file|*.dll";
            this._dlgAdapterDll.Title = "Select Adapter Dll";
            // 
            // _PropertiesGridView
            // 
            this._PropertiesGridView.AutoGenerateColumns = false;
            this._PropertiesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._PropertiesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.propertyDataGridViewTextBoxColumn,
            this.valueDataGridViewTextBoxColumn});
            this._PropertiesGridView.DataSource = this._bindingSourceProperties;
            this._PropertiesGridView.Location = new System.Drawing.Point(217, 30);
            this._PropertiesGridView.Name = "_PropertiesGridView";
            this._PropertiesGridView.Size = new System.Drawing.Size(458, 301);
            this._PropertiesGridView.TabIndex = 14;
            // 
            // propertyDataGridViewTextBoxColumn
            // 
            this.propertyDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.propertyDataGridViewTextBoxColumn.DataPropertyName = "Property";
            this.propertyDataGridViewTextBoxColumn.HeaderText = "Property";
            this.propertyDataGridViewTextBoxColumn.Name = "propertyDataGridViewTextBoxColumn";
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            // 
            // _bindingSourceProperties
            // 
            this._bindingSourceProperties.DataSource = typeof(SppaAdapter_Toolkit.CustomProperties);
            // 
            // _bnSaveProperites
            // 
            this._bnSaveProperites.Location = new System.Drawing.Point(230, 337);
            this._bnSaveProperites.Name = "_bnSaveProperites";
            this._bnSaveProperites.Size = new System.Drawing.Size(75, 37);
            this._bnSaveProperites.TabIndex = 15;
            this._bnSaveProperites.Text = "Save Properites";
            this._bnSaveProperites.UseVisualStyleBackColor = true;
            this._bnSaveProperites.Click += new System.EventHandler(this._bnSaveProperites_Click);
            // 
            // _bnLoadProperties
            // 
            this._bnLoadProperties.Location = new System.Drawing.Point(428, 337);
            this._bnLoadProperties.Name = "_bnLoadProperties";
            this._bnLoadProperties.Size = new System.Drawing.Size(75, 37);
            this._bnLoadProperties.TabIndex = 16;
            this._bnLoadProperties.Text = "Load Properites";
            this._bnLoadProperties.UseVisualStyleBackColor = true;
            this._bnLoadProperties.Click += new System.EventHandler(this._bnLoadProperties_Click);
            // 
            // _dlgLoadProperties
            // 
            this._dlgLoadProperties.Filter = "Text file|*.txt";
            // 
            // _dlgSaveProperties
            // 
            this._dlgSaveProperties.Filter = "Text file|*.txt";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(208, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(498, 370);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Adapter Specific Properties";
            // 
            // GenericFrameworkAdapterToolkit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 395);
            this.Controls.Add(this._bnLoadProperties);
            this.Controls.Add(this._bnSaveProperites);
            this.Controls.Add(this._PropertiesGridView);
            this.Controls.Add(this._txtStepSize);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._bnTranslate);
            this.Controls.Add(this._txtDcsAdapterName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._bnAdapterDll);
            this.Controls.Add(this._txtAdapterDll);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._bnOutputFolder);
            this.Controls.Add(this._txtOutputFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._bnInputFile);
            this.Controls.Add(this._txtInputFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "GenericFrameworkAdapterToolkit";
            this.Text = "Generic Framework Adapter Toolkit";
            ((System.ComponentModel.ISupportInitialize)(this._PropertiesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSourceProperties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _txtInputFile;
        private System.Windows.Forms.Button _bnInputFile;
        private System.Windows.Forms.Button _bnOutputFolder;
        private System.Windows.Forms.TextBox _txtOutputFolder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _txtAdapterDll;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button _bnAdapterDll;
        private System.Windows.Forms.TextBox _txtDcsAdapterName;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button _bnTranslate;
        private System.Windows.Forms.OpenFileDialog _dlgInputFile;
        private System.Windows.Forms.OpenFileDialog _dlgAdapterDll;
        private System.Windows.Forms.FolderBrowserDialog _dlgOutputFolder;
        private System.Windows.Forms.TextBox _txtStepSize;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView _PropertiesGridView;
        private System.Windows.Forms.Button _bnSaveProperites;
        private System.Windows.Forms.Button _bnLoadProperties;
        private System.Windows.Forms.BindingSource _bindingSourceProperties;
        private System.Windows.Forms.OpenFileDialog _dlgLoadProperties;
        private System.Windows.Forms.SaveFileDialog _dlgSaveProperties;
        private System.Windows.Forms.DataGridViewTextBoxColumn propertyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

