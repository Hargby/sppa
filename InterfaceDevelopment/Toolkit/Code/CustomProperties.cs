﻿/////////////////////////////////////////////////////////////////////////////
//
//							  COPYRIGHT (c) 2020
//								HONEYWELL LTD.
//							  ALL RIGHTS RESERVED
//
//  This software is a copyrighted work and/or information protected as a
//  trade secret. Legal rights of Honeywell Ltd. in this software is distinct
//  from ownership of any medium in which the software is embodied. Copyright
//  or trade secret notices included must be reproduced in any copies
//  authorised by Honeywell Ltd.
//
/////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SppaAdapter_Toolkit
{
    /// <summary>
    /// This class is used for display of user defined properties for their specific adapter.  This will allow
    /// specific property entries to be written into the .dcsAdapter file.
    /// </summary>
    class CustomProperties
    {
        /// <summary>
        /// The property name
        /// </summary>
        public string Property { get; set; }
        /// <summary>
        /// The value of the property
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// The default constructor
        /// </summary>
        public CustomProperties()
        {
            Property = string.Empty;
            Value = string.Empty;
        }
    }
}
