﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    class SppaPCHeader
    {
        public UInt32 PCNumber { get; set; }
        public UInt32 APHeadCount { get; set; }
        static public UInt32 HeaderSize { get; set; }
        public SppaPCHeader()
        {
            PCNumber = 0;
            APHeadCount = 0;
            HeaderSize = 8;
        }
        public byte[] GetPCHeader()
        {
            byte[] bytes = new byte[HeaderSize];
            int pos = 0;
            byte[] headerBytes;
            UInt32 uintVal;

            // Form the header buffer
            uintVal = SppaComms.ReverseEndiannessV2(PCNumber);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            uintVal = SppaComms.ReverseEndiannessV2(APHeadCount);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            return bytes;
        }
    }
}
