﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    class FeedbackPoint
    {
        public string AOPoint { get; set; }
        public float AOLo { get; set; }
        public float AOHi {get;set;}
        public string AIPoint1 { get; set; }
        public string AIPoint2 { get; set; }
        public int NumPointsInTable { get; set; }
        public float[] SiemensValues { get; set; }
        public float[] UniSimValues { get; set; }
        public int AOSlotIndex { get; set; }
        public int AOChannel { get; set; }
        public int AOType { get; set; }
        public int AI1SlotIndex { get; set; }
        public int AI1Channel { get; set; }
        public int AI1Type { get; set; }
        public int AI2SlotIndex { get; set; }
        public int AI2Channel { get; set; }
        public int AI2Type { get; set; }

        public FeedbackPoint(int numPoints)
        {
            AOPoint = string.Empty;
            AOLo = 0.0F;
            AOHi = 0.0F;
            AIPoint1 = string.Empty;
            AIPoint2 = string.Empty;
            NumPointsInTable = numPoints;
        }
    }
}
