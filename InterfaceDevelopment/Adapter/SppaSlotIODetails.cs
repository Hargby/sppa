﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    class SppaSlotIODetails
    {
        public int SlotIndex { get; set; }
        public int ChannelIndex { get; set; }
        public int ChannelType { get; set; }
        public int conversionType { get; set; }
        public float euHi { get; set; }
        public float euLo { get; set; }
        public float maHi { get; set; }
        public float maLo { get; set; }
    }
}
