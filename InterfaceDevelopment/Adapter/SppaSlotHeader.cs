﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    /// <summary>
    /// Enum for channel types.  All channel types are in little endian format so no need to reverse
    /// </summary>
    public enum ChannelType
    {
        Bool = 1,
        Float = 2,
        UInt8 = 3,
        UInt16 = 4,
        UInt32 = 5,
        Int8 = 6,
        Int16 = 7,
        Int32 = 8,
    }
    class SppaSlotHeader
    {
        public UInt32 SlotNumber { get; set; }
        public UInt32 ChannelCount { get; set; }
        public UInt32 ChannelType { get; set; }
        public string SlotName { get; set; }
        private bool[] boolValues;
        private float[] floatValues;
        private byte[] uint8Values;
        private UInt16[] uint16Values;
        private UInt32[] uint32Values;
        private sbyte[] int8Values;
        private Int16[] int16Values;
        private Int32[] int32Values;
        static public UInt32 HeaderSize { get; set; }
        enum ChannelIOType
        {
            Bool = 1,
            Float = 2,
            UInt8 = 3,
            UInt16 = 4,
            UInt32 = 5,
            Int8 = 6,
            Int16 = 7,
            Int32 = 8,
        }

        public SppaSlotHeader()
        {
            SlotNumber = 0;
            ChannelCount = 0;
            ChannelType = 0;
            HeaderSize = 12;
        }
        public byte[] GetSlotHeader()
        {
            byte[] bytes = new byte[HeaderSize];
            int pos = 0;
            byte[] headerBytes;
            UInt32 uintVal;

            // Form the header buffer
            uintVal = SppaComms.ReverseEndiannessV2(SlotNumber);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            uintVal = SppaComms.ReverseEndiannessV2(ChannelCount);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            uintVal = SppaComms.ReverseEndiannessV2(ChannelType);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            return bytes;
        }
        public void SetupChannelDataArea()
        {
            ChannelIOType type = (ChannelIOType)ChannelType;
            switch (type)
            {
                case ChannelIOType.Bool:
                    boolValues = new bool[ChannelCount];
                    break;
                case ChannelIOType.Float:
                    floatValues = new float[ChannelCount];
                    break;
                case ChannelIOType.UInt8:
                    uint8Values = new byte[ChannelCount];
                    break;
                case ChannelIOType.UInt16:
                    uint16Values = new UInt16[ChannelCount];
                    break;
                case ChannelIOType.UInt32:
                    uint32Values = new UInt32[ChannelCount];
                    break;
                case ChannelIOType.Int8:
                    int8Values = new sbyte[ChannelCount];
                    break;
                case ChannelIOType.Int16:
                    int16Values = new Int16[ChannelCount];
                    break;
                case ChannelIOType.Int32:
                    int32Values = new Int32[ChannelCount];
                    break;
            }
        }
        public void SetValue(bool value, int index)
        {
            boolValues[index] = value;
        }
        public void SetValue(float value, int index)
        {
            floatValues[index] = value;
        }
        public void SetValue(byte value, int index)
        {
            uint8Values[index] = value;
        }
        public void SetValue(UInt16 value, int index)
        {
            uint16Values[index] = value;
        }
        public void SetValue(UInt32 value, int index)
        {
            uint32Values[index] = value;
        }
        public void SetValue(sbyte value, int index)
        {
            int8Values[index] = value;
        }
        public void SetValue(Int16 value, int index)
        {
            int16Values[index] = value;
        }
        public void SetValue(Int32 value, int index)
        {
            int32Values[index] = value;
        }
        public void GetValue(ref object value, int index)
        {
            ChannelIOType type = (ChannelIOType)ChannelType;
            switch (type)
            {
                case ChannelIOType.Bool:
                    value = boolValues[index];
                    break;
                case ChannelIOType.Float:
                    value = floatValues[index];
                    break;
                case ChannelIOType.UInt8:
                    value = uint8Values[index];
                    break;
                case ChannelIOType.UInt16:
                    value = uint16Values[index];
                    break;
                case ChannelIOType.UInt32:
                    value = uint32Values[index];
                    break;
                case ChannelIOType.Int8:
                    value = int8Values[index];
                    break;
                case ChannelIOType.Int16:
                    value = int16Values[index];
                    break;
                case ChannelIOType.Int32:
                    value = int32Values[index];
                    break;
            }
        }
        public Array GetAllValues()
        {
            Array values = null;
            ChannelIOType type = (ChannelIOType)ChannelType;
            switch (type)
            {
                case ChannelIOType.Bool:
                    values = boolValues.ToArray();
                    break;
                case ChannelIOType.Float:
                    values = floatValues;
                    break;
                case ChannelIOType.UInt8:
                    values = uint8Values;
                    break;
                case ChannelIOType.UInt16:
                    values = uint16Values;
                    break;
                case ChannelIOType.UInt32:
                    values = uint32Values;
                    break;
                case ChannelIOType.Int8:
                    values = int8Values;
                    break;
                case ChannelIOType.Int16:
                    values = int16Values;
                    break;
                case ChannelIOType.Int32:
                    values = int32Values;
                    break;
            }

            // If we don't have a valid type return null;
            return values;
        }
        public void SetAllValue(bool[] values)
        {
            boolValues = values;
        }
        public void SetAllValue(float[] values)
        {
            floatValues = values;
        }
        public void SetAllValue(byte[] values)
        {
            uint8Values = values;
        }
        public void SetAllValue(UInt16[] values)
        {
            uint16Values = values;
        }
        public void SetAllValue(UInt32[] values)
        {
            uint32Values = values;
        }
        public void SetAllValue(sbyte[] values)
        {
            int8Values = values;
        }
        public void SetAllValue(Int16[] values)
        {
            int16Values = values;
        }
        public void SetAllValue(Int32[] values)
        {
            int32Values = values;
        }
    }
}
