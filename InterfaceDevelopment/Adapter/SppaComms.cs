﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace GenericFrameworkAdapterInterface
{
    class SppaComms
    {
        #region Private Fields
        private Socket _commandSocket;
        private Socket _dataSocket;
        private Socket _appServerSocket = null;
        private UInt32 _cmdSessionID = 1;
        private UInt32 _cmdSequenceID = 1;
        private UInt32 _dataSessionID = 1;
        private UInt32 _dataSequenceID = 1;
        private SppaHeader _cmdAckHeader = new SppaHeader();
        private SppaHeader _cmdHeader = new SppaHeader();
        private SppaHeader _dataHeader = new SppaHeader();
        private AliveStatus _sppaStatus = 0;
        private UInt16 _slowModeFactor;
        private UInt16 _fastModeFactor;
        private UInt16[] _SppaStepTime = new UInt16[8];
        private UInt16[] _SppaReplayTime = new UInt16[8];
        private string _aliveProjectName;
        private bool _sppaFrozen = false;
        private int _readDataSize = 0;
        private int _writeDataSize = 0;
        private SppaHWConfigParser _parser;
        private object _commandLock = new object();
        private object _dataLock = new object();
        private object _emuLock = new object();
        private Dictionary<string, int> _readSlotIndices = new Dictionary<string, int>();
        private Dictionary<string, int> _writeSlotIndices = new Dictionary<string, int>();
        private List<SppaSlotIODetails> _readIndices = new List<SppaSlotIODetails>();
        private List<SppaSlotIODetails> _writeIndices = new List<SppaSlotIODetails>();
        #endregion
        #region Public Fields
        public SppaEmuDefinition EmuDefinition { get; set; }
        public float StepSize { get; set; }
        public double CurrentTime { get; set; }
        public Dictionary<string, FeedbackPoint> FeedbackPoints { get; set; }
        #endregion
        #region Constants
        const int SendBufferSize = 16384;
        const int ResponseBufferSize = 8192;
        const UInt32 BitPattern1 = 0x55555555;
        const UInt32 BitPattern2 = 0xFFFFFFFF;
        const UInt32 BitPattern3 = 0;
        const int AckResponseLength = 12;
        const int AckDataSizePos = 20;
        const UInt32 Two = 2;
        const UInt32 Zero = 0;
        const UInt32 One = 1;
        const int IcNamespaceLength = 3;    // "IC\0"
        const int OverlayNamespaceLength = 8;   // "Overlay\0"
        const int BacktrackNamespaceLength = 10;    // "Backtrack\0"
        const int DateTimeLength = 16;  // broken up into 8 shorts
        #endregion
        #region Enums
        /// <summary>
        /// Return values from the socket connection
        /// </summary>
        public enum SppaSocketStatus
        {
            /// <summary>
            /// Good return
            /// </summary>
            OK = 0,
            /// <summary>
            /// Null Argument Error
            /// </summary>
            SocketArgumentError = -1,
            /// <summary>
            /// Connection Error
            /// </summary>
            SocketConnectError = -2,
            /// <summary>
            /// Unknown Socket Error
            /// </summary>
            SocketUnkownError = -3,
        }
        public enum SppaCommandStatus
        {
            INVALID_NAME = 0x8011, //File name or namespace name is invalid.
            INVALID_EXTENSION = 0x8012, //File extension within file name is missing or invalid.
            INVALID_uintVal = 0x8013, //Integer value, e.g. time factor or AP instance number, is out of valid range.
            INVALID_DATETIME = 0x8014, //Part of SYSTEMTIME structure is invalid. 
            INVALID_FLOATVAL = 0x8015, //Float value is not in valid range.
            PRJ_OPEN = 0x8021, //A project version has been opened before.
            PRJ_NOT_OPEN = 0x8022, //No project version has been opened before.
            PRJ_OFFLINE = 0x8023, //Project version was opened in offline mode.
            SIM_OPEN = 0x8031, //A simulation has been opened before.
            SIM_NOT_OPEN = 0x8032, //No simulation has been opened before.
            SIM_IN_FREEZE = 0x8033, //Simulation is in FREEZE state.
            SIM_NOT_IN_FREEZE = 0x8034, //Simulation is not in FREEZE state.
            SIM_IN_RUN = 0x8035, //Simulation is in RUN state.
            SIM_NOT_IN_RUN = 0x8036, //Simulation is not in RUN state.
            REPLAYING = 0x8037, //Simulation is replaying.
            NOT_REPLAYING = 0x8038, //Simulation is not replaying.
            SLOWMODE = 0x8039, //Simulation is in slow mode.
            FASTMODE = 0x803B, // Simulation is in fast mode.
            STEPPING = 0x803D, // Simulation is stepping.
            COULDNT_OPEN_FILE = 0x8041, //File couldn’t be opened.
            COULDNT_CREATE_FILE = 0x8042, //File couldn’t be created.
            COULDNT_READ_FILE = 0x8043, //File couldn’t be read.
            COULDNT_WRITE_FILE = 0x8044, //File couldn’t be written.
            COULDNT_COPY_FILE = 0x8045, //File couldn’t be copied.
            COULDNT_DELETE_FILE = 0x8046, //File couldn’t be deleted.
            FILE_INCONSISTENT = 0x8047, //File content doesn’t match to expected data format.
            OBJ_NOT_FOUND = 0x8051, //Object, e.g. file or project version, wasn’t found.
            OBJ_IGNORED = 0x8052, //Object, e.g. malfunction, was not relevant and was ignored.
            HWCONFIG_NOT_RECEIVED = 0x8061, //PROC_CONFIG telegram not received from component.
            HWCONFIG_INVALID = 0x8062, //Content of PROC_CONFIG telegram does not match to local Emu-HW-Config file.
            PI_NO_CONNECTION = 0x8063, //Connection couldn’t be established.
            COULDNT_FINISH = 0xFFF1, //Error during command completion.
            UNFINISHED_CMD = 0xFFF2, //Error when an additional command has been received before an acknowledge was sent.
            UNKNOWN_CMD = 0xFFF3, //Error when a command with unknown CommandID has been received. If required, additional error subcodes are implemented.
            ANYCMD_NO_ERROR = 0x00007FFF, // No error has occurred.
            OPENPRJ_INVALID_NAME = 0x00018011, // Project version name is invalid.
            OPENPRJ_INVALID_uintVal = 0x00018013, // Offline Status is not in valid range.
            OPENPRJ_PRJ_OPEN = 0x00018021, // A project version has already been opened.
            OPENPRJ_OBJ_NOT_FOUND = 0x00018051, // Project version wasn’t found.
            OPENPRJ_COULDNT_FINISH = 0x0001FFF1, // Error during command completion.
            OPENPRJ_UNFINISHED_CMD = 0x0001FFF2, // A previous command hasn't been completed yet.
            CLOSEPRJ_PRJ_NOT_OPEN = 0x00028022, // No project version has been opened before.
            CLOSEPRJ_COULDNT_FINISH = 0x0002FFF1, // Error during command completion.
            CLOSEPRJ_UNFINISHED_CMD = 0x0002FFF2, // A previous command hasn't been completed yet.
            OPENSIM_PRJ_NOT_OPEN = 0x00038022, // No project version has been opened.
            OPENSIM_SIM_OPEN = 0x00038031, // A simulation already is open.
            OPENSIM_HWCONFIG_NOT_RECEIVED = 0x00038061, // PROC_CONFIG telegram not received.
            OPENSIM_HWCONFIG_INVALID = 0x00038062, // PROC_CONFIG telegram does not match to local file.
            OPENSIM_NO_PI_CONNECTION = 0x00038063, // Connection is not established.
            OPENSIM_COULDNT_FINISH = 0x0003FFF1, // Error during command completion.
            OPENSIM_UNFINISHED_CMD = 0x0003FFF2, // A previous command hasn't been completed yet.
            CLOSESIM_PRJ_NOT_OPEN = 0x00048022, // No project version has been opened.
            CLOSESIM_SIM_NOT_OPEN = 0x00048032, // No simulation has been opened.
            CLOSESIM_COULDNT_FINISH = 0x0004FFF1, // Error during command completion.
            CLOSESIM_UNFINISHED_CMD = 0x0004FFF2, // A previous command hasn't been completed yet.
            INIT_PRJ_NOT_OPEN = 0x00058022, // No project version has been opened.
            INIT_PRJ_OFFLINE = 0x00058023, // Project version was opened offline.
            INIT_SIM_NOT_OPEN = 0x00058032, // No simulation has been opened.
            INIT_SIM_NOT_IN_FREEZE = 0x00058034, // Simulation is not in FREEZE state.
            INIT_COULDNT_FINISH = 0x0005FFF1, // Error during command completion.
            INIT_UNFINISHED_CMD = 0x0005FFF2, // A previous command hasn't been completed yet.
            FREEZE_PRJ_NOT_OPEN = 0x00068022, // No project version has been opened.
            FREEZE_PRJ_OFFLINE = 0x00068023, // Project version was opened offline.
            FREEZE_SIM_NOT_OPEN = 0x00068032, // No simulation has been opened.
            FREEZE_SIM_NOT_IN_RUN = 0x00068036, // Simulation is not in RUN state.
            FREEZE_COULDNT_FINISH = 0x0006FFF1, // Error during command completion.
            FREEZE_UNFINISHED_CMD = 0x0006FFF2, // A previous command hasn't been completed yet.
            RUN_PRJ_NOT_OPEN = 0x00078022, // No project version has been opened.
            RUN_PRJ_OFFLINE = 0x00078023, // Project version was opened offline.
            RUN_SIM_NOT_OPEN = 0x00078032, // No simulation has been opened.
            RUN_SIM_NOT_IN_FREEZE = 0x00078034, // Simulation is not in FREEZE state.
            RUN_COULDNT_FINISH = 0x0007FFF1, // Error during command completion.
            RUN_UNFINISHED_CMD = 0x0007FFF2, //A previous command hasn't been completed yet.
            SLOWMODE_INVALID_uintVal = 0x00088013, // Slow Mode factor is out of valid range.
            SLOWMODE_PRJ_NOT_OPEN = 0x00088022, // No project version has been opened.
            SLOWMODE_PRJ_OFFLINE = 0x00088023, // Project version was opened offline.
            SLOWMODE_SIM_NOT_OPEN = 0x00088032, // Simulation is not open.
            SLOWMODE_SIM_NOT_IN_FREEZE = 0x00088034, // Simulation is not in FREEZE state.
            SLOWMODE_REPLAYING = 0x00088037, // Simulation is replaying.
            SLOWMODE_COULDNT_FINISH = 0x0008FFF1, // Error during command completion.
            SLOWMODE_UNFINISHED_CMD = 0x0008FFF2, // A previous command hasn't been completed yet.
            FASTMODE_INVALID_uintVal = 0x00098013, // Fast Mode factor is not in valid range.
            FASTMODE_PRJ_NOT_OPEN = 0x00098022, // No project version has been opened.
            FASTMODE_PRJ_OFFLINE = 0x00098023, // Project version was opened offline.
            FASTMODE_SIM_NOT_OPEN = 0x00098032, // Simulation is not open.
            FASTMODE_SIM_NOT_IN_FREEZE = 0x00098034, // Simulation is not in FREEZE state.
            FASTMODE_REPLAYING = 0x00098037, // Simulation is replaying.
            FASTMODE_COULDNT_FINISH = 0x0009FFF1, // Error during command completion.
            FASTMODE_UNFINISHED_CMD = 0x0009FFF2, // A previous command hasn't been completed yet.
            STEP_INVALID_uintVal = 0x000A8013, // Cycle number is not in valid range.
            STEP_PRJ_NOT_OPEN = 0x000A8022, // No project version has been opened.
            STEP_PRJ_OFFLINE = 0x000A8023, // Project version was opened offline.
            STEP_SIM_NOT_OPEN = 0x000A8032, // Simulation is not open.
            STEP_SIM_NOT_IN_FREEZE = 0x000A8034, // Simulation is not in FREEZE state.
            STEP_COULDNT_FINISH = 0x000AFFF1, // Error during command completion.
            STEP_UNFINISHED_CMD = 0x000AFFF2, // A previous command hasn't been completed yet.
            RECORD_INVALID_uintVal = 0x000B8013, // Activation status is not in valid range.
            RECORD_PRJ_NOT_OPEN = 0x000B8022, // No project version has been opened.
            RECORD_PRJ_OFFLINE = 0x000B8023, // Project version was opened offline.
            RECORD_SIM_NOT_OPEN = 0x000B8032, // Simulation is not open.
            RECORD_COULDNT_FINISH = 0x000BFFF1, // Error during command completion.
            RECORD_UNFINISHED_CMD = 0x000BFFF2, // A previous command hasn't been completed yet.
            REPLAY_INVALID_DATETIME = 0x000C8014, // Begin or end of record is invalid.
            REPLAY_PRJ_NOT_OPEN = 0x000C8022, // No project version has been opened.
            REPLAY_PRJ_OFFLINE = 0x000C8023, // Project version was opened offline.
            REPLAY_SIM_NOT_OPEN = 0x000C8032, // Simulation is not open.
            REPLAY_SIM_NOT_IN_FREEZE = 0x000C8034, // Simulation is not in FREEZE state.
            REPLAY_REPLAYING = 0x000C8037, // Simulation is replaying.
            REPLAY_COULDNT_FINISH = 0x000CFFF1, // Error during command completion.
            REPLAY_UNFINISHED_CMD = 0x000CFFF2, // A previous command hasn't been completed yet.
            LOADIC_INVALID_NAME = 0x000D8011, // Name of IC file is invalid.
            LOADIC_INVALID_EXTENSION = 0x000D8012, // File extension is neither "BC" nor "IC".
            LOADIC_PRJ_NOT_OPEN = 0x000D8022, // No project version has been opened.
            LOADIC_SIM_NOT_OPEN = 0x000D8032, // No simulation has been opened.
            LOADIC_SIM_NOT_IN_FREEZE = 0x000D8034, // Simulation is not in FREEZE state.
            LOADIC_COULDNT_OPEN_FILE = 0x000D8041, // IC file couldn’t be opened.
            LOADIC_COULDNT_READ_FILE = 0x000D8043, // IC file couldn’t be read.
            LOADIC_FILE_INCONSISTENT = 0x000D8047, // Content of IC file doesn’t match to expected data format.
            LOADIC_IC_MAINTENANCE_REQUIRED = 0x000D8048, // IC Maintenance is required
            LOADIC_OBJ_NOT_FOUND = 0x000D8051, //IC file wasn’t found.
            LOADIC_COULDNT_FINISH = 0x000DFFF1, // Error during command completion.
            LOADIC_UNFINISHED_CMD = 0x000DFFF2, // A previous command hasn't been completed yet.
            LOADIC_NO_LICENSE = 0x000DFFF4, // License for command execution is missing
            SAVESNAP_INVALID_NAME = 0x000E8011, // Name of IC file is invalid.
            SAVESNAP_INVALID_EXTENSION = 0x000E8012, // File extension is neither "BC" nor "IC".
            SAVESNAP_PRJ_NOT_OPEN = 0x000E8022, // No project version has been opened.
            SAVESNAP_PRJ_OFFLINE = 0x000E8023, // Project version was opened offline.
            SAVESNAP_SIM_NOT_OPEN = 0x000E8032, // No simulation has been opened.
            SAVESNAP_COULDNT_CREATE_FILE = 0x000E8042, // IC file couldn’t be created. E.g. IC file with extension  IC" already exists.
            SAVESNAP_COULDNT_WRITE_FILE = 0x000E8044, // IC file couldn’t be written.
            SAVESNAP_COULDNT_DELETE_FILE = 0x000E8046, // IC file with extension "BC" couldn't be deleted.
            SAVESNAP_COULDNT_FINISH = 0x000EFFF1, // Error during command completion.
            SAVESNAP_UNFINISHED_CMD = 0x000EFFF2, // A previous command hasn't been completed yet.
            COPYFILE_INVALID_NAME = 0x000F8011, // Name of IC file is invalid.
            COPYFILE_INVALID_EXTENSION = 0x000F8012, // File extension is neither "BC" nor "IC".
            COPYFILE_PRJ_NOT_OPEN = 0x000F8022, // No project version is open.
            COPYFILE_COULDNT_COPY_FILE = 0x000F8045, // IC file couldn’t be copied.
            COPYFILE_COULDNT_DELETE_FILE = 0x000F8046, // IC file with extension "BC" couldn't be deleted.
            COPYFILE_OBJ_NOT_FOUND = 0x000F8051, // IC file wasn’t found.
            COPYFILE_COULDNT_FINISH = 0x000FFFF1, // Error during command completion.
            COPYFILE_UNFINISHED_CMD = 0x000FFFF2, // A previous command hasn't been completed yet.
            DELFILE_INVALID_NAME = 0x00108011, // Name of IC file is invalid.
            DELFILE_INVALID_EXTENSION = 0x00108012, // File extension is neither "BC" nor "IC".
            DELFILE_PRJ_NOT_OPEN = 0x00108022, // No project version is open.
            DELFILE_COULDNT_DELETE_FILE = 0x00108046, // IC file couldn’t be deleted.
            DELFILE_COULDNT_FINISH = 0x0010FFF1, // Error during command completion.
            DELFILE_UNFINISHED_CMD = 0x0010FFF2, // A previous command hasn't been completed yet.
            SYNCTIME_INVALID_DATETIME = 0x00118014, // Part of date and time is invalid.
            SYNCTIME_PRJ_NOT_OPEN = 0x00118022, // No project version has been opened.
            SYNCTIME_PRJ_OFFLINE = 0x00118023, // Project version was opened offline.
            SYNCTIME_SIM_NOT_OPEN = 0x00118032, // Simulation is not open.
            SYNCTIME_COULDNT_FINISH = 0x0011FFF1, // Error during command completion.
            SYNCTIME_UNFINISHED_CMD = 0x0011FFF2, // A previous command hasn't been completed yet.
            ENDREPLAY_PRJ_NOT_OPEN = 0x00158022, //No project version has been opened.
            ENDREPLAY_PRJ_OFFLINE = 0x00158023, // Project version was opened offline.
            ENDREPLAY_SIM_NOT_OPEN = 0x00158032, // Simulation is not open.
            ENDREPLAY_SIM_NOT_IN_FREEZE = 0x00158034, // Simulaton is not in FREEZE state.
            ENDREPLAY_NOT_REPLAYING = 0x00158038, // Simulation is not open.
            ENDREPLAY_COULDNT_FINISH = 0x0015FFF1, // Error during command completion.
            ENDREPLAY_UNFINISHED_CMD = 0x0015FFF2, // A previous command hasn't been completed yet.
            //The definitions, listed below, are used for development diagnosis purposes only:
            STARTAS_INVALID_uintVal = 0x00218013, // AP instance number is not in valid range.
            STARTAS_PRJ_NOT_OPEN = 0x00218022, // No project version has been opened.
            STARTAS_PRJ_OFFLINE = 0x00218023, // Project version was opened offline.
            STARTAS_SIM_NOT_OPEN = 0x00218032, // Simulation is not open.
            STARTAS_COULDNT_FINISH = 0x0021FFF1, // Error during command completion.
            STARTAS_UNFINISHED_CMD = 0x0021FFF2, // A previous command hasn't been completed yet.
            RESTARTAS_INVALID_uintVal = 0x00228013, // AP instance number is not in valid range.
            RESTARTAS_PRJ_NOT_OPEN = 0x00228022, // No project version has been opened.
            RESTARTAS_PRJ_OFFLINE = 0x00228023, // Project version was opened offline.
            RESTARTAS_SIM_NOT_OPEN = 0x00228032, // Simulation is not open.
            RESTARTAS_COULDNT_FINISH = 0x0022FFF1, // Error during command completion.
            RESTARTAS_UNFINISHED_CMD = 0x0022FFF2, // A previous command hasn't been completed yet.
            STOPAS_INVALID_uintVal = 0x00238013, // AP instance number is not in valid range.
            STOPAS_PRJ_NOT_OPEN = 0x00238022, // No project version has been opened.
            STOPAS_PRJ_OFFLINE = 0x00238023, // Project version was opened offline.
            STOPAS_SIM_NOT_OPEN = 0x00238032, // Simulation is not open.
            STOPAS_COULDNT_FINISH = 0x0023FFF1, // Error during command completion.
            STOPAS_UNFINISHED_CMD = 0x0023FFF2, // A previous command hasn't been completed yet.
            SETMALFUNC_INVALID_uintVal = 0x00318013, // Code for malfunction or number of AP, rack or slot is not in valid range.
            SETMALFUNC_PRJ_NOT_OPEN = 0x00318022, // No project version has been opened.
            SETMALFUNC_PRJ_OFFLINE = 0x00318023, // Project version was opened offline.
            SETMALFUNC_SIM_NOT_OPEN = 0x00318032, // Simulation is not open.
            SETMALFUNC_OBJ_IGNORED = 0x00318052, // Malfunction is not relevant and so ignored.
            SETMALFUNC_COULDNT_FINISH = 0x0031FFF1, // Error during command completion.
            SETMALFUNC_UNFINISHED_CMD = 0x0031FFF2, // A previous command hasn't been completed yet.
            RESETMALFUNC_INVALID_uintVal = 0x00328013, // Number of AP, rack or slot is not in valid range.
            RESETMALFUNC_PRJ_NOT_OPEN = 0x00328022, // No project version has been opened.
            RESETMALFUNC_PRJ_OFFLINE = 0x00328023, // Project version was opened offline.
            RESETMALFUNC_SIM_NOT_OPEN = 0x00328032, // Simulation is not open.
            RESETMALFUNC_OBJ_IGNORED = 0x00328052, // Malfunction is not relevant and so ignored.
            RESETMALFUNC_COULDNT_FINISH = 0x0032FFF1, // Error during command completion.
            RESETMALFUNC_UNFINISHED_CMD = 0x0032FFF2, // A previous command hasn't been completed yet.
            CLEANMALFUNC_PRJ_NOT_OPEN = 0x00338022, // No project version has been opened.
            CLEANMALFUNC_SIM_NOT_OPEN = 0x00338032, // Simulation is not open.
            CLEANMALFUNC_PRJ_OFFLINE = 0x00338023, // Project version was opened offline.
            CLEANMALFUNC_COULDNT_FINISH = 0x0033FFF1, // Error during command completion.
            CLEANMALFUNC_UNFINISHED_CMD = 0x0033FFF2, // A previous command hasn't been completed yet.
            MULTIPLEMALFUNC_INVALID_uintVal = 0x00348013, // Number of AP, rack or slot is not in valid range.
            MULTIPLEMALFUNC_PRJ_NOT_OPEN = 0x00348022, // No project version has been opened.
            MULTIPLEMALFUNC_PRJ_OFFLINE = 0x00348023, // Project version was opened offline.
            MULTIPLEMALFUNC_SIM_NOT_OPEN = 0x00348032, // Simulation is not open.
            MULTIPLEMALFUNC_OBJ_IGNORED = 0x00348052, // All malfunctions are not relevant and so ignored.
            MULTIPLEMALFUNC_COULDNT_FINISH = 0x0034FFF1, // Error during command completion.
            MULTIPLEMALFUNC_UNFINISHED_CMD = 0x0034FFF2, // A previous command hasn't been completed yet.
            ADJUSTSPEED_INVALID_FLOATVAL = 0x00168015, // Float value for speed is invalid.
            ADJUSTSPEED_PRJ_NOT_OPEN = 0x00168022, // No project version has been opened.
            ADJUSTSPEED_PRJ_OFFLINE = 0x00168023, // Project version was opened offline.
            ADJUSTSPEED_SIM_NOT_OPEN = 0x00168032, // Simulation is not open.
            ADJUSTSPEED_SIM_NOT_IN_RUN = 0x00168036, // Simulation is not in RUN state.
            ADJUSTSPEED_SLOWMODE = 0x00168039, // Simulation is in slow mode. 
            ADJUSTSPEED_FASTMODE = 0x0016803B, // Simulation is in fast mode.
            ADJUSTSPEED_STEPPING = 0x0016803D, // Simulation is stepping.
            ADJUSTSPEED_COULDNT_FINISH = 0x0016FFF1, // Error during command completion.
            ADJUSTSPEED_UNFINISHED_CMD = 0x0016FFF2, // A previous command hasn't been completed yet.
            NotEnoughBytesSent = -3, // Not enough bytes sent to SPPA
            SocketUnkownError = -4, // Unkown socket error on send or receive
            SocketIsNull = -5,
            SocketTimeoutError = -6,
            DecodeError = -7,
        }
        private enum SppaCommands
        {
            // Simulator and Control Commands
            CMDID_OPEN_PROJECT = 1,
            CMDID_CLOSE_PROJECT = 2,
            CMDID_OPEN_SIMULATION = 3,
            CMDID_CLOSE_SIMULATION = 4,
            CMDID_INIT = 5,
            CMDID_FREEZE = 6,
            CMDID_RUN = 7,
            CMDID_SLOW_MODE = 8,
            CMDID_FAST_MODE = 9,
            CMDID_STEP = 10,
            CMDID_RECORD = 11,
            CMDID_REPLAY = 12,
            CMDID_LOAD_IC = 13,
            CMDID_SAVE_SNAPSHOT = 14,
            CMDID_COPY_FILE = 15,
            CMDID_DELETE_FILE = 16,
            CMDID_SYNC_TIME = 17,
            CMDID_ALIVE = 18,
            CMDID_ACKNOWLEDGE = 19,
            CMDID_STATUS_OM = 20,
            CMDID_END_REPLAY = 21,
            CMDID_ADJUST_SPEED = 22,
            // Service Commands (are used for test and development purposes only!)
            CMDID_START_AS = 33,
            CMDID_RESTART_AS = 34,
            CMDID_STOP_AS = 35,
            // Malfunction Commands
            CMDID_SET_DCS_MALFUNCTIONS = 49,
            CMDID_RESET_DCS_MALFUNCTIONS = 50,
            CMDID_CLEAN_DCS_MALFUNCTIONS = 51,
            CMDID_MULTIPLE_DCS_MALFUNCTIONS = 52,
            // Commands for Communication with Process Model
            CMDID_PROC_CONFIG = 65,
            CMDID_PROC_DATA = 66,
        };
        enum AliveStatus
        {
            // Initial status of simulation and signal image interface:
            ALIVESTS_INITIAL = 0x00000000, // Initial status
            // Bits indicating status of simulation:
            ALIVESTS_PRJ_OPEN = 0x00000001, // Project is open
            ALIVESTS_SIM_OPEN = 0x00000002, // Simulation is open
            ALIVESTS_SIM_IN_RUN = 0x00000004, // Simulation is in RUN state
            ALIVESTS_SIM_IN_FREEZE = 0x00000008, // Simulation is in FREEZE state
            ALIVESTS_SLOWMODE = 0x00000010, // Simulation is running in slow mode
            ALIVESTS_FASTMODE = 0x00000020, // Simulation is running in fast mode
            ALIVESTS_RECORDING = 0x00000040, // Simulation is in record mode
            ALIVESTS_REPLAYING = 0x00000080, // Simulation is in replay mode
            ALIVESTS_STEPPING = 0x00000100, // Simulation is in stepping mode
            ALIVESTS_OFFLINE = 0x00000200, // Project is offline
            ALIVESTS_ALL_SIM = 0x000003FF, // Mask for all simulation status
            // Bits indicating status of signal image interface:
            ALIVESTS_PI_INTERFACE_READY = 0x00010000, // Signal image interface is ready
            ALIVESTS_PI_ALL_CFG_RECEIVED = 0x00020000, // All PROC_CONFIG telegrams have been received on signal image interface
            ALIVESTS_PI_ALL_CFG_VALID = 0x00040000, // All PROC_CONFIG telegrams received on signal image interface are valid
            ALIVESTS_ALL_PI = 0x00070000, // Mask for all process image interface status
            // Bits indicating status of asynchronous OM messages
            ALIVESTS_OM_MSG_STARTUP = 0x01000000, // OM Startup message
            ALIVESTS_OM_MSG_UP = 0x02000000, // OM Up message
            ALIVESTS_OM_MSG_ERROR = 0x04000000, // OM Error message
            ALIVESTS_OM_MSG_FATAL = 0x08000000, // OM Fatal message
            ALIVESTS_OM_MSG_DOWN = 0x10000000, // OM Down message
            ALIVESTS_ALL_OM = 0x1F000000, /// Mask for all OM messages
            // Mask for all used status bits:
            ALIVESTS_ALL = 0x1F0703FF, // (ALIVESTS_ALL_SIM | ALIVESTS_ALL_PI | ALIVESTS_ALL_OM) //< Mask for simulation status, signal image interface and OM message status
        };
        public enum ChannelType
        {
            Bool = 1,
            Float = 2,
            Uint8 = 3,
            Uint16 = 4,
            UInt32 = 5, 
            Int8 = 6, 
            Int16 = 9, 
            Int32 = 8,
        };
        #endregion
        #region Event Handlers
        public virtual event EventHandler CommsMessageLogged;
        #endregion
        #region Private Methods
        /// <summary>
        /// Convert the unsigned short into Big Endian
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static ushort ReverseEndiannessV2(ushort value)
        {
            return (ushort)((value & 0x00FFU) << 8 | (value & 0xFF00U) >> 8);
        }
        /// <summary>
        /// Convert the unsigned in into Big Endian
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static uint ReverseEndiannessV2(uint value)
        {
            value = (value << 16) | (value >> 16);
            value = (value & 0x00FF00FF) << 8 | (value & 0xFF00FF00) >> 8;
            return value;
        }
        /// <summary>
        /// Paste the bytes into the output byte array.  This allows us to build a network array
        /// </summary>
        /// <param name="inArray">input array</param>
        /// <param name="outArray">output array</param>
        /// <param name="pos">position in the output array</param>
        public static void PasteBytesIntoBuffer(byte[] inArray, ref byte[] outArray, int pos)
        {
            if (pos + inArray.Length > outArray.Length)
            {
                throw new System.ArgumentOutOfRangeException("message Overflow");
            }
            for (int index = 0; index < inArray.Length; index++)
            {
                outArray[index + pos] = inArray[index];
            }
        }
        /// <summary>
        /// Insert the desired values into the command header.  This saves us having to do this for each command
        /// Any additional data portion of the packet will be handled by the indiviual calling method
        /// </summary>
        /// <param name="time">simulation time</param>
        /// <param name="command">The SPPA command</param>
        /// <param name="version">what version of the call is being used</param>
        /// <param name="size">the size of the additional data</param>
        /// <param name="sequenceID">the packet sequence ID</param>
        private void PopulateCommandHeader(double time, SppaCommands command, UInt32 version, UInt32 size, ref UInt32 sequenceID)
        {
            UInt32 xorLen;
            UInt32 cmd;
            UInt32 xorCmd;
            UInt32 xorSequenceID;
            UInt32 checksum;
            UInt32 xorChecksum;
            DateTime nowDate = DateTime.Today;
            TimeSpan ts = TimeSpan.FromSeconds(time);

            _cmdHeader.BitPattern1 = ReverseEndiannessV2(BitPattern1);
            _cmdHeader.BitPattern2 = ReverseEndiannessV2(BitPattern2);
            _cmdHeader.BitPattern3 = ReverseEndiannessV2(BitPattern3);
            _cmdHeader.VersionID = ReverseEndiannessV2(version);
            _cmdHeader.SessionID = ReverseEndiannessV2(_cmdSessionID);

            _cmdHeader.Size = ReverseEndiannessV2(size);
            xorLen = size ^ BitPattern2;
            _cmdHeader.SizeCompl = ReverseEndiannessV2(xorLen);

            // Put in the command and the xor of the command
            cmd = Convert.ToUInt32(command);
            _cmdHeader.CommandID = ReverseEndiannessV2(cmd);
            xorCmd = cmd ^ BitPattern2;
            _cmdHeader.CommandIDCompl = ReverseEndiannessV2(xorCmd);

            // Put in the sequence ID and the xor of the sequence ID
            _cmdHeader.SequenceID = ReverseEndiannessV2(sequenceID);
            xorSequenceID = sequenceID ^ BitPattern2;
            _cmdHeader.SequenceIDCompl = ReverseEndiannessV2(xorSequenceID);

            // Put in the checksum and xor of the checksum (size ^ command ^ sequence
            checksum = size ^ cmd ^ sequenceID;
            _cmdHeader.Checksum = ReverseEndiannessV2(checksum);
            xorChecksum = checksum ^ BitPattern2;
            _cmdHeader.ChecksumCompl = ReverseEndiannessV2(xorChecksum);

            // Now the date and time
            _cmdHeader.Year = ReverseEndiannessV2((UInt16)nowDate.Year);
            _cmdHeader.Day = ReverseEndiannessV2((UInt16)nowDate.Day);
            _cmdHeader.DayOfWeek = ReverseEndiannessV2((UInt16)nowDate.DayOfWeek);
            _cmdHeader.Month = ReverseEndiannessV2((UInt16)nowDate.Month);
            _cmdHeader.Hour = ReverseEndiannessV2((UInt16)ts.Hours);
            _cmdHeader.Minute = ReverseEndiannessV2((UInt16)ts.Minutes);
            _cmdHeader.Second = ReverseEndiannessV2((UInt16)ts.Seconds);
            _cmdHeader.MilliSecond = ReverseEndiannessV2((UInt16)ts.Milliseconds);
        }
        /// <summary>
        /// Insert the desired values into the command header.  This saves us having to do this for each command
        /// Any additional data portion of the packet will be handled by the indiviual calling method
        /// </summary>
        /// <param name="time">simulation time</param>
        /// <param name="command">The SPPA command</param>
        /// <param name="version">what version of the call is being used</param>
        /// <param name="size">the size of the additional data</param>
        /// <param name="sequenceID">the packet sequence ID</param>
        private void PopulateDataHeader(double time, SppaCommands command, UInt32 version, UInt32 size, ref UInt32 sequenceID)
        {
            UInt32 xorLen;
            UInt32 cmd;
            UInt32 xorCmd;
            UInt32 xorSequenceID;
            UInt32 checksum;
            UInt32 xorChecksum;
            DateTime nowDate = DateTime.Today;
            TimeSpan ts = TimeSpan.FromSeconds(time);

            _dataHeader.BitPattern1 = ReverseEndiannessV2(BitPattern1);
            _dataHeader.BitPattern2 = ReverseEndiannessV2(BitPattern2);
            _dataHeader.BitPattern3 = ReverseEndiannessV2(BitPattern3);
            _dataHeader.VersionID = ReverseEndiannessV2(version);
            //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"setting proc_data session ID to {_dataSessionID}");
            _dataHeader.SessionID = ReverseEndiannessV2(_dataSessionID);

            _dataHeader.Size = ReverseEndiannessV2(size);
            xorLen = size ^ BitPattern2;
            _dataHeader.SizeCompl = ReverseEndiannessV2(xorLen);

            // Put in the command and the xor of the command
            cmd = Convert.ToUInt32(command);
            _dataHeader.CommandID = ReverseEndiannessV2(cmd);
            xorCmd = cmd ^ BitPattern2;
            _dataHeader.CommandIDCompl = ReverseEndiannessV2(xorCmd);

            // Put in the sequence ID and the xor of the sequence ID
            _dataHeader.SequenceID = ReverseEndiannessV2(sequenceID);
            xorSequenceID = sequenceID ^ BitPattern2;
            _dataHeader.SequenceIDCompl = ReverseEndiannessV2(xorSequenceID);

            // Put in the checksum and xor of the checksum (size ^ command ^ sequence
            checksum = size ^ cmd ^ sequenceID;
            _dataHeader.Checksum = ReverseEndiannessV2(checksum);
            xorChecksum = checksum ^ BitPattern2;
            _dataHeader.ChecksumCompl = ReverseEndiannessV2(xorChecksum);

            // Now the date and time
            _dataHeader.Year = ReverseEndiannessV2((UInt16)nowDate.Year);
            _dataHeader.Day = ReverseEndiannessV2((UInt16)nowDate.Day);
            _dataHeader.DayOfWeek = ReverseEndiannessV2((UInt16)nowDate.DayOfWeek);
            _dataHeader.Month = ReverseEndiannessV2((UInt16)nowDate.Month);
            _dataHeader.Hour = ReverseEndiannessV2((UInt16)ts.Hours);
            _dataHeader.Minute = ReverseEndiannessV2((UInt16)ts.Minutes);
            _dataHeader.Second = ReverseEndiannessV2((UInt16)ts.Seconds);
            _dataHeader.MilliSecond = ReverseEndiannessV2((UInt16)ts.Milliseconds);
        }
        /// <summary>
        /// Decode the header for the acknowledge from SPPA
        /// </summary>
        /// <param name="receiveBuffer"></param>
        private void DecodeCmdAckHeader(byte[] receiveBuffer)
        {
            int pos = 0;
            UInt32 uintVal;
            UInt16 ushortVal;

            uintVal = BitConverter.ToUInt32(receiveBuffer, 0);
            _cmdAckHeader.BitPattern1 = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.BitPattern2 = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.BitPattern3 = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.VersionID = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.SessionID = ReverseEndiannessV2(uintVal);

            // Compute the data length and the xor of the data length
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.Size = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.SizeCompl = ReverseEndiannessV2(uintVal);

            // Put in the command and the xor of the command
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.CommandID = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.CommandIDCompl = ReverseEndiannessV2(uintVal);

            // Put in the sequence ID and the xor of the sequence ID
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.SequenceID = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.SequenceIDCompl = ReverseEndiannessV2(uintVal);

            // Put in the checksum and xor of the checksum (size ^ command ^ sequence
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.Checksum = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _cmdAckHeader.ChecksumCompl = ReverseEndiannessV2(uintVal);

            // Now the date and time
            pos += 4;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _cmdAckHeader.Year = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _cmdAckHeader.Month = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _cmdAckHeader.DayOfWeek = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _cmdAckHeader.Day = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _cmdAckHeader.Hour = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _cmdAckHeader.Minute = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _cmdAckHeader.Second = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _cmdAckHeader.MilliSecond = ReverseEndiannessV2(ushortVal);
            pos += 2;
        }
        /// <summary>
        /// Send the packet to SPPA
        /// </summary>
        /// <param name="command">The command buffer</param>
        /// <param name="msgLength">length of the message</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        private SppaCommandStatus SendCommand(Socket sock, byte[] command, int msgLength, ref string errMsg)
        {
            int nBytes;
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;

            if (sock == null)
            {
                status = SppaCommandStatus.SocketIsNull;
                return status;
            }

            lock (_commandLock)
            {
                try
                {
                    nBytes = sock.Send(command, msgLength, SocketFlags.None);
                    if (nBytes != msgLength)
                    {
                        errMsg = $"CommandComms: Not enough bytes sent: Expected was {msgLength}, actual was {nBytes}";
                        return SppaCommandStatus.NotEnoughBytesSent;
                    }
                }
                catch (SocketException se)
                {
                    errMsg = $"CommandComms: Error sending message on {sock.LocalEndPoint.ToString()}: !{se.Message}";
                    if ((se.SocketErrorCode == SocketError.ConnectionReset) || (se.SocketErrorCode == SocketError.ConnectionAborted))
                    {
                        _dataSocket.Close();
                        _dataSocket = null;
                        _commandSocket.Close();
                        _commandSocket = null;
                        if (_appServerSocket != null)
                        {
                            _appServerSocket.Close();
                            _appServerSocket = null;
                        }
                    }
                    return SppaCommandStatus.SocketUnkownError;
                }
            }
            return status;
        }
        /// <summary>
        /// Send the packet to SPPA
        /// </summary>
        /// <param name="command">The command buffer</param>
        /// <param name="msgLength">length of the message</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        private SppaCommandStatus SendDataPacket(byte[] command, int msgLength, ref string errMsg)
        {
            int nBytes;
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;

            lock (_dataLock)
            {
                if (_dataSocket == null)
                {
                    status = SppaCommandStatus.SocketIsNull;
                    return status;
                }

                try
                {
                    nBytes = _dataSocket.Send(command, msgLength, SocketFlags.None);
                    if (nBytes != msgLength)
                    {
                        errMsg = $"DataComms: Not enough bytes sent: Expected was {msgLength}, actual was {nBytes}";
                        return SppaCommandStatus.NotEnoughBytesSent;
                    }
                }
                catch (SocketException se)
                {
                    errMsg = $"DataComms: Error sending message: !{se.Message}";
                    if ((se.SocketErrorCode == SocketError.ConnectionReset) || (se.SocketErrorCode == SocketError.ConnectionAborted))
                    {
                        _dataSocket.Close();
                        _dataSocket = null;
                        _commandSocket.Close();
                        _commandSocket = null;
                        if (_appServerSocket != null)
                        {
                            _appServerSocket.Close();
                            _appServerSocket = null;
                        }
                    }
                    return SppaCommandStatus.SocketUnkownError;
                }
            }
            return status;
        }
        /// <summary>
        /// Receive the command acknowledgement 
        /// </summary>
        /// <param name="expectedBytes">number of bytes to receive from SPPA</param>
        /// <param name="receiveBuffer">The buffer for the response</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        private SppaCommandStatus ReceiveCommandAck(Socket sock, ref int receivedBytes, ref byte[] receiveBuffer, ref string errMsg, int timeout, bool isAlive = false)
        {
            bool done = false;
            int readIndex = 0;
            int pos = 0;
            int dataPos = 0;
            int totalBytes = 0;
            int nBytes = 0;
            byte[] rcvBuffer = new byte[ResponseBufferSize];
            byte[] rcvBuffer2 = new byte[ResponseBufferSize];
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;

            if (sock == null)
            {
                status = SppaCommandStatus.SocketIsNull;
                return status;
            }

            lock (_commandLock)
            {
                if (timeout < 500)
                {
                    timeout = 500;
                }
                sock.ReceiveTimeout = timeout;

                while (!done)
                {
                    totalBytes = 0;
                    pos = 0;
                    dataPos = 0;

                    while (totalBytes < _cmdAckHeader.Length)
                    {
                        readIndex = 0;
                        try
                        {
                            nBytes = sock.Receive(rcvBuffer, _cmdAckHeader.Length, SocketFlags.None);
                            for (; pos < nBytes; pos++)
                            {
                                receiveBuffer[pos] = rcvBuffer[readIndex++];
                            }
                            totalBytes += nBytes;
                            pos = totalBytes;
                        }
                        catch (SocketException se)
                        {
                            if (se.SocketErrorCode == SocketError.TimedOut)
                            {
                                return SppaCommandStatus.SocketTimeoutError;
                            }
                            else
                            {
                                errMsg = $"Error receiving on {sock}: !{se.Message}.  Number of bytes received = {pos}";
                                if ((se.SocketErrorCode == SocketError.ConnectionReset) || (se.SocketErrorCode == SocketError.ConnectionAborted))
                                {
                                    _dataSocket.Close();
                                    _dataSocket = null;
                                    _commandSocket.Close();
                                    _commandSocket = null;
                                    if (_appServerSocket != null)
                                    {
                                        _appServerSocket.Close();
                                        _appServerSocket = null;
                                    }
                                }
                                return SppaCommandStatus.SocketUnkownError;
                            }
                        }
                    }

                    DecodeCmdAckHeader(receiveBuffer);

                    if (_cmdAckHeader.CommandID == (int)SppaCommands.CMDID_ALIVE)
                    {
                        if (isAlive)
                        {
                            done = true;
                        }
                    }
                    else
                    {
                        done = true;
                    }
                    // Now get the rest if there is any more
                    if (_cmdAckHeader.Size > 0)
                    {
                        pos = _cmdAckHeader.Length;
                        totalBytes = 0;
                        while (totalBytes < _cmdAckHeader.Size)
                        {
                            readIndex = 0;
                            try
                            {
                                nBytes = sock.Receive(rcvBuffer2, (int)_cmdAckHeader.Size, SocketFlags.None);
                                for (; dataPos < nBytes; dataPos++)
                                {
                                    receiveBuffer[pos + dataPos] = rcvBuffer2[readIndex++];
                                }
                                totalBytes += nBytes;
                                dataPos = totalBytes;
                            }
                            catch (SocketException se2)
                            {
                                errMsg = $"Error receiving: !{se2.Message}.  Number of bytes received = {pos}";
                                if ((se2.SocketErrorCode == SocketError.ConnectionReset) || (se2.SocketErrorCode == SocketError.ConnectionAborted))
                                {
                                    _dataSocket.Close();
                                    _dataSocket = null;
                                    _commandSocket.Close();
                                    _commandSocket = null;
                                }
                                return SppaCommandStatus.SocketUnkownError;
                            }
                        }
                    }

                    if (_cmdAckHeader.CommandID == (int)SppaCommands.CMDID_ALIVE)
                    {
                        DecodeAliveData(receiveBuffer);
                    }
                    receivedBytes = pos + totalBytes;
                }
            }
            return status;
        }
        /// <summary>
        /// Receive the command acknowledgement 
        /// </summary>
        /// <param name="expectedBytes">number of bytes to receive from SPPA</param>
        /// <param name="receiveBuffer">The buffer for the response</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        private SppaCommandStatus ReceiveDataPackets(ref int receivedBytes, ref byte[] receiveBuffer, ref string errMsg, int timeout, bool isConfig = false)
        {
            bool done = false;
            int readIndex = 0;
            int pos = 0;
            int dataPos = 0;
            int totalBytes = 0;
            int nBytes = 0;
            byte[] rcvBuffer = new byte[ResponseBufferSize];
            byte[] rcvBuffer2 = new byte[ResponseBufferSize];
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;

            lock (_dataLock)
            {
                if (_dataSocket == null)
                {
                    status = SppaCommandStatus.SocketIsNull;
                    return status;
                }
                //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, "Getting data header");
                _dataSocket.ReceiveTimeout = timeout;

                while (!done)
                {
                    totalBytes = 0;
                    pos = 0;
                    dataPos = 0;

                    while (totalBytes < _dataHeader.Length)
                    {
                        readIndex = 0;
                        try
                        {
                            nBytes = _dataSocket.Receive(rcvBuffer, _dataHeader.Length - totalBytes, SocketFlags.None);
                            LogCommsMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, $"Received {nBytes} bytes.  Left is {_dataHeader.Length - nBytes}");
                            for (; pos < nBytes; pos++)
                            {
                                receiveBuffer[pos] = rcvBuffer[readIndex++];
                            }
                            totalBytes += nBytes;
                            pos = totalBytes;
                        }
                        catch (SocketException se)
                        {
                            LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Error on socket.receive.  Error is {se.Message}");
                            if (se.SocketErrorCode != SocketError.TimedOut)
                            {
                                errMsg = $"Error receiving: {se.Message}.  Number of bytes received = {pos}";
                                if ((se.SocketErrorCode == SocketError.ConnectionReset) || (se.SocketErrorCode == SocketError.ConnectionAborted))
                                {
                                    _dataSocket.Close();
                                    _dataSocket = null;
                                    _commandSocket.Close();
                                    _commandSocket = null;
                                }
                                return SppaCommandStatus.SocketUnkownError;
                            }
                            else
                            {
                                errMsg = "ReceiveDataPackets timed out";
                                return SppaCommandStatus.SocketTimeoutError;
                            }
                        }
                    }
                    LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, "Decoding data header");

                    try
                    {
                        DecodeDataHeader(receiveBuffer);
                    }
                    catch (Exception ex1)
                    {
                        LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Exeption decoding data: {ex1.Message} {ex1.StackTrace}");
                        return SppaCommandStatus.DecodeError;
                    }

                    LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, $"Getting rest.  size = {_dataHeader.Size}");
                    // Now get the rest if there is any more
                    if (_dataHeader.Size > 0)
                    {
                        pos = _dataHeader.Length;
                        totalBytes = 0;
                        while (totalBytes < _dataHeader.Size)
                        {
                            readIndex = 0;
                            try
                            {
                                //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, "Socket receive");
                                nBytes = _dataSocket.Receive(rcvBuffer2, (int)_dataHeader.Size, SocketFlags.None);
                                for (; dataPos < nBytes; dataPos++)
                                {
                                    receiveBuffer[pos + dataPos] = rcvBuffer2[readIndex++];
                                }
                                totalBytes += nBytes;
                                dataPos = totalBytes;
                                //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, $"t={totalBytes}, pos = {dataPos}");
                            }
                            catch (SocketException se2)
                            {
                                errMsg = $"Error receiving: !{se2.Message}.  Number of bytes received = {pos}";
                                if ((se2.SocketErrorCode == SocketError.ConnectionReset) || (se2.SocketErrorCode == SocketError.ConnectionAborted))
                                {
                                    _dataSocket.Close();
                                    _dataSocket = null;
                                    _commandSocket.Close();
                                    _commandSocket = null;
                                }
                                return SppaCommandStatus.SocketUnkownError;
                            }
                        }
                    }

                    if (_dataHeader.CommandID == (UInt32)SppaCommands.CMDID_PROC_CONFIG)
                    {
                        status = DecodeProcConfig(_dataHeader.Size, receiveBuffer);
                    }

                    // Check ti see if there was another packet from Siemens before we finished
                    if (_dataSocket.Available > 0)
                    {
//                        LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, "More bytes to read after data read");
                    }
                    else
                    {
                        done = true;
                    }
                    receivedBytes = pos + totalBytes;
                }
            }
            return status;
        }

        private SppaCommandStatus DecodeProcConfig(UInt32 size, byte[] receiveBuffer)
        {
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;

            return status;
        }
        private void DecodeDataHeader(byte[] receiveBuffer)
        {
            int pos = 0;
            UInt32 uintVal;
            UInt16 ushortVal;

            uintVal = BitConverter.ToUInt32(receiveBuffer, 0);
            _dataHeader.BitPattern1 = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.BitPattern2 = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.BitPattern3 = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.VersionID = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.SessionID = ReverseEndiannessV2(uintVal);

            // Compute the data length and the xor of the data length
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.Size = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.SizeCompl = ReverseEndiannessV2(uintVal);

            // Put in the command and the xor of the command
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.CommandID = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.CommandIDCompl = ReverseEndiannessV2(uintVal);

            // Put in the sequence ID and the xor of the sequence ID
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.SequenceID = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.SequenceIDCompl = ReverseEndiannessV2(uintVal);

            //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"DecodeDataHeader: session = {_dataHeader.SessionID}, sequence = {_dataHeader.SequenceID}");
            // Put in the checksum and xor of the checksum (size ^ command ^ sequence
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.Checksum = ReverseEndiannessV2(uintVal);
            pos += 4;
            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _dataHeader.ChecksumCompl = ReverseEndiannessV2(uintVal);

            // Now the date and time
            pos += 4;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _dataHeader.Year = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _dataHeader.Month = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _dataHeader.DayOfWeek = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _dataHeader.Day = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _dataHeader.Hour = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _dataHeader.Minute = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _dataHeader.Second = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _dataHeader.MilliSecond = ReverseEndiannessV2(ushortVal);
            pos += 2;
        }
        private void LogCommsMessage(eGenericFrameworkAdapterErrorLevel level, string message, Exception ex = null)
        {
            if (CommsMessageLogged != null)
            {
                StringBuilder msg = new StringBuilder();
                if (!string.IsNullOrEmpty(message))
                {
                    msg.AppendLine(message);
                }

                if (ex != null)
                {
                    msg.AppendLine("Exception Message = " + ex.Message);
                    Exception inner = ex.InnerException;
                    while (inner != null)
                    {
                        msg.AppendLine("\t - " + inner.Message);

                        //Dig deeper
                        inner = inner.InnerException;
                    }

                    msg.AppendLine();
                    msg.AppendLine("Stack Trace = " + ex.StackTrace.ToString());
                }

                CommsMessageLogged(this, new GenericFrameworkAdapterEventArgs(msg.ToString(), level));
            }
        }
        private void SetWriteSlotIndices()
        {
            int pcNumber;
            int apNumber;
            int rackNumber;
            int slotNumber;
            int apIndex;
            int rackIndex;
            int slotIndex;
            int rackCount;
            int slotCount;
            int writeSlotIndex;
            string slotKey;

            pcNumber = (int)EmuDefinition.InputPCHeaders[0].PCNumber;
            rackIndex = 0;
            slotIndex = 0;
            for (apIndex = 0; apIndex < EmuDefinition.InputApHeaders.Count; apIndex++)
            {
                apNumber = (int)EmuDefinition.InputApHeaders[apIndex].APNumber;
                rackCount = (int)(EmuDefinition.InputApHeaders[apIndex].RackHeadCount) + rackIndex;
                for (; rackIndex < rackCount; rackIndex++)
                {
                    rackNumber = (int)EmuDefinition.InputRackHeaders[rackIndex].RackNumber;
                    slotCount = (int)(EmuDefinition.InputRackHeaders[rackIndex].SlotHeadCount + slotIndex);
                    for (; slotIndex < slotCount; slotIndex++)
                    {
                        slotNumber = (int)EmuDefinition.InputSlotHeaders[slotIndex].SlotNumber;

                        // Now add this address to the read slot dictionary
                        slotKey = $"PC{pcNumber:D2}AP{apNumber:D3}R{rackNumber:D3}S{slotNumber:D3}";
                        if (!_writeSlotIndices.TryGetValue(slotKey, out writeSlotIndex))
                        {
                            _writeSlotIndices.Add(slotKey, slotIndex);
                        }
                    }
                }
            }
        }
        private void SetReadSlotIndices()
        {
            int pcNumber;
            int apNumber;
            int rackNumber;
            int slotNumber;
            int apIndex;
            int rackIndex;
            int slotIndex;
            int readSlotIndex;
            int rackCount;
            int slotCount;
            string slotKey;

            pcNumber = (int)EmuDefinition.OutputPCHeaders[0].PCNumber;
            rackIndex = 0;
            slotIndex = 0;
            for (apIndex = 0; apIndex < EmuDefinition.OutputApHeaders.Count; apIndex++)
            {
                apNumber = (int)EmuDefinition.OutputApHeaders[apIndex].APNumber;
                rackCount = (int)(EmuDefinition.OutputApHeaders[apIndex].RackHeadCount) + rackIndex;
                for (; rackIndex < rackCount; rackIndex++)
                {
                    rackNumber = (int)EmuDefinition.OutputRackHeaders[rackIndex].RackNumber;
                    slotCount = (int)(EmuDefinition.OutputRackHeaders[rackIndex].SlotHeadCount + slotIndex);
                    for (; slotIndex < slotCount; slotIndex++)
                    {
                        slotNumber = (int)EmuDefinition.OutputSlotHeaders[slotIndex].SlotNumber;

                        // Now add this address to the read slot dictionary
                        slotKey = $"PC{pcNumber:D2}AP{apNumber:D3}R{rackNumber:D3}S{slotNumber:D3}";
                        if (!_readSlotIndices.TryGetValue(slotKey, out readSlotIndex))
                        {
                            _readSlotIndices.Add(slotKey, slotIndex);
                        }
                    }
                }
            }
        }
        private void ProcessFeedbackPoints()
        {
            foreach (FeedbackPoint fbPoint in FeedbackPoints.Values)
            {
            }
        }
        #endregion
        #region Public Methods
        /// <summary>
        /// Initialize the socket connection to the SPPA
        /// </summary>
        /// <param name="hostName">The hostname of the SPPA server</param>
        /// <param name="portNo">The port number the SPPA is listening to</param>
        /// <param name="Out: errMsg">If there is an error this willbe set to the return exception message</param>
        /// <param name="adapterHandle">Reference to the calling adapter object so that this comms layer can log directly</param>
        /// <returns></returns>
        public SppaSocketStatus SppaInit(string hostName, int commandPortNo, string appServer, int appServerPortNo, ref string errMsg)
        {
            SppaSocketStatus ret = SppaSocketStatus.OK;
            IPEndPoint cmdIPEndPoint;
            IPEndPoint appServerIPEndPoint;

            _commandSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _dataSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress[] ipHostInfo = Dns.GetHostAddresses(hostName);
            IPAddress ipAddress = null;

            for (int i = 0; i <= ipHostInfo.Length - 1; i++)
            {
                if (ipHostInfo[i].AddressFamily != AddressFamily.InterNetworkV6)
                {
                    ipAddress = ipHostInfo[i];
                    break;
                }
            }

            if (ipAddress == null)
            {
                errMsg = $"Could not resolve the host name into the IP address for EmulationComputer {hostName}";
                return SppaSocketStatus.SocketUnkownError;
            }

            cmdIPEndPoint = new IPEndPoint(ipAddress, commandPortNo);

            // Set the socket timeouts
            _commandSocket.ReceiveTimeout = 30000;
            _commandSocket.SendTimeout = 30000;

            try
            {
                _commandSocket.Connect(cmdIPEndPoint);
                _cmdSessionID = 1;
            }
            catch (ArgumentNullException ane)
            {
                errMsg = $"CommandSocket->ArgumentNullException: Error = !{ane.Message}";
                _commandSocket.Close();
                _commandSocket = null;
                return SppaSocketStatus.SocketArgumentError;
            }
            catch (SocketException se)
            {
                errMsg = $"CommandSocket->SocketException: Error = !{se.Message}";
                _commandSocket.Close();
                _commandSocket = null;
                return SppaSocketStatus.SocketConnectError;
            }
            catch (Exception e)
            {
                errMsg = $"CommandSocket->Exception: Error = !{e.Message}";
                _commandSocket.Close();
                _commandSocket = null;
                return SppaSocketStatus.SocketUnkownError;
            }

            // Umdocumented that SPPA will send an ALIVE packet after connection
            GetAlivePacket(_commandSocket, ref errMsg);

            // If an app server is specified then create the socket here
            if (appServerPortNo != -1)
            {
                _appServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                ipHostInfo = null;
                ipHostInfo = Dns.GetHostAddresses(appServer);

                for (int i = 0; i <= ipHostInfo.Length - 1; i++)
                {
                    if (ipHostInfo[i].AddressFamily != AddressFamily.InterNetworkV6)
                    {
                        ipAddress = ipHostInfo[i];
                        break;
                    }
                }

                if (ipAddress == null)
                {
                    errMsg = $"Could not resolve the host name into the IP address for ApplicationServer {hostName}";
                    return SppaSocketStatus.SocketUnkownError;
                }

                appServerIPEndPoint = new IPEndPoint(ipAddress, appServerPortNo);

                // Set the socket timeouts
                _appServerSocket.ReceiveTimeout = 30000;
                _appServerSocket.SendTimeout = 30000;

                try
                {
                    _appServerSocket.Connect(appServerIPEndPoint);
                }
                catch (ArgumentNullException ane)
                {
                    errMsg = $"AppServerSocket->ArgumentNullException: Error = !{ane.Message}";
                    _appServerSocket.Close();
                    _appServerSocket = null;
                    return SppaSocketStatus.SocketArgumentError;
                }
                catch (SocketException se)
                {
                    errMsg = $"AppServerSocket->SocketException: Error = !{se.Message}";
                    _appServerSocket.Close();
                    _appServerSocket = null;
                    return SppaSocketStatus.SocketConnectError;
                }
                catch (Exception e)
                {
                    errMsg = $"AppServerSocket->Exception: Error = !{e.Message}";
                    _appServerSocket.Close();
                    _appServerSocket = null;
                    return SppaSocketStatus.SocketUnkownError;
                }

                // Umdocumented that SPPA will send an ALIVE packet after connection
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            return ret;
        }
        /// <summary>
        /// Open the specified project in the SPPA
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="time">current sim time</param>
        /// <param name="errMsg">returned error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaOpenProject(string projectName, double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            int pos = 0;
            int msgLength;
            int sendLength;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            UInt32 len;

            // Compute the data length and the xor of the data length.  Add 3 for the offline/online and the trailing null
            len = Convert.ToUInt32(projectName.Length + 3);

            // Populate standard header
            PopulateCommandHeader(time, SppaCommands.CMDID_OPEN_PROJECT, Two, len, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);
            pos = _cmdHeader.Length;

            //Now the name.  First put in a 0 for V2 to show this is online
            byte[] bZero = System.BitConverter.GetBytes(Zero);
            PasteBytesIntoBuffer(bZero, ref command, pos);
            pos += 2;
            byte[] name = Encoding.UTF8.GetBytes(projectName);
            PasteBytesIntoBuffer(name, ref command, pos);
            pos += projectName.Length + 1;

            sendLength = pos;
            cmdStatus = SendCommand(_commandSocket, command, sendLength, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 30000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }
            // Umdocumented that SPPA will send an ALIVE packet after connection
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null )
            {
                sendLength = pos;
                appStatus = SendCommand(_appServerSocket, command, sendLength, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 30000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }
                // Umdocumented that SPPA will send an ALIVE packet after connection
                GetAlivePacket(_appServerSocket, ref errMsg);

            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Open the SPPA simulation
        /// </summary>
        /// <param name="time">simulation time</param>
        /// <param name="errMsg">returned error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaOpenSimulation(double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int msgLength;
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            int pos = 0;

            // Populate standard header
            PopulateCommandHeader(time, SppaCommands.CMDID_OPEN_SIMULATION, One, Zero, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);

            cmdStatus = SendCommand(_commandSocket, command, _cmdHeader.Length, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, _cmdHeader.Length, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Close the SPPA simulation
        /// </summary>
        /// <param name="time">simulation time</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaCloseSimulation(double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int msgLength;
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            int pos = 0;

            _cmdSessionID = 0; // Per te documentation
            // Populate standard header
            PopulateCommandHeader(time, SppaCommands.CMDID_CLOSE_SIMULATION, One, Zero, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);

            cmdStatus = SendCommand(_commandSocket, command, _cmdHeader.Length, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 30000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, _cmdHeader.Length, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Close the SPPA Project
        /// </summary>
        /// <param name="time">simulation time</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaCloseProject(double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int msgLength;
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            int pos = 0;

            _cmdSessionID = 0; // Per te documentation
            // Populate standard header
            PopulateCommandHeader(time, SppaCommands.CMDID_CLOSE_PROJECT, One, Zero, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);

            cmdStatus = SendCommand(_commandSocket, command, _cmdHeader.Length, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 30000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, _cmdHeader.Length, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Close the SPPA Project
        /// </summary>
        /// <param name="time">simulation time</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaRun(double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int msgLength;
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            int pos = 0;

            // Determine if we are already running
            if (!_sppaFrozen)
            {
                return cmdStatus;
            }
            // Populate standard header
            PopulateCommandHeader(time, SppaCommands.CMDID_RUN, One, Zero, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);

            cmdStatus = SendCommand(_commandSocket, command, _cmdHeader.Length, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 2000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, _cmdHeader.Length, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Close the SPPA Project
        /// </summary>
        /// <param name="time">simulation time</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaFreeze(double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int msgLength;
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            int pos = 0;

            // Checkto see if we are already frozen
            if (_sppaFrozen)
            {
                return cmdStatus;
            }
            // Populate standard header
            PopulateCommandHeader(time, SppaCommands.CMDID_FREEZE, One, Zero, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);

            cmdStatus = SendCommand(_commandSocket, command, _cmdHeader.Length, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 2000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, _cmdHeader.Length, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Slow the Sppa simulation speed
        /// </summary>
        /// <param name="time">current simulation time</param>
        /// <param name="speed">desired speed</param>
        /// <param name="errMsg">returned error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaSlowTime(double time, UInt16 speed, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int msgLength;
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            UInt16 bigEndianSpeed;
            int pos = 0;
            UInt32 len;
            int sendLength;

            // Compute the data length and the xor of the data length.  Add 3 for the offline/online and the trailing null
            len = 2;
            PopulateCommandHeader(time, SppaCommands.CMDID_SLOW_MODE, One, len, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);
            pos = _cmdHeader.Length;

            //Now the name.  First put in a 0 for V2 to show this is online
            bigEndianSpeed = ReverseEndiannessV2(speed);
            byte[] bSpeed = System.BitConverter.GetBytes(bigEndianSpeed);
            PasteBytesIntoBuffer(bSpeed, ref command, pos);
            pos += 2;
            sendLength = pos;

            cmdStatus = SendCommand(_commandSocket, command, sendLength, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 2000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, sendLength, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Close the SPPA Project
        /// </summary>
        /// <param name="time">simulation time</param>
        /// <param name="errMsg">return error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaFastTime(double time, UInt16 speed, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int msgLength;
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            UInt16 bigEndianSpeed;
            int pos = 0;
            UInt32 len;
            int sendLength;

            // Compute the data length and the xor of the data length.  Add 3 for the offline/online and the trailing null
            len = 2;
            PopulateCommandHeader(time, SppaCommands.CMDID_FAST_MODE, One, len, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);
            pos = _cmdHeader.Length;

            //Now the name.  First put in a 0 for V2 to show this is online
            bigEndianSpeed = ReverseEndiannessV2(speed);
            byte[] bSpeed = System.BitConverter.GetBytes(bigEndianSpeed);
            PasteBytesIntoBuffer(bSpeed, ref command, pos);
            pos += 2;
            sendLength = pos;

            cmdStatus = SendCommand(_commandSocket, command, sendLength, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 2000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, sendLength, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Slow the Sppa simulation speed
        /// </summary>
        /// <param name="time">current simulation time</param>
        /// <param name="speed">desired speed</param>
        /// <param name="errMsg">returned error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaSyncTime(double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int msgLength;
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            int pos = 0;

            // Compute the data length and the xor of the data length.  Add 3 for the offline/online and the trailing null
            PopulateCommandHeader(time, SppaCommands.CMDID_SYNC_TIME, One, Zero, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);
            pos = _cmdHeader.Length;

            cmdStatus = SendCommand(_commandSocket, command, pos, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 2000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, _cmdHeader.Length, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Open the specified project in the SPPA
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="time">current sim time</param>
        /// <param name="errMsg">returned error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaLoadSnapshot(string snapshotName, eGenericFrameworkAdapterSnapshotType snapType,  double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            int pos = 0;
            int msgLength;
            int sendLength;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            UInt32 len;
            string snapFolder = string.Empty;
            //DateTime nowDate = DateTime.Now;

            // Compute the data length and the xor of the data length.  Add 3 for the offline/online and the trailing null
            if (snapType == eGenericFrameworkAdapterSnapshotType.InitialCondition)
            {
                len = Convert.ToUInt32(snapshotName.Length + IcNamespaceLength + DateTimeLength + 1 );   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "IC";
            }
            else if (snapType == eGenericFrameworkAdapterSnapshotType.Overlay)
            {
                len = Convert.ToUInt32(snapshotName.Length + OverlayNamespaceLength + DateTimeLength + 1);   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "Overlay";
            }
            else
            {
                len = Convert.ToUInt32(snapshotName.Length + BacktrackNamespaceLength + DateTimeLength + 1);   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "Backtrack";
            }

            // Populate standard header
            _cmdSessionID++;
            _dataSessionID++;
            // According to the documentation we need to increment the session ID every time we load a snapshot
            PopulateCommandHeader(time, SppaCommands.CMDID_LOAD_IC, One, len, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);
            pos = _cmdHeader.Length;

            // Now the date and time
            byte[] bYear = System.BitConverter.GetBytes(_cmdHeader.Year);
            PasteBytesIntoBuffer(bYear, ref command, pos);
            pos += 2;
            byte[] bMonth = System.BitConverter.GetBytes(_cmdHeader.Month);
            PasteBytesIntoBuffer(bMonth, ref command, pos);
            pos += 2;
            byte[] bDayOfWeek = System.BitConverter.GetBytes(_cmdHeader.DayOfWeek);
            PasteBytesIntoBuffer(bDayOfWeek, ref command, pos);
            pos += 2;
            byte[] bDay = System.BitConverter.GetBytes(_cmdHeader.Day);
            PasteBytesIntoBuffer(bDay, ref command, pos);
            pos += 2;
            byte[] bHour = System.BitConverter.GetBytes(_cmdHeader.Hour);
            PasteBytesIntoBuffer(bHour, ref command, pos);
            pos += 2;
            byte[] bMinute = System.BitConverter.GetBytes(_cmdHeader.Minute);
            PasteBytesIntoBuffer(bMinute, ref command, pos);
            pos += 2;
            byte[] bSec = System.BitConverter.GetBytes(_cmdHeader.Second);
            PasteBytesIntoBuffer(bSec, ref command, pos);
            pos += 2;
            byte[] bMilli = System.BitConverter.GetBytes(_cmdHeader.MilliSecond);
            PasteBytesIntoBuffer(bMilli, ref command, pos);
            pos += 2;
            byte[] bSnapFolder = System.Text.Encoding.UTF8.GetBytes(snapFolder);
            PasteBytesIntoBuffer(bSnapFolder, ref command, pos);
            pos += snapFolder.Length + 1;
            byte[] bSnapName = System.Text.Encoding.UTF8.GetBytes(snapshotName);
            PasteBytesIntoBuffer(bSnapName, ref command, pos);
            pos += snapshotName.Length + 1;

            msgLength = (int)pos;
            sendLength = msgLength;
            cmdStatus = SendCommand(_commandSocket, command, sendLength, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 100000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, sendLength, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Open the specified project in the SPPA
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="time">current sim time</param>
        /// <param name="errMsg">returned error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaSaveSnapshot(string snapshotName, eGenericFrameworkAdapterSnapshotType snapType, double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            int pos = 0;
            int msgLength;
            int sendLength;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            UInt32 len;
            string snapFolder = string.Empty;
            //DateTime nowDate = DateTime.Now;

            // Compute the data length and the xor of the data length.  Add 3 for the offline/online and the trailing null
            if (snapType == eGenericFrameworkAdapterSnapshotType.InitialCondition)
            {
                len = Convert.ToUInt32(snapshotName.Length + IcNamespaceLength + 1);   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "IC";
            }
            else if (snapType == eGenericFrameworkAdapterSnapshotType.Overlay)
            {
                len = Convert.ToUInt32(snapshotName.Length + OverlayNamespaceLength + 1);   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "Overlay";
            }
            else
            {
                len = Convert.ToUInt32(snapshotName.Length + BacktrackNamespaceLength + 1);   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "Backtrack";
            }

            PopulateCommandHeader(time, SppaCommands.CMDID_SAVE_SNAPSHOT, One, len, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);
            pos = _cmdHeader.Length;

            byte[] bSnapFolder = System.Text.Encoding.UTF8.GetBytes(snapFolder);
            PasteBytesIntoBuffer(bSnapFolder, ref command, pos);
            pos += snapFolder.Length + 1;
            byte[] bSnapName = System.Text.Encoding.UTF8.GetBytes(snapshotName);
            PasteBytesIntoBuffer(bSnapName, ref command, pos);
            pos += snapshotName.Length + 1;

            msgLength = pos;
            sendLength = msgLength;

            cmdStatus = SendCommand(_commandSocket, command, sendLength, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 20000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, sendLength, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        /// <summary>
        /// Open the specified project in the SPPA
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="time">current sim time</param>
        /// <param name="errMsg">returned error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaDeleteSnapshot(string snapshotName, eGenericFrameworkAdapterSnapshotType snapType, double time, ref string errMsg)
        {
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            int pos = 0;
            int msgLength;
            int sendLength;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            UInt32 rcvCommandID;
            UInt32 rcvSequence;
            UInt32 rcvErrorCode;
            UInt32 uintVal;
            UInt32 len;
            string snapFolder = string.Empty;
            //DateTime nowDate = DateTime.Now;

            // Compute the data length and the xor of the data length.  Add 3 for the offline/online and the trailing null
            if (snapType == eGenericFrameworkAdapterSnapshotType.InitialCondition)
            {
                len = Convert.ToUInt32(snapshotName.Length + IcNamespaceLength + 1);   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "IC";
            }
            else if (snapType == eGenericFrameworkAdapterSnapshotType.Overlay)
            {
                len = Convert.ToUInt32(snapshotName.Length + OverlayNamespaceLength + 1);   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "Overlay";
            }
            else
            {
                len = Convert.ToUInt32(snapshotName.Length + BacktrackNamespaceLength + 1);   // Add 1 for the trailing 0 on the snapshot name
                snapFolder = "Backtrack";
            }

            PopulateCommandHeader(time, SppaCommands.CMDID_DELETE_FILE, One, len, ref _cmdSequenceID);
            _cmdSequenceID++;
            PasteBytesIntoBuffer(_cmdHeader.GetSppaHeader(), ref command, pos);
            pos = _cmdHeader.Length;

            byte[] bSnapFolder = System.Text.Encoding.UTF8.GetBytes(snapFolder);
            PasteBytesIntoBuffer(bSnapFolder, ref command, pos);
            pos += snapFolder.Length + 1;
            byte[] bSnapName = System.Text.Encoding.UTF8.GetBytes(snapshotName);
            PasteBytesIntoBuffer(bSnapName, ref command, pos);
            pos += snapshotName.Length + 1;

            msgLength = pos;
            sendLength = msgLength;

            cmdStatus = SendCommand(_commandSocket, command, sendLength, ref errMsg);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }

            msgLength = 0;
            cmdStatus = ReceiveCommandAck(_commandSocket, ref msgLength, ref receiveBuffer, ref errMsg, 30000);

            // If the receive is ok then decode the return error code
            if (cmdStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                // Decode the response from command.
                pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvCommandID = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvSequence = ReverseEndiannessV2(uintVal);
                pos += 4;
                uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                rcvErrorCode = ReverseEndiannessV2(uintVal);
                pos += 4;

                cmdStatus = (SppaCommandStatus)rcvErrorCode;
            }

            // After we get the ack the emulator sends an alive packet
            GetAlivePacket(_commandSocket, ref errMsg);

            if (_appServerSocket != null)
            {
                appStatus = SendCommand(_appServerSocket, command, sendLength, ref errMsg);
                if (appStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    return appStatus;
                }

                msgLength = 0;
                appStatus = ReceiveCommandAck(_appServerSocket, ref msgLength, ref receiveBuffer, ref errMsg, 1200000);

                // If the receive is ok then decode the return error code
                if (appStatus == SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    // Decode the response from command.
                    pos = _cmdAckHeader.Length;    // Set the position to the first data after the header
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvCommandID = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvSequence = ReverseEndiannessV2(uintVal);
                    pos += 4;
                    uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
                    rcvErrorCode = ReverseEndiannessV2(uintVal);
                    pos += 4;

                    appStatus = (SppaCommandStatus)rcvErrorCode;
                }

                // After we get the ack the emulator sends an alive packet
                GetAlivePacket(_appServerSocket, ref errMsg);
            }
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return cmdStatus;
            }
            else
            {
                return appStatus;
            }
        }
        public SppaCommandStatus SppaGetAlivePackets(ref string errMsg)
        {
            string errMsg2 = string.Empty;
            string errMsg3 = string.Empty;
            SppaCommandStatus cmdStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            SppaCommandStatus appStatus = SppaCommandStatus.ANYCMD_NO_ERROR;
            cmdStatus = GetAlivePacket(_commandSocket, ref errMsg2);
            appStatus = GetAlivePacket(_appServerSocket, ref errMsg3);
            if (cmdStatus != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                errMsg = errMsg2;
                return cmdStatus;
            }
            else
            {
                errMsg = errMsg3;
                return appStatus;
            }
        }
        /// <summary>
        /// Get the alive packet
        /// </summary>
        /// <param name="errMsg">out error mesage</param>
        /// <returns></returns>
        private SppaCommandStatus GetAlivePacket(Socket sock, ref string errMsg)
        {
            int msgLength = 0;
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] receiveBuffer = new byte[ResponseBufferSize];

            status = ReceiveCommandAck(sock, ref msgLength, ref receiveBuffer, ref errMsg, 500, true);
            if (status == SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                DecodeAliveData(receiveBuffer);
            }

            return status;
        }
        /// <summary>
        /// When we receive an alive packet we need the information contained
        /// </summary>
        /// <param name="size">The number of bytes in the data portion.  For our commands this will always be 0</param>
        /// <param name="receiveBuffer"></param>
        private void DecodeAliveData(byte[] receiveBuffer)
        {
            UInt32 size = _cmdAckHeader.Size;
            int pos = _cmdAckHeader.Length;
            int index;
            int extraLength;
            UInt32 uintVal;
            UInt16 ushortVal;
            StringBuilder sb = new StringBuilder();

            uintVal = BitConverter.ToUInt32(receiveBuffer, pos);
            _sppaStatus = (AliveStatus)ReverseEndiannessV2(uintVal);
            pos += 4;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _slowModeFactor = ReverseEndiannessV2(ushortVal);
            pos += 2;
            ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
            _fastModeFactor = ReverseEndiannessV2(ushortVal);
            pos += 2;
            for (index = 0; index < 8; index++)
            {
                ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
                _SppaStepTime[index] = ReverseEndiannessV2(ushortVal);
                pos += 2;
            }
            for (index = 0; index < 8; index++)
            {
                ushortVal = BitConverter.ToUInt16(receiveBuffer, pos);
                _SppaReplayTime[index] = ReverseEndiannessV2(ushortVal);
                pos += 2;
            }
            // Get the project name.  We need to add one to get rid of the trailing null that C++ requires.
            extraLength = pos - _cmdAckHeader.Length;
            extraLength++;
            if (((int)_cmdAckHeader.Size - extraLength) > 1)
            {
                // Copy the project name.  length is equal to the size minus the processed fields
                _aliveProjectName = Encoding.UTF8.GetString(receiveBuffer, pos, ((int)_cmdAckHeader.Size - extraLength));
            }
            else
            {
                _aliveProjectName = "";
            }

            if ((_sppaStatus & AliveStatus.ALIVESTS_PRJ_OPEN) == AliveStatus.ALIVESTS_PRJ_OPEN)
            {
                sb.Append($"Project Open({_aliveProjectName});");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_SIM_OPEN) == AliveStatus.ALIVESTS_SIM_OPEN)
            {
                sb.Append("Simulation Open;");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_SIM_IN_RUN) == AliveStatus.ALIVESTS_SIM_IN_RUN)
            {
                sb.Append("Simulation Running;");
                _sppaFrozen = false;
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_SIM_IN_FREEZE) == AliveStatus.ALIVESTS_SIM_IN_FREEZE)
            {
                _sppaFrozen = true;
                sb.Append("Simulation Frozen;");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_SLOWMODE) == AliveStatus.ALIVESTS_SLOWMODE)
            {
                sb.Append($"Slow Mode({_slowModeFactor});");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_FASTMODE) == AliveStatus.ALIVESTS_FASTMODE)
            {
                sb.Append($"Fast Mode({_fastModeFactor});");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_RECORDING) == AliveStatus.ALIVESTS_RECORDING)
            {
                sb.Append("Recording;");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_REPLAYING) == AliveStatus.ALIVESTS_REPLAYING)
            {
                sb.Append("Replaying;");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_STEPPING) == AliveStatus.ALIVESTS_STEPPING)
            {
                sb.Append("Stepping;");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_OFFLINE) == AliveStatus.ALIVESTS_OFFLINE)
            {
                sb.Append("Offline;");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_PI_INTERFACE_READY) == AliveStatus.ALIVESTS_PI_INTERFACE_READY)
            {
                sb.Append("SignalInterfaceReady;");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_PI_ALL_CFG_RECEIVED) == AliveStatus.ALIVESTS_PI_ALL_CFG_RECEIVED)
            {
                sb.Append("AllProcConfigReceived;");
            }
            if ((_sppaStatus & AliveStatus.ALIVESTS_PI_ALL_CFG_VALID) == AliveStatus.ALIVESTS_PI_ALL_CFG_VALID)
            {
                sb.Append("AllProcConfigAreValid;");
            }
            LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, sb.ToString().Trim());
        }
        /// <summary>
        ///  Close te sockets
        /// </summary>
        public void SppaClose()
        {
            _commandSocket.Close();
            _commandSocket = null;
            _dataSocket.Close();
            _dataSocket = null;
            if (_appServerSocket != null)
            {
                _appServerSocket.Close();
                _appServerSocket = null;
            }
        }
        /// <summary>
        /// Parse the HWConfig.dat file
        /// </summary>
        /// <param name="fileName">The file and path to the hwconfig.dat</param>
        /// <param name="errMsg">out error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaParseHWConfigFile(string fileName, ref string errMsg)
        {
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;

            _parser = new SppaHWConfigParser(fileName);
            if (!_parser.ParseHWConfigDatFile(ref _writeDataSize, ref _readDataSize, ref errMsg))
            {
                status = SppaCommandStatus.OBJ_NOT_FOUND;
            }
            else
            {
                EmuDefinition = _parser.EmuConfig;
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("List of available channels");
                foreach (string channel in EmuDefinition.InputApRackSlots)
                {
                    sb.AppendLine(channel);
                }
                LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, sb.ToString());
            }
            return status;
        }

        public SppaSocketStatus OpenDataPort(string hostName, int dataPortNumber, ref string errMsg)
        {
            SppaSocketStatus ret = SppaSocketStatus.OK;
            IPEndPoint dataIPEndPoint;

            _dataSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress[] ipHostInfo;
            IPAddress ipAddress = null;

            ipHostInfo = Dns.GetHostAddresses(hostName);
            for (int i = 0; i <= ipHostInfo.Length - 1; i++)
            {
                if (ipHostInfo[i].AddressFamily != AddressFamily.InterNetworkV6)
                {
                    ipAddress = ipHostInfo[i];
                    break;
                }
            }

            if (ipAddress == null)
            {
                errMsg = $"Could not resolve the host name into the IP address for EmulationComputer {hostName}";
                return SppaSocketStatus.SocketUnkownError;
            }

            dataIPEndPoint = new IPEndPoint(ipAddress, dataPortNumber);

            // Set the socket timeouts
            _dataSocket.ReceiveTimeout = 30000;
            _dataSocket.SendTimeout = 30000;

            try
            {
                _dataSocket.Connect(dataIPEndPoint);
            }
            catch (ArgumentNullException ane)
            {
                errMsg = $"dataSocket->ArgumentNullException: Error = !{ane.Message}";
                _dataSocket.Close();
                _dataSocket = null;
                LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                ret = SppaSocketStatus.SocketArgumentError;
            }
            catch (SocketException se)
            {
                errMsg = $"dataSocket->SocketException: Error = !{se.Message}";
                _dataSocket.Close();
                _dataSocket = null;
                LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                ret = SppaSocketStatus.SocketConnectError;
            }
            catch (Exception e)
            {
                errMsg = $"dataSocket->Exception: Error = !{e.Message}";
                _dataSocket.Close();
                _dataSocket = null;
                LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                ret = SppaSocketStatus.SocketUnkownError;
            }
            return ret;
        }

        /// <summary>
        /// Send the PROC_CONFIG packet
        /// </summary>
        /// <param name="time">current simulation time</param>
        /// <param name="errMsg">out error message</param>
        /// <returns></returns>
        public SppaCommandStatus SppaSendProcConfig(double time, ref string errMsg)
        {
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;
            int pos = 0;
            int msgLength;
            byte[] command = new byte[SendBufferSize];
            byte[] receiveBuffer = new byte[ResponseBufferSize];
            int receivedBytes = 0;
            int apCount;
            int rackCount;
            int slotCount;
            int apIndex = 0;
            int rackIndex = 0;
            int slotIndex = 0;
            UInt32 len;
            int pcNumber;
            int apNumber;
            int rackNumber;
            int slotNumber;
            
            // Calculate the length of the data.  This involves travesring the emuDef buffer
            apCount = (int)(SppaAPHeader.HeaderSize * EmuDefinition.InputApHeaders.Count);
            rackCount = (int)(SppaRackHeader.HeaderSize * EmuDefinition.InputRackHeaders.Count);
            slotCount = (int)(SppaSlotHeader.HeaderSize * EmuDefinition.InputSlotHeaders.Count);
            len = (UInt32)(SppaPCHeader.HeaderSize + apCount + rackCount + slotCount);

            PopulateDataHeader(time, SppaCommands.CMDID_PROC_CONFIG, One, len, ref _dataSequenceID);
            _dataSequenceID++;
            PasteBytesIntoBuffer(_dataHeader.GetSppaHeader(), ref command, pos);
            pos = _cmdHeader.Length;

            // Now populate the data portion
            PasteBytesIntoBuffer(EmuDefinition.InputPCHeaders[0].GetPCHeader(), ref command, pos);
            pos += (int)SppaPCHeader.HeaderSize;

            pcNumber = (int)EmuDefinition.InputPCHeaders[0].PCNumber;
            rackIndex = 0;
            slotIndex = 0;
            for (apIndex = 0; apIndex < EmuDefinition.InputApHeaders.Count; apIndex++)
            {
                apNumber = (int)EmuDefinition.InputApHeaders[apIndex].APNumber;
                PasteBytesIntoBuffer(EmuDefinition.InputApHeaders[apIndex].GetAPHeader(), ref command, pos);
                pos += (int)SppaAPHeader.HeaderSize;
                rackCount = (int)(EmuDefinition.InputApHeaders[apIndex].RackHeadCount) + rackIndex;
                for (; rackIndex < rackCount; rackIndex++)
                {
                    rackNumber = (int)EmuDefinition.InputRackHeaders[rackIndex].RackNumber;
                    PasteBytesIntoBuffer(EmuDefinition.InputRackHeaders[rackIndex].GetRackHeader(), ref command, pos);
                    pos += (int)SppaRackHeader.HeaderSize;
                    slotCount = (int)(EmuDefinition.InputRackHeaders[rackIndex].SlotHeadCount + slotIndex);
                    for (; slotIndex < slotCount; slotIndex++)
                    {
                        slotNumber = (int)EmuDefinition.InputSlotHeaders[slotIndex].SlotNumber;
                        PasteBytesIntoBuffer(EmuDefinition.InputSlotHeaders[slotIndex].GetSlotHeader(), ref command, pos);
                        pos += (int)SppaSlotHeader.HeaderSize;
                    }
                }
            }

            msgLength = pos;
            status = SendDataPacket(command, msgLength, ref errMsg);
            if (status != SppaCommandStatus.ANYCMD_NO_ERROR)
            {
                return status;
            }

            status = ReceiveDataPackets(ref receivedBytes, ref receiveBuffer, ref errMsg, 1000, true);

            return status;
        }
        public SppaCommandStatus SppaGetProcData(ref string errMsg)
        {
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;
            int receivedBytes = 0;
            byte[] buffer = new byte[ResponseBufferSize];
            int pos;
            UInt32 len;
            SppaSlotHeader slot;
            int dataIndex;
            UInt32 uiVal;
            float fVal;
            UInt16 usVal;
            Int16 sVal;
            Int32 iVal;
            byte[] bytes;
            int timeout = 0;

            lock (_emuLock)
            {
                if (_fastModeFactor > 0)
                {
                    timeout = (int)((StepSize * 1000) / _fastModeFactor);
                }
                else if (_slowModeFactor > 0)
                {
                    timeout = (int)((StepSize * 1000) * _slowModeFactor);
                }
                else
                {
                    timeout = (int)(StepSize * 1000);
                }
                try
                {
                    if (_readIndices.Count == 0)
                    {
                        return status;
                    }
                    //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, $"bytes = {receivedBytes}, err = {errMsg}");
                    status = ReceiveDataPackets(ref receivedBytes, ref buffer, ref errMsg, timeout);
                    if (status != SppaCommandStatus.ANYCMD_NO_ERROR)
                    {
                        return status;
                    }
                    //DecodeDataHeader(buffer);
                }
                catch (Exception ex1)
                {
                    LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Error receiving data: {ex1.Message}");
                }

                LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, "unpacking read data");
                if ((SppaCommands)_dataHeader.CommandID == SppaCommands.CMDID_PROC_DATA)
                {
                    pos = _dataHeader.Length;
                    len = _dataHeader.Size;

                    // Now loop through the slots to get the values
                    for (int index = 0; index < EmuDefinition.OutputSlotHeaders.Count; index++)
                    {
                        slot = EmuDefinition.OutputSlotHeaders[index];

                        switch ((ChannelType)slot.ChannelType)
                        {
                            case ChannelType.Bool:
                                for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                                {
                                    slot.SetValue(Convert.ToBoolean(buffer[pos]), dataIndex);
                                    pos++;
                                }
                                break;
                            case ChannelType.Float:
                                for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                                {
                                    uiVal = BitConverter.ToUInt32(buffer, pos);
                                    uiVal = ReverseEndiannessV2(uiVal);
                                    bytes = BitConverter.GetBytes(uiVal);
                                    fVal = BitConverter.ToSingle(bytes, 0);
                                    slot.SetValue(fVal, dataIndex);
                                    pos += 4;
                                    bytes = null;
                                }
                                break;
                            case ChannelType.Uint8:
                                for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                                {
                                    slot.SetValue(buffer[pos], dataIndex);
                                    pos++;
                                }
                                break;
                            case ChannelType.Uint16:
                                for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                                {
                                    usVal = BitConverter.ToUInt16(buffer, pos);
                                    usVal = ReverseEndiannessV2(usVal);
                                    slot.SetValue(usVal, dataIndex);
                                    pos += 2;
                                }
                                break;
                            case ChannelType.UInt32:
                                for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                                {
                                    uiVal = BitConverter.ToUInt32(buffer, pos);
                                    uiVal = ReverseEndiannessV2(uiVal);
                                    slot.SetValue(uiVal, dataIndex);
                                    pos += 4;
                                }
                                break;
                            case ChannelType.Int8:
                                for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                                {
                                    slot.SetValue(Convert.ToSByte(buffer[pos]), dataIndex);
                                    pos++;
                                }
                                break;
                            case ChannelType.Int16:
                                for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                                {
                                    usVal = BitConverter.ToUInt16(buffer, pos);
                                    usVal = ReverseEndiannessV2(usVal);
                                    sVal = Convert.ToInt16(usVal);
                                    slot.SetValue(sVal, dataIndex);
                                    pos += 2;
                                }
                                break;
                            case ChannelType.Int32:
                                for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                                {
                                    uiVal = BitConverter.ToUInt32(buffer, pos);
                                    uiVal = ReverseEndiannessV2(uiVal);
                                    iVal = Convert.ToInt32(uiVal);
                                    slot.SetValue(iVal, dataIndex);
                                    pos += 4;
                                }
                                break;
                        }
                    }
                }
                if (FeedbackPoints != null)
                {
                    ProcessFeedbackPoints();
                }
            }
            return status;
        }
        public SppaCommandStatus SppaSendProcData(double time, ref string errMsg)
        {
            SppaCommandStatus status = SppaCommandStatus.ANYCMD_NO_ERROR;
            byte[] command = new byte[SendBufferSize];
            int pos = 0;
            UInt32 len;
            SppaSlotHeader slot;
            int dataIndex;
            UInt32 uiVal;
            float fVal;
            UInt16 usVal;
            Array vals;
            byte[] bytes;
            bool bVal;
            int len2 = 0;

            len = (UInt32)_parser.ReadSizeInBytes;
            PopulateDataHeader(time, SppaCommands.CMDID_PROC_DATA, One, len, ref _dataSequenceID);
            _dataSequenceID++;
            //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"SppaSendProcData: session = {_dataHeader.SessionID}, sequence = {_dataHeader.SequenceID}");
            PasteBytesIntoBuffer(_dataHeader.GetSppaHeader(), ref command, pos);
            pos = _dataHeader.Length;

            lock (_emuLock)
            {
                // Now loop through the slots to get the values
                for (int index = 0; index < EmuDefinition.InputSlotHeaders.Count; index++)
                {
                    slot = EmuDefinition.InputSlotHeaders[index];

                    switch ((ChannelType)slot.ChannelType)
                    {
                        case ChannelType.Bool:
                            vals = slot.GetAllValues();
                            len2 += (int)slot.ChannelCount;
                            for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                            {
                                bVal = (bool)vals.GetValue(dataIndex);
                                if (bVal)
                                {
                                    command[pos] = 1;
                                }
                                else
                                {
                                    command[pos] = 0;
                                }
                                pos++;
                            }
                            Array.Clear(vals, 0, vals.Length);
                            vals = null;
                            break;
                        case ChannelType.Float:
                            vals = slot.GetAllValues();
                            len2 += (int)slot.ChannelCount * 4;
                            for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                            {
                                fVal = Convert.ToSingle(vals.GetValue(dataIndex));
                                bytes = BitConverter.GetBytes(fVal);
                                uiVal = BitConverter.ToUInt32(bytes, 0);
                                uiVal = ReverseEndiannessV2(uiVal);
                                bytes = null;

                                bytes = BitConverter.GetBytes(uiVal);
                                PasteBytesIntoBuffer(bytes, ref command, pos);
                                pos += 4;
                                bytes = null;
                            }
                            Array.Clear(vals, 0, vals.Length);
                            vals = null;
                            break;
                        case ChannelType.Uint8:
                            vals = slot.GetAllValues();
                            len2 += (int)slot.ChannelCount;
                            for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                            {
                                command[pos] = (byte)vals.GetValue(dataIndex);
                                pos++;
                            }
                            Array.Clear(vals, 0, vals.Length);
                            vals = null;
                            break;
                        case ChannelType.Uint16:
                            vals = slot.GetAllValues();
                            len2 += (int)slot.ChannelCount * 2;
                            for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                            {
                                usVal = Convert.ToUInt16(vals.GetValue(dataIndex));
                                usVal = ReverseEndiannessV2(usVal);
                                bytes = BitConverter.GetBytes(usVal);
                                PasteBytesIntoBuffer(bytes, ref command, pos);
                                pos += 2;
                                bytes = null;
                            }
                            Array.Clear(vals, 0, vals.Length);
                            vals = null;
                            break;
                        case ChannelType.UInt32:
                            vals = slot.GetAllValues();
                            len2 += (int)slot.ChannelCount * 4;
                            for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                            {
                                uiVal = Convert.ToUInt32(vals.GetValue(dataIndex));
                                uiVal = ReverseEndiannessV2(uiVal);
                                bytes = BitConverter.GetBytes(uiVal);
                                PasteBytesIntoBuffer(bytes, ref command, pos);
                                pos += 4;
                                bytes = null;
                            }
                            Array.Clear(vals, 0, vals.Length);
                            vals = null;
                            break;
                        case ChannelType.Int8:
                            vals = slot.GetAllValues();
                            len2 += (int)slot.ChannelCount;
                            for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                            {
                                command[pos] = (byte)vals.GetValue(dataIndex);
                                pos++;
                            }
                            Array.Clear(vals, 0, vals.Length);
                            vals = null;
                            break;
                        case ChannelType.Int16:
                            vals = slot.GetAllValues();
                            len2 += (int)slot.ChannelCount * 2;
                            for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                            {
                                usVal = Convert.ToUInt16(vals.GetValue(dataIndex));
                                usVal = ReverseEndiannessV2(usVal);
                                bytes = BitConverter.GetBytes(usVal);
                                PasteBytesIntoBuffer(bytes, ref command, pos);
                                pos += 2;
                                bytes = null;
                            }
                            Array.Clear(vals, 0, vals.Length);
                            vals = null;
                            break;
                        case ChannelType.Int32:
                            vals = slot.GetAllValues();
                            len2 += (int)slot.ChannelCount * 4;
                            for (dataIndex = 0; dataIndex < slot.ChannelCount; dataIndex++)
                            {
                                uiVal = Convert.ToUInt32(vals.GetValue(dataIndex));
                                uiVal = ReverseEndiannessV2(uiVal);
                                bytes = BitConverter.GetBytes(uiVal);
                                PasteBytesIntoBuffer(bytes, ref command, pos);
                                pos += 4;
                                bytes = null;
                            }
                            Array.Clear(vals, 0, vals.Length);
                            vals = null;
                            break;
                    }
                }
            }
            status = SendDataPacket(command, pos, ref errMsg);

            return status;
        }
        public ArrayList ReadEmuConfigData()
        {
            ArrayList readVals = new ArrayList();
            int index;
            SppaSlotIODetails ioPoint;
            int conversionType;
            float euHi;
            float euLo;
            float maHi;
            float maLo;
            float fVal;
            float tmpfVal;
            float aiFval;

            //lock (_emuLock)
            {
                for (index = 0; index < _readIndices.Count; index++)
                {
                    ioPoint = _readIndices[index];
                    conversionType = ioPoint.conversionType;
                    euHi = ioPoint.euHi;
                    euLo = ioPoint.euLo;
                    maHi = ioPoint.maHi;
                    maLo = ioPoint.maLo;

                    var value = GetReadValue(ioPoint, false);
                    fVal = Convert.ToSingle(value);

                    if (conversionType > 0)
                    {
                        switch (conversionType)
                        {
                            case 1: // 0-20mA
                            case 2: // 4-20mA
                            case 5: // -20-20mA
                                tmpfVal = (fVal - maLo) / (maHi - maLo);
                                fVal = (tmpfVal * (euHi - euLo)) + euLo;
                                break;
                            case 3: // Field Value
                            case 4: // Flashing DI
                                break;
                        }
                    }

                    switch ((ChannelType)ioPoint.ChannelType)
                    {
                        case ChannelType.Bool:
                            readVals.Add(Convert.ToBoolean(fVal));
                            break;
                        case ChannelType.Float:
                            readVals.Add(Convert.ToSingle(fVal));
                            break;
                        case ChannelType.Uint8:
                            readVals.Add(Convert.ToByte(fVal));
                            break;
                        case ChannelType.Uint16:
                            readVals.Add(Convert.ToUInt16(fVal));
                            break;
                        case ChannelType.UInt32:
                            readVals.Add(Convert.ToUInt32(fVal));
                            break;
                        case ChannelType.Int8:
                            readVals.Add(Convert.ToSByte(fVal));
                            break;
                        case ChannelType.Int16:
                            readVals.Add(Convert.ToInt16(fVal));
                            break;
                        case ChannelType.Int32:
                            readVals.Add(Convert.ToInt32(fVal));
                            break;
                    }
                }
                // If there are feedback point process them here
                if (FeedbackPoints.Count > 0)
                {
                    foreach (FeedbackPoint fbPoint in FeedbackPoints.Values)
                    {
                        ioPoint = new SppaSlotIODetails();
                        ioPoint.ChannelIndex = fbPoint.AOChannel;
                        ioPoint.ChannelType = fbPoint.AOType;
                        ioPoint.SlotIndex = fbPoint.AOSlotIndex;

                        var value = GetReadValue(ioPoint, false);
                        fVal = Convert.ToSingle(value);

                        // Convert to 0-100
                        tmpfVal = (fVal - fbPoint.AOLo) / (fbPoint.AOHi - fbPoint.AOLo);
                        fVal = tmpfVal * 100;

                        for (index = 0; index < fbPoint.NumPointsInTable; index++)
                        {
                            aiFval = fbPoint.SiemensValues[index];
                            if (fVal <= aiFval)
                            {
                                fVal = fbPoint.UniSimValues[index];
                                break;
                            }
                        }
                        // Push the AI values back to Siemens
                        EmuDefinition.InputSlotHeaders[fbPoint.AI1SlotIndex].SetValue(fVal, fbPoint.AI1Channel);
                        EmuDefinition.InputSlotHeaders[fbPoint.AI2SlotIndex].SetValue(fVal, fbPoint.AI2Channel);
                    }
                }
            }
            return readVals;
        }
        public object GetReadValue(SppaSlotIODetails ioPoint, bool allSlots)
        {
            int slot;
            int channel;
            int type;
            object value = new object();

            slot = ioPoint.SlotIndex;
            channel = ioPoint.ChannelIndex;
            type = ioPoint.ChannelType;
            if (allSlots)
            {
                EmuDefinition.InputSlotHeaders[slot].GetValue(ref value, channel);
            }
            else
            {
                EmuDefinition.OutputSlotHeaders[slot].GetValue(ref value, channel);
            }

            return value;
        }
        public SppaCommandStatus WriteEmuConfigData(double time, ArrayList writeVals, ref string errMsg)
        {
            int index;
            int slot;
            int channel;
            int type;
            int conversionType;
            float euHi;
            float euLo;
            float maHi;
            float maLo;
            SppaSlotIODetails ioPoint;
            SppaSlotHeader slotHeader;
            SppaCommandStatus status;
            float fVal = 0.0F;
            float tmpfVal;

            //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, "WriteEmuConfigData: Packing data");
            //lock (_emuLock)
            {
                for (index = 0; index < writeVals.Count; index++)
                {
                    ioPoint = _writeIndices[index];
                    slot = ioPoint.SlotIndex;
                    channel = ioPoint.ChannelIndex;
                    type = ioPoint.ChannelType;
                    slotHeader = EmuDefinition.InputSlotHeaders[slot];
                    conversionType = ioPoint.conversionType;
                    euHi = ioPoint.euHi;
                    euLo = ioPoint.euLo;
                    maHi = ioPoint.maHi;
                    maLo = ioPoint.maLo;

                    fVal = Convert.ToSingle(writeVals[index]);
                    // Convert to mA if required
                    if (conversionType > 0)
                    {
                        switch (conversionType)
                        {
                            case 1: // 0-20mA
                            case 2: // 4-20mA
                            case 5: // -20-20mA
                                tmpfVal = (fVal - euLo) / (euHi - euLo);
                                fVal = (tmpfVal * (maHi - maLo)) + maLo;
                                //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Info, $"write: eu = {euLo}-{euHi}, fVal = {fVal}, orig = {writeVals[index]}");
                                break;
                            case 3: // Field Value
                            case 4: // Flashing DI
                                break;
                        }
                    }

                    //LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"index={index}, fVal = {fVal}, cha = {channel}");
                    switch ((ChannelType)type)
                    {
                        case ChannelType.Bool:
                            slotHeader.SetValue(Convert.ToBoolean(fVal), channel);
                            break;
                        case ChannelType.Float:
                            slotHeader.SetValue(Convert.ToSingle(fVal), channel);
                            break;
                        case ChannelType.Uint8:
                            slotHeader.SetValue(Convert.ToByte(fVal), channel);
                            break;
                        case ChannelType.Uint16:
                            slotHeader.SetValue(Convert.ToUInt16(fVal), channel);
                            break;
                        case ChannelType.UInt32:
                            slotHeader.SetValue(Convert.ToUInt32(fVal), channel);
                            break;
                        case ChannelType.Int8:
                            slotHeader.SetValue(Convert.ToSByte(fVal), channel);
                            break;
                        case ChannelType.Int16:
                            slotHeader.SetValue(Convert.ToInt16(fVal), channel);
                            break;
                        case ChannelType.Int32:
                            slotHeader.SetValue(Convert.ToInt32(fVal), channel);
                            break;
                    }
                    EmuDefinition.InputSlotHeaders[slot] = slotHeader;
                }
            }
            status = SppaSendProcData(CurrentTime, ref errMsg);
            return status;
        }
        public void DefineReadTags(List<string> fullTagNames, List<int> slotIndices, Dictionary<string, SppaSlotIODetails> conversionPointInfo)
        {
            // PC{0:D2}AP{1:D3}R{2:D3}S{3:D3}C{4:D2}T{5:D1}
            int channel;
            int type;
            int tagIndex = 0;
            int index = 0;
            string tag;

            for (index = 0; index < fullTagNames.Count; index++)
            {
                tag = fullTagNames[index];
                tagIndex = 18;
                channel = Convert.ToInt32(tag.Substring(tagIndex, 2));
                tagIndex = 21;
                type = Convert.ToInt32(tag.Substring(tagIndex, 1));

                //// Form the tag to get the index in the dictionary for this
                //tagIndex = tag.LastIndexOf("C");
                //tag = tag.Substring(0, tagIndex);

                //if (_readSlotIndices.TryGetValue(tag, out slotIndex))
                {
                    SppaSlotIODetails ioPoint = new SppaSlotIODetails();
                    ioPoint.SlotIndex = slotIndices[index];
                    ioPoint.ChannelIndex = channel;
                    ioPoint.ChannelType = type;
                    // Add conversion info
                    if (conversionPointInfo.ContainsKey(fullTagNames[index]))
                    {
                        SppaSlotIODetails tmpPoint = conversionPointInfo[fullTagNames[index]];
                        ioPoint.conversionType = tmpPoint.conversionType;
                        ioPoint.euHi = tmpPoint.euHi;
                        ioPoint.euLo = tmpPoint.euLo;
                        ioPoint.maHi = tmpPoint.maHi;
                        ioPoint.maLo = tmpPoint.maLo;
                    }
                    else
                    {
                        ioPoint.conversionType = 0;
                    }
                    _readIndices.Add(ioPoint);
                }
            }
        }
        public void DefineWriteTags(List<string> fullTagNames, List<int> slotIndices, Dictionary<string, SppaSlotIODetails> conversionPointInfo)
        {
            // PC{0:D2}AP{1:D3}R{2:D3}S{3:D3}C{4:D2}T{5:D1}
            int channel;
            int type;
            int tagIndex = 0;
            int index = 0;
            string tag;

            // Setup the write slot dictionary
            //SetWriteSlotIndices();

            for (index = 0; index < fullTagNames.Count; index++)
            {
                tag = fullTagNames[index];
                //tagIndex = 2;
                //pc = Convert.ToInt32(tag.Substring(tagIndex, 2));
                //tagIndex = 6;
                //ap = Convert.ToInt32(tag.Substring(tagIndex, 3));
                //tagIndex = 10;
                //rack = Convert.ToInt32(tag.Substring(tagIndex, 3));
                //tagIndex = 14;
                //slot = Convert.ToInt32(tag.Substring(tagIndex, 3));
                tagIndex = 18;
                channel = Convert.ToInt32(tag.Substring(tagIndex, 2));
                tagIndex = 21;
                type = Convert.ToInt32(tag.Substring(tagIndex, 1));

                // Form the tag to get the index in the dictionary for this
                //tagIndex = 17;
                //tag = tag.Substring(0, tagIndex);

                //if (_writeSlotIndices.TryGetValue(tag, out tagIndex))
                {
                    SppaSlotIODetails ioPoint = new SppaSlotIODetails();
                    ioPoint.SlotIndex = slotIndices[index];
                    ioPoint.ChannelIndex = channel;
                    ioPoint.ChannelType = type;
                    // Add conversion info
                    if (conversionPointInfo.ContainsKey(fullTagNames[index]))
                    {
                        SppaSlotIODetails tmpPoint = conversionPointInfo[fullTagNames[index]];
                        ioPoint.conversionType = tmpPoint.conversionType;
                        ioPoint.euHi = tmpPoint.euHi;
                        ioPoint.euLo = tmpPoint.euLo;
                        ioPoint.maHi = tmpPoint.maHi;
                        ioPoint.maLo = tmpPoint.maLo;
                    }
                    else
                    {
                        ioPoint.conversionType = 0;
                    }
                    _writeIndices.Add(ioPoint);
                }
            }
        }
        public void AddFeedbackPoints(Dictionary<string, FeedbackPoint> points)
        {
        }
        #endregion
    }
}
