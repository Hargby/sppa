﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    class SppaHWConfigParser
    {
        public string HWConfigDatFile { get; set; }
        public UInt32 PCCount { get; set; }
        public SppaEmuDefinition EmuConfig { get; set; }
        public int ReadSizeInBytes { get; set; }
        public int WriteSizeInBytes { get; set; }
        public SppaHWConfigParser()
        {
            PCCount = 0;
        }
        public SppaHWConfigParser(string file)
                :this()
        {
            HWConfigDatFile = file;
        }
        public bool ParseHWConfigDatFile(ref int readSize, ref int writeSize, ref string errMsg)
        {
            bool ret = true;
            int pos = 0;
            UInt32 pcIndex = 0;
            UInt32 pcNumber = 0;
            UInt32 apCount = 0;
            UInt32 apIndex = 0;
            UInt32 rackIndex = 0;
            UInt32 slotIndex = 0;
            int channelIndex;
            UInt32 uintVal;
            string slotName;
            SppaEmuDefinition emuDef = new SppaEmuDefinition();
            SppaPCHeader inPCHeader = null;
            SppaPCHeader outPCheader = null;
            SppaAPHeader apHeader = null;
            SppaRackHeader rackHeader = null;
            SppaSlotHeader slotHeader = null;
            SppaComms.ChannelType channelType;

            FileInfo fi = new FileInfo(HWConfigDatFile);
            if (!fi.Exists)
            {
                errMsg = $"Error: HWConfig.dat file not found.  Passed in file was {HWConfigDatFile}";
                ret = false;
            }
            else
            {
                byte[] allBytes = File.ReadAllBytes(HWConfigDatFile);

                // Get the number of PCs and create that number of PCDef classes
                uintVal = BitConverter.ToUInt32(allBytes, pos); 
                PCCount = SppaComms.ReverseEndiannessV2(uintVal);

                pos += 4;
                emuDef.PCCount = PCCount;

                // First loop through the PCs
                for (pcIndex = 0; pcIndex < PCCount; pcIndex++)
                {
                    // First the inputs
                    inPCHeader = new SppaPCHeader();
                    uintVal = BitConverter.ToUInt32(allBytes, pos);
                    pcNumber = SppaComms.ReverseEndiannessV2(uintVal);
                    pos += 4;
                    inPCHeader.PCNumber = pcNumber;

                    uintVal = BitConverter.ToUInt32(allBytes, pos);
                    apCount = SppaComms.ReverseEndiannessV2(uintVal);
                    pos += 4;
                    inPCHeader.APHeadCount = apCount;
                    emuDef.InputPCHeaders.Add(inPCHeader);

                    // Now loop through the InAPs for this PC
                    for (apIndex = 0; apIndex < apCount; apIndex++)
                    {
                        apHeader = new SppaAPHeader();
                        uintVal = BitConverter.ToUInt32(allBytes, pos);
                        apHeader.APNumber = SppaComms.ReverseEndiannessV2(uintVal);
                        pos += 4;
                        emuDef.InputApNumbers.Add(apHeader.APNumber);
                        uintVal = BitConverter.ToUInt32(allBytes, pos);
                        apHeader.RackHeadCount = SppaComms.ReverseEndiannessV2(uintVal);
                        pos += 4;
                        emuDef.InputApHeaders.Add(apHeader);

                        // Now loop through the InRacks fpr this PC
                        for (rackIndex = 0; rackIndex < apHeader.RackHeadCount; rackIndex++)
                        {
                            rackHeader = new SppaRackHeader();
                            uintVal = BitConverter.ToUInt32(allBytes, pos);
                            rackHeader.RackNumber = SppaComms.ReverseEndiannessV2(uintVal);
                            pos += 4;
                            emuDef.InputRackNumbers.Add(rackHeader.RackNumber);
                            uintVal = BitConverter.ToUInt32(allBytes, pos);
                            rackHeader.SlotHeadCount = SppaComms.ReverseEndiannessV2(uintVal);
                            pos += 4;
                            emuDef.InputRackHeaders.Add(rackHeader);

                            for (slotIndex = 0; slotIndex < rackHeader.SlotHeadCount; slotIndex++)
                            {
                                slotHeader = new SppaSlotHeader();
                                // Put the fully qualified slot name
                                uintVal = BitConverter.ToUInt32(allBytes, pos);
                                slotHeader.SlotNumber = SppaComms.ReverseEndiannessV2(uintVal);
                                emuDef.InputSlotNumbers.Add(slotHeader.SlotNumber);
                                pos += 4;
                                uintVal = BitConverter.ToUInt32(allBytes, pos);
                                slotHeader.ChannelCount = SppaComms.ReverseEndiannessV2(uintVal);
                                pos += 4;
                                uintVal = BitConverter.ToUInt32(allBytes, pos);
                                slotHeader.ChannelType = SppaComms.ReverseEndiannessV2(uintVal);
                                pos += 4;
                                // Create the data array for this slots channels
                                slotName = string.Format("PC{0:D2}AP{1:D3}R{2:D3}S{3:D3}", pcNumber, apHeader.APNumber, rackHeader.RackNumber, slotHeader.SlotNumber);
                                slotHeader.SlotName = slotName;

                                slotHeader.SetupChannelDataArea();
                                emuDef.InputSlotHeaders.Add(slotHeader);
                                channelType = (SppaComms.ChannelType)slotHeader.ChannelType;

                                // Define the data type and populate the initial buffers to the DCS with 0
                                switch (channelType)
                                {
                                    case SppaComms.ChannelType.Bool:
                                        readSize += (1 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < (int)slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.InputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                            slotHeader.SetValue(false, channelIndex);
                                        }
                                        break;
                                    case SppaComms.ChannelType.Float:
                                        readSize += (4 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < (int)slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.InputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                            slotHeader.SetValue(0.0F, channelIndex);
                                        }
                                        break;
                                    case SppaComms.ChannelType.Uint8:
                                        readSize += (1 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < (int)slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.InputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                            slotHeader.SetValue((byte)0, channelIndex);
                                        }
                                        break;
                                    case SppaComms.ChannelType.Uint16:
                                        readSize += (2 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < (int)slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.InputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                            slotHeader.SetValue((UInt16)0, channelIndex);
                                        }
                                        break;
                                    case SppaComms.ChannelType.UInt32:
                                        readSize += (4 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < (int)slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.InputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                            slotHeader.SetValue((UInt32)0, channelIndex);
                                        }
                                        break;
                                    case SppaComms.ChannelType.Int8:
                                        readSize += (1 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < (int)slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.InputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                            slotHeader.SetValue((sbyte)0, channelIndex);
                                        }
                                        break;
                                    case SppaComms.ChannelType.Int16:
                                        readSize += (2 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < (int)slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.InputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                            slotHeader.SetValue((Int16)0, channelIndex);
                                        }
                                        break;
                                    case SppaComms.ChannelType.Int32:
                                        readSize += (4 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < (int)slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.InputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                            slotHeader.SetValue((Int32)0, channelIndex);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

                // First loop through the PCs
                for (pcIndex = 0; pcIndex < PCCount; pcIndex++)
                {
                    // First the inputs
                    outPCheader = new SppaPCHeader();
                    uintVal = BitConverter.ToUInt32(allBytes, pos);
                    pcNumber = SppaComms.ReverseEndiannessV2(uintVal);
                    pos += 4;
                    outPCheader.PCNumber = pcNumber;

                    uintVal = BitConverter.ToUInt32(allBytes, pos);
                    apCount = SppaComms.ReverseEndiannessV2(uintVal);
                    pos += 4;
                    outPCheader.APHeadCount = apCount;
                    emuDef.OutputPCHeaders.Add(outPCheader);

                    // Now loop through the OutAPs for this PC
                    for (apIndex = 0; apIndex < apCount; apIndex++)
                    {
                        apHeader = new SppaAPHeader();
                        uintVal = BitConverter.ToUInt32(allBytes, pos);
                        apHeader.APNumber = SppaComms.ReverseEndiannessV2(uintVal);
                        pos += 4;
                        emuDef.OutputApNumbers.Add(apHeader.APNumber);
                        uintVal = BitConverter.ToUInt32(allBytes, pos);
                        apHeader.RackHeadCount = SppaComms.ReverseEndiannessV2(uintVal);
                        pos += 4;
                        emuDef.OutputApHeaders.Add(apHeader);

                        // Now loop through the InRacks fpr this PC
                        for (rackIndex = 0; rackIndex < apHeader.RackHeadCount; rackIndex++)
                        {
                            rackHeader = new SppaRackHeader();
                            uintVal = BitConverter.ToUInt32(allBytes, pos);
                            rackHeader.RackNumber = SppaComms.ReverseEndiannessV2(uintVal);
                            pos += 4;
                            emuDef.OutputRackNumbers.Add(rackHeader.RackNumber);
                            uintVal = BitConverter.ToUInt32(allBytes, pos);
                            rackHeader.SlotHeadCount = SppaComms.ReverseEndiannessV2(uintVal);
                            pos += 4;
                            emuDef.OutputRackHeaders.Add(rackHeader);

                            for (slotIndex = 0; slotIndex < rackHeader.SlotHeadCount; slotIndex++)
                            {
                                slotHeader = new SppaSlotHeader();
                                uintVal = BitConverter.ToUInt32(allBytes, pos);
                                slotHeader.SlotNumber = SppaComms.ReverseEndiannessV2(uintVal);
                                pos += 4;
                                emuDef.OutputSlotNumbers.Add(slotHeader.SlotNumber);
                                uintVal = BitConverter.ToUInt32(allBytes, pos);
                                slotHeader.ChannelCount = SppaComms.ReverseEndiannessV2(uintVal);
                                pos += 4;
                                uintVal = BitConverter.ToUInt32(allBytes, pos);
                                slotHeader.ChannelType = SppaComms.ReverseEndiannessV2(uintVal);
                                pos += 4;

                                // Create the data array for this slots channels
                                slotName = string.Format("PC{0:D2}AP{1:D3}R{2:D3}S{3:D3}", pcNumber, apHeader.APNumber, rackHeader.RackNumber, slotHeader.SlotNumber);
                                slotHeader.SlotName = slotName;
                                // Create the data array for this slots channels
                                slotHeader.SetupChannelDataArea();
                                emuDef.OutputSlotHeaders.Add(slotHeader);
                                channelType = (SppaComms.ChannelType)slotHeader.ChannelType;
                                switch (channelType)
                                {
                                    case SppaComms.ChannelType.Bool:
                                        writeSize += (1 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.OutputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                        }
                                        break;
                                    case SppaComms.ChannelType.Float:
                                        writeSize += (4 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.OutputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                        }
                                        break;
                                    case SppaComms.ChannelType.Uint8:
                                        writeSize += (1 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.OutputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                        }
                                        break;
                                    case SppaComms.ChannelType.Uint16:
                                        writeSize += (2 * (int)slotHeader.ChannelCount);
                                        for (channelIndex = 0; channelIndex < slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.OutputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                        }
                                        break;
                                    case SppaComms.ChannelType.UInt32:
                                        for (channelIndex = 0; channelIndex < slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.OutputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                        }
                                        writeSize += (4 * (int)slotHeader.ChannelCount);
                                        break;
                                    case SppaComms.ChannelType.Int8:
                                        for (channelIndex = 0; channelIndex < slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.OutputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                        }
                                        writeSize += (1 * (int)slotHeader.ChannelCount);
                                        break;
                                    case SppaComms.ChannelType.Int16:
                                        for (channelIndex = 0; channelIndex < slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.OutputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                        }
                                        writeSize += (2 * (int)slotHeader.ChannelCount);
                                        break;
                                    case SppaComms.ChannelType.Int32:
                                        for (channelIndex = 0; channelIndex < slotHeader.ChannelCount; channelIndex++)
                                        {
                                            // Create the tag identifier for this slot
                                            emuDef.OutputApRackSlots.Add($"PC{pcNumber:D2}AP{apHeader.APNumber:D3}R{rackHeader.RackNumber:D3}S{slotHeader.SlotNumber:D3}C{channelIndex:D2}T{(int)channelType:D1}");
                                        }
                                        writeSize += (4 * (int)slotHeader.ChannelCount);
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            // Now that we have populated this expose it
            EmuConfig = emuDef;
            ReadSizeInBytes = readSize;
            WriteSizeInBytes = writeSize;

            return ret;
        }
        public byte[] GetHWConfigDatFileData()
        {
            return new byte[1];
        }
    }
}
