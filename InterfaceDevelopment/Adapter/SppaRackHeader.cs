﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    class SppaRackHeader
    {
        public UInt32 RackNumber { get; set; }
        public UInt32 SlotHeadCount { get; set; }
        static public UInt32 HeaderSize { get; set; }

        public SppaRackHeader()
        {
            RackNumber = 0;
            SlotHeadCount = 0;
            HeaderSize = 8;
        }
        public byte[] GetRackHeader()
        {
            byte[] bytes = new byte[HeaderSize];
            int pos = 0;
            byte[] headerBytes;
            UInt32 uintVal;

            // Form the header buffer
            uintVal = SppaComms.ReverseEndiannessV2(RackNumber);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            uintVal = SppaComms.ReverseEndiannessV2(SlotHeadCount);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            return bytes;
        }
    }
}
