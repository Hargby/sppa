﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    class SppaAPHeader
    {
        public UInt32 APNumber { get; set; }
        public UInt32 RackHeadCount { get; set; }
        static public UInt32 HeaderSize { get; set; }

        public SppaAPHeader()
        {
            APNumber = 0;
            RackHeadCount = 0;
            HeaderSize = 8;
        }
        public byte[] GetAPHeader()
        {
            byte[] bytes = new byte[HeaderSize];
            int pos = 0;
            byte[] headerBytes;
            UInt32 uintVal;

            // Form the header buffer
            uintVal = SppaComms.ReverseEndiannessV2(APNumber);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            uintVal = SppaComms.ReverseEndiannessV2(RackHeadCount);
            headerBytes = System.BitConverter.GetBytes(uintVal);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;

            return bytes;
        }
    }
}
