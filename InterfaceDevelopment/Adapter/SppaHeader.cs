﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    class SppaHeader
    {
        public int Length { get; set; }
        // Packet header
        public UInt32 BitPattern1 { get; set; }
        public UInt32 BitPattern2 { get; set; }
        public UInt32 BitPattern3 { get; set; }
        public UInt32 VersionID { get; set; }
        public UInt32 SessionID { get; set; }
        public UInt32 Size { get; set; }
        public UInt32 SizeCompl { get; set; }
        public UInt32 CommandID { get; set; }
        public UInt32 CommandIDCompl { get; set; }
        public UInt32 SequenceID { get; set; }
        public UInt32 SequenceIDCompl { get; set; }
        public UInt32 Checksum { get; set; }
        public UInt32 ChecksumCompl { get; set; }

        // Date and Time
        public UInt16 Year { get; set; }
        public UInt16 Month { get; set; }
        public UInt16 DayOfWeek { get; set; }
        public UInt16 Day { get; set; }
        public UInt16 Hour { get; set; }
        public UInt16 Minute { get; set; }
        public UInt16 Second { get; set; }
        public UInt16 MilliSecond { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SppaHeader()
        {
            Length = 68;
        }
        /// <summary>
        /// Return the header in packet form
        /// </summary>
        /// <returns></returns>
        public byte[] GetSppaHeader()
        {
            byte[] bytes = new byte[Length];
            int pos = 0;
            byte[] headerBytes;

            // Form the header buffer
            headerBytes = System.BitConverter.GetBytes(BitPattern1);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(BitPattern2);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(BitPattern3);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(VersionID);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(SessionID);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(Size);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(SizeCompl);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(CommandID);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(CommandIDCompl);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(SequenceID);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(SequenceIDCompl);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(Checksum);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(ChecksumCompl);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 4;
            headerBytes = System.BitConverter.GetBytes(Year);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 2;
            headerBytes = System.BitConverter.GetBytes(Month);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 2;
            headerBytes = System.BitConverter.GetBytes(DayOfWeek);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 2;
            headerBytes = System.BitConverter.GetBytes(Day);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 2;
            headerBytes = System.BitConverter.GetBytes(Hour);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 2;
            headerBytes = System.BitConverter.GetBytes(Minute);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 2;
            headerBytes = System.BitConverter.GetBytes(Second);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 2;
            headerBytes = System.BitConverter.GetBytes(MilliSecond);
            SppaComms.PasteBytesIntoBuffer(headerBytes, ref bytes, pos);
            headerBytes = null;
            pos += 2;

            // Now put the length of the header into the Length property
            Length = pos;

            return bytes;
        }
    }
}
