﻿
using GenericFrameworkAdapterInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Collections;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

namespace GenericFrameworkAdapterInterface
{
    /// <summary>
    /// This is the adapter for the Siemens SPPA adapter.
    /// </summary>
    public class SPPAAdapter : GenericFrameworkAdapterBase
    {
        #region Private Fields
        private float _adapterSpeed;
        private bool _UseSpeedToRun;
        private bool _calledFromRun;
        private SppaComms _sComms = new SppaComms();
        private Thread _syncTimeThread;
        private Thread _aliveThread;
        private Thread _readWriteThread;
        private bool _snapshotOperationInProgress;
        private bool _shutdownAliveThread;
        private bool _shutdownTimeSyncThread;
        private bool _shutdownReadWriteThread;
        private bool _skipAliveProcessing;
        private bool _finishedInitialization;
        private string _configDatFile = string.Empty;
        private string _rangesFile = string.Empty;
        private List<int> _invalidReadIndices = new List<int>();
        private List<int> _invalidWriteIndices = new List<int>();
        private ArrayList _lastWriteValues = new ArrayList();
        private Dictionary<string, SppaSlotIODetails> _aiConversionInfo = new Dictionary<string, SppaSlotIODetails>();
        private Dictionary<string, SppaSlotIODetails> _aoConversionInfo = new Dictionary<string, SppaSlotIODetails>();
        private bool _stopReadWrite = false;
        List<string> _writeTags = new List<string>();
        List<string> _readTags = new List<string>();
        List<int> _writeSlotIndices = new List<int>();
        List<int> _readSlotIndices = new List<int>();
        Dictionary<string, SppaSlotIODetails> _readInputSlotDetails = new Dictionary<string, SppaSlotIODetails>();
        Dictionary<string, FeedbackPoint> _feedBackPoints = new Dictionary<string, FeedbackPoint>();
        #endregion
        #region Public Fields
        #endregion
        #region Protected Fields
        #endregion
        #region Constants
        // These are for example only!!!
        const int TagName = 0;
        const int OldValue = 1;
        const int NewValue = 2;
        const int ThirtySeconds = 30000;
        #endregion
        #region Properties
        public override float AdapterStepSize
        {
            get => base.AdapterStepSize;
            set
            {
                if (base.AdapterStepSize == 0.0F)
                {
                    base.AdapterStepSize = value;
                }
            }
        }
        #endregion
        #region Constructors, Destructors
        /// <summary>
        /// This is the default constructor for our adapter
        /// </summary>
        public SPPAAdapter()
            : base()
        {
            _adapterSpeed = 1.0F;
            _UseSpeedToRun = false;
            _calledFromRun = false;
            _snapshotOperationInProgress = false;
            _shutdownAliveThread = false;
            _shutdownTimeSyncThread = false;
            _shutdownReadWriteThread = false;
            _skipAliveProcessing = true;
            _finishedInitialization = false;
    }
    #endregion
    #region Private Methods
    /// <summary>
    /// Sync the simulation time.  This therad runs at a 30 second frquency
    /// </summary>
    private void SendSyncTime()
        {
            string errMsg = string.Empty;
            string errMsg2 = string.Empty;
            SppaComms.SppaCommandStatus status = SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR;

            while (!_shutdownTimeSyncThread)
            {
                // Sync time every 30 seconds
                Thread.Sleep(ThirtySeconds);
                // Only sync the time in run
                if (!IsFrozen && !_snapshotOperationInProgress)
                {
                    if (_sComms != null)
                    {
                        status = _sComms.SppaSyncTime(CurrentSimulationTime, ref errMsg);
                        if (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                        {
                            switch (status)
                            {
                                case SppaComms.SppaCommandStatus.SYNCTIME_INVALID_DATETIME:
                                    errMsg = "Sync Time: Part of date and time is invalid.";
                                    break;
                                case SppaComms.SppaCommandStatus.SYNCTIME_PRJ_NOT_OPEN:
                                    errMsg = "Sync Time: No project version has been opened.";
                                    break;
                                case SppaComms.SppaCommandStatus.SYNCTIME_PRJ_OFFLINE:
                                    errMsg = "Sync Time: Project version was opened offline.";
                                    break;
                                case SppaComms.SppaCommandStatus.SYNCTIME_SIM_NOT_OPEN:
                                    errMsg = "Sync Time: Simulation is not open.";
                                    break;
                                case SppaComms.SppaCommandStatus.SYNCTIME_COULDNT_FINISH:
                                    errMsg = "Sync Time: Error during command completion.";
                                    break;
                                case SppaComms.SppaCommandStatus.SYNCTIME_UNFINISHED_CMD:
                                    errMsg = "Sync Time: A previous command hasn't been completed yet.";
                                    break;
                                case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                                    errMsg2 = errMsg;
                                    errMsg = $"Sync Time: {errMsg}";
                                    break;
                                case SppaComms.SppaCommandStatus.SocketUnkownError:
                                    errMsg2 = errMsg;
                                    errMsg = $"Sync Time: {errMsg}";
                                    break;
                                default:
                                    errMsg = $"Sync Time: Unkown error code: {status.ToString()}.";
                                    break;
                            }
                            if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                                _sComms = null;
                            }
                            else
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                            }
                        }
                        else
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "Sync Time completed successfully");
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Get the status packets from Siemens
        /// </summary>
        private void GetAlivePacketsThread()
        {
            string errMsg = string.Empty;
            SppaComms.SppaCommandStatus status = SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR;

            while (!_shutdownAliveThread)
            {
                // Only check for alive packets if we are frozen and initialization has been finished
                if (IsFrozen && !_skipAliveProcessing && !_snapshotOperationInProgress)
                {
                    if (_sComms != null)
                    {
                        status = _sComms.SppaGetAlivePackets(ref errMsg);
                        if ((status != SppaComms.SppaCommandStatus.SocketTimeoutError) && (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR))
                        {
                            errMsg = "GetAlivePacketsThread: ReadData error ->" + errMsg;
                            if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                                _sComms = null;
                            }
                            else
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                            }
                        }
                    }
                }
                else
                {
                    Thread.Sleep(ThirtySeconds);
                }
            }
        }
        private void ReadWriteDataThread()
        {
            string errMsg = null;
            SppaComms.SppaCommandStatus status = SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR;
            bool firstError = true;

            while (!_shutdownReadWriteThread)
            {
                Thread.Sleep(Convert.ToInt32(AdapterStepSize * 1000));
                if (IsFrozen && !_snapshotOperationInProgress && !_stopReadWrite && (_readTags.Count > 0))
                {
                    try
                    {
                        if (_sComms != null && !_stopReadWrite)
                        {
                            //LogMessage(eGenericFrameworkAdapterErrorLevel.Info, "ReadWrite: SppaGetProcData");
                            status = _sComms.SppaGetProcData(ref errMsg);
                            if ((status != SppaComms.SppaCommandStatus.SocketTimeoutError) && (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR))
                            {
                                if (firstError)
                                {
                                    errMsg = "ReadWriteDataThread: ReadData error ->" + errMsg;
                                    if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                                    {
                                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                                        _sComms = null;
                                    }
                                    else
                                    {
                                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                                    }
                                    firstError = false;
                                }
                            }
                        }
                    }
                    catch (Exception readEx)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"ReadWriteThread: exception caught on read: {readEx.Message}", readEx);
                    }

                    try
                    {
                        if (_sComms != null && !_snapshotOperationInProgress && !_stopReadWrite && (_writeTags.Count > 0))
                        {
                            if (_lastWriteValues.Count > 0)
                            {
                                status = _sComms.WriteEmuConfigData(CurrentSimulationTime, _lastWriteValues, ref errMsg);
                                if ((status != SppaComms.SppaCommandStatus.SocketTimeoutError) && (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR))
                                {
                                    errMsg = "ReadWriteDataThread: WriteData error ->" + errMsg;
                                    if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                                    {
                                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                                        _sComms = null;
                                    }
                                    else
                                    {
                                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                                    }
                                }
                            }
                            else
                            {
                                status = _sComms.SppaSendProcData(CurrentSimulationTime, ref errMsg);
                                if ((status != SppaComms.SppaCommandStatus.SocketTimeoutError) && (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR))
                                {
                                    errMsg = "ReadWriteDataThread: WriteData error ->" + errMsg;
                                    if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                                    {
                                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                                        _sComms = null;
                                    }
                                    else
                                    {
                                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception writeEx)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"ReadWriteThread: exception caught on write: {writeEx.Message}", writeEx);
                    }
                }
            }
        }
        /// <summary>
        /// Receive messages from the comms layer and send to USO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogCommsMessage(object sender, EventArgs e)
        {
            GenericFrameworkAdapterEventArgs mea = e as GenericFrameworkAdapterEventArgs;
            LogMessage(mea.severity, mea.message);
        }
        #endregion
        #region Public Methods
        #endregion
        #region IGenericFrameworkAdapterAPI
        /// <summary>
        /// Parse the fields from the .dcsAdapter file
        /// </summary>
        /// <param name="names"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public override int ParseConfigurationData(string[] names, string[] data)
        {
            int index = 0;
            double myStepSize;
            string appServer = string.Empty;
            List<string> hosts = new List<string>();
            List<string> attributes = new List<string>();
            string aeProgID = string.Empty;
            // See the switch statement for the use of these 3 parameters
            string delimStr = ",";
            char[] delimiters = delimStr.ToCharArray(); // The Split method requires a char array
            string[] tokens;
            int cmdPortNumber = 0;
            int dataPortNumber = 0;
            int appServerPortNumber = -1;
            SppaComms.SppaSocketStatus initStatus;
            SppaComms.SppaCommandStatus cmdStatus;
            SppaComms.SppaCommandStatus dataStatus;
            string projectName = string.Empty;
            string errMsg = string.Empty;
            string errMsg2 = string.Empty;
            string aliveString = string.Empty;
            string feedbackFile = string.Empty;

            //Let the base class parse this first ...  The reason for this is that the base class handles the common parameters
            // such as HostName, ComputerName, StepSize, and StubMode.  
            base.ParseConfigurationData(names, data);

            // Since this is a dummy application simply log the parameters to the USO log.  In a real adapter this would
            // process the keywords to setup communications to the 3rd party system.  
            // NOTE: HostName and ComputerName are the same property (HostName)
            // So if you need more than 1 name then you need to use a keyword such as HostNames and provide a comma separated list
            for (index=0; index<names.Length; index++)
            {
                switch (names[index].ToUpper())
                {
                    // This is handled in base.  Each case would be a keyword from the .dcsAdapter file that is specific
                    // to this adapter.
                    case "STEPSIZE":
                        // Assign to a local or class variable from this keyword.  For numbers use the followinf syntax:
                        myStepSize = Double.Parse(data[index], CultureInfo.InvariantCulture.NumberFormat);
                        HandledDcsAdapterSettings.Add(names[index]);
                        AdapterStepSize = Convert.ToSingle(myStepSize);
                        break;
                    case "EMULATIONHOSTNAMES":
                        tokens = data[index].Split(delimiters); // use the split function to get the names
                        // Put the names into the list for later use
                        foreach (string hostName in tokens)
                        {
                            hosts.Add(hostName);
                        }
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                    case "COMMAND_PORTNUMBER":
                        cmdPortNumber = Convert.ToInt32(data[index]);
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                    case "DATA_PORTNUMBER":
                        dataPortNumber = Convert.ToInt32(data[index]);
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                    case "PROJECT_NAME":
                        projectName = data[index];
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                    case "CONFIG_FILE":
                        _configDatFile = data[index];
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                    case "APPLICATIONSERVER":
                        appServer = data[index];
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                    case "APPLICATIONSERVERPORTNUMBER":
                        appServerPortNumber = Convert.ToInt32(data[index]);
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                    case "RANGESFILE":
                        _rangesFile = data[index];
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                    case "FEEDBACK_FILE":
                        feedbackFile = data[index];
                        HandledDcsAdapterSettings.Add(names[index]);
                        break;
                }
            }

            // Call the base class to validate that all keywords have been processed
            ValidateAllConfigDataHandled(names, data);

            // Read the eu ranges file
            if (_rangesFile != string.Empty)
            {
               PopulateConversionInfo();
            }
            // Here is where you perform any connections or initialization to the 3rd party system.
            // Check if we are in stub mode.  If we are then skip connecting to the 3rd party system.
            if (!AdapterIsInStubMode && (_sComms != null))
            {
                // Register with the Comms event handler
                _sComms.CommsMessageLogged += new EventHandler(LogCommsMessage);
                _sComms.StepSize = AdapterStepSize;

                // Initialize the communications
                initStatus = _sComms.SppaInit(hosts[0], cmdPortNumber, appServer, appServerPortNumber, ref errMsg);
                if (initStatus != SppaComms.SppaSocketStatus.OK)
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, $"Init: Error is {errMsg}");
                    _sComms = null;
                }

                if (_sComms != null)
                {
                    // Open the project file
                    cmdStatus = _sComms.SppaOpenProject("Jazan", 0.0, ref errMsg);
                    if (cmdStatus != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                    {
                        switch (cmdStatus)
                        {
                            case SppaComms.SppaCommandStatus.OPENPRJ_INVALID_NAME:
                                errMsg = "Open_Project: Project name is invalid.";
                                break;
                            case SppaComms.SppaCommandStatus.OPENPRJ_INVALID_uintVal:
                                errMsg = "Open_Project: Project version is invalid.";
                                break;
                            case SppaComms.SppaCommandStatus.OPENPRJ_PRJ_OPEN:
                                errMsg = "Open_Project: A project has already been opened.";
                                break;
                            case SppaComms.SppaCommandStatus.OPENPRJ_OBJ_NOT_FOUND:
                                errMsg = "Open_Project: Project wasn’t found.";
                                break;
                            case SppaComms.SppaCommandStatus.OPENSIM_COULDNT_FINISH:
                                errMsg = "Open_Project: Error during command completion.";
                                break;
                            case SppaComms.SppaCommandStatus.OPENPRJ_UNFINISHED_CMD:
                                errMsg = "Open_Project: A previous command hasn't been completed yet.";
                                break;
                            case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                                errMsg2 = errMsg;
                                errMsg = $"Open_Project: {errMsg}";
                                break;
                            case SppaComms.SppaCommandStatus.SocketUnkownError:
                                errMsg2 = errMsg;
                                errMsg = $"Open_Project: {errMsg}";
                                break;
                            default:
                                errMsg = $"Open_Project: Unkown error code: {cmdStatus.ToString()}.";
                                break;
                        }
                        if (cmdStatus == SppaComms.SppaCommandStatus.SocketUnkownError)
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                            _sComms = null;
                        }
                        else
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                        }
                    }
                    else
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "OpenProject completed successfully");
                    }


                    Thread.Sleep(2000);

                    // Open the simulation
//                    if (cmdStatus == SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR && (_sComms != null))
                    if (_sComms != null)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "Sending OpenSimulation");
                        cmdStatus = _sComms.SppaOpenSimulation(0.0, ref errMsg);
                        if (cmdStatus != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                        {
                            switch (cmdStatus)
                            {
                                case SppaComms.SppaCommandStatus.OPENSIM_PRJ_NOT_OPEN:
                                    errMsg = "Open_Simulation: No project has been opened";
                                    break;
                                case SppaComms.SppaCommandStatus.OPENSIM_SIM_OPEN:
                                    errMsg = "Open_Simulation: A simulation already is open";
                                    break;
                                case SppaComms.SppaCommandStatus.OPENSIM_HWCONFIG_NOT_RECEIVED:
                                    errMsg = "PROC_CONFIG telegram not received";
                                    break;
                                case SppaComms.SppaCommandStatus.OPENSIM_HWCONFIG_INVALID:
                                    errMsg = "PROC_CONFIG telegram does not match to local file";
                                    break;
                                case SppaComms.SppaCommandStatus.OPENSIM_NO_PI_CONNECTION:
                                    errMsg = "Connection is not established";
                                    break;
                                case SppaComms.SppaCommandStatus.OPENSIM_COULDNT_FINISH:
                                    errMsg = "Error during command completion";
                                    break;
                                case SppaComms.SppaCommandStatus.OPENPRJ_UNFINISHED_CMD:
                                    errMsg = "A previous command hasn't been completed yet";
                                    break;
                                case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                                    errMsg2 = errMsg;
                                    errMsg = $"Open_Simulation: {errMsg}";
                                    break;
                                case SppaComms.SppaCommandStatus.SocketUnkownError:
                                    errMsg2 = errMsg;
                                    errMsg = $"Open_Simulation: {errMsg}";
                                    break;
                                default:
                                    errMsg = $"Open_Simulation: Unkown error code: {cmdStatus.ToString()}.";
                                    break;
                            }
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                            _sComms = null;
                        }
                        else
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "OpenSimulation completed successfully");
                        }
                    }
                    // Before the OPEN_SIMULATION we need to send the PROC_CONFIG
                    if (_configDatFile != string.Empty && (_sComms != null))
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Info, "Sleeping before Proc_Config");
                        System.Threading.Thread.Sleep(2000);
                        // Now read the hwConfig.dat file since we need it for the comms
                        dataStatus = _sComms.SppaParseHWConfigFile(_configDatFile, ref errMsg);
                        if (dataStatus != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                        {
                            switch (dataStatus)
                            {
                                case SppaComms.SppaCommandStatus.OBJ_NOT_FOUND:
                                    errMsg = $"ParseHWConfigFile: File not found: {_configDatFile}.";
                                    break;
                                case SppaComms.SppaCommandStatus.SocketUnkownError:
                                    break;
                                default:
                                    errMsg = $"ParseHWConfigFile: Unkown error code: {dataStatus.ToString()}.";
                                    break;
                            }
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Parse of Proc_Config file failed: {errMsg}");
                        }
                        else
                        {
                            int retryCount = 0;
                            bool done = false;
                            SppaComms.SppaSocketStatus sockStatus;

                            while (!done && (retryCount < 15))
                            {
                                Thread.Sleep(2000);
                                sockStatus = _sComms.OpenDataPort(hosts[0], dataPortNumber, ref errMsg);
                                if (sockStatus != SppaComms.SppaSocketStatus.OK)
                                {
                                    retryCount++;
                                }
                                else
                                {
                                    done = true;
                                }
                            }
                            if (!done)
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Severe, "Could not open data socket after 60 seconds");
                            }
                            // Since we succeeded then lets send a PROC_CONFIG to the SPPA
                            _sComms.SppaSendProcConfig(CurrentSimulationTime, ref errMsg);
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Info, "Proc_Config sent successfully");
                        }
                    }

                    // Process the feedback file
                    if (feedbackFile != string.Empty)
                    {
                        FileInfo fi = new FileInfo(feedbackFile);
                        if (fi.Exists)
                        {
                            ParseFeedbackFile(feedbackFile);
                            _sComms.FeedbackPoints = _feedBackPoints;
                        }
                    }
                    // Start the alive thread and the time sync thread
                    _aliveThread = new Thread(GetAlivePacketsThread);
                    _aliveThread.Start();
                    _syncTimeThread = new Thread(SendSyncTime);
                    _syncTimeThread.Start();
                    _readWriteThread = new Thread(ReadWriteDataThread);
                    _readWriteThread.Start();
                }
                _stopReadWrite = false;
            }
            return 0;
        }

        private void ParseFeedbackFile(string feedbackFile)
        {
            string strLine;
            string aoTag;
            string aiTag1;
            string aiTag2;
            string[] tokens;
            string[] valTokens;
            int numItems;
            float[] siemensValues;
            float[] uniSimValues;
            float aoLo;
            float aoHi;
            int index;
            int outSlotIndex;
            int aoSlotIndex;
            int ai1SlotIndex;
            int ai2SlotIndex;
            string tagSlotName;
            int aoTagType;
            int ai1TagType;
            int ai2TagType;
            int tagIndex;
            int aoChannel;
            int ai1Channel;
            int ai2Channel;

            using (StreamReader sr = new StreamReader(feedbackFile))
            {
                while ((strLine = sr.ReadLine()) != null)
                {
                    if (strLine.StartsWith("#"))
                    {
                        continue;
                    }
                    tokens = strLine.Split(',');

                    FeedbackPoint fbPoint = new FeedbackPoint(Convert.ToInt32(tokens[5]));
                    aoTag = tokens[0];
                    if (_sComms.EmuDefinition.OutputApRackSlots.Contains(aoTag))
                    {
                        outSlotIndex = aoTag.LastIndexOf('C');
                        aoSlotIndex = 0;

                        tagIndex = 18;
                        aoChannel = Convert.ToInt32(aoTag.Substring(tagIndex, 2));
                        tagIndex = 21;
                        aoTagType = Convert.ToInt32(aoTag.Substring(tagIndex, 1));

                        // Verify the channel and tag type
                        tagSlotName = aoTag.Substring(0, outSlotIndex);
                        foreach (SppaSlotHeader sh in _sComms.EmuDefinition.OutputSlotHeaders)
                        {
                            if (tagSlotName == sh.SlotName)
                            {
                                if ((int)sh.ChannelType == aoTagType)
                                {
                                    break;
                                }
                            }
                            aoSlotIndex++;
                        }
                        aoLo = Convert.ToSingle(tokens[1]);
                        aoHi = Convert.ToSingle(tokens[2]);
                        aiTag1 = tokens[3];

                        tagIndex = 18;
                        ai1Channel = Convert.ToInt32(aiTag1.Substring(tagIndex, 2));
                        tagIndex = 21;
                        ai1TagType = Convert.ToInt32(aiTag1.Substring(tagIndex, 1));

                        // Verify the channel and tag type
                        tagSlotName = aiTag1.Substring(0, 17);
                        ai1SlotIndex = 0;
                        foreach (SppaSlotHeader sh in _sComms.EmuDefinition.InputSlotHeaders)
                        {
                            if (tagSlotName == sh.SlotName)
                            {
                                if ((int)sh.ChannelType == ai1TagType)
                                {
                                    break;
                                }
                            }
                            ai1SlotIndex++;
                        }
                        aiTag2 = tokens[4];

                        tagIndex = 18;
                        ai2Channel = Convert.ToInt32(aiTag2.Substring(tagIndex, 2));
                        tagIndex = 21;
                        ai2TagType = Convert.ToInt32(aiTag2.Substring(tagIndex, 1));

                        // Verify the channel and tag type
                        tagSlotName = aiTag2.Substring(0, 17);
                        ai2SlotIndex = 0;
                        foreach (SppaSlotHeader sh in _sComms.EmuDefinition.InputSlotHeaders)
                        {
                            if (tagSlotName == sh.SlotName)
                            {
                                if ((int)sh.ChannelType == ai2TagType)
                                {
                                    break;
                                }
                            }
                            ai2SlotIndex++;
                        }
                        numItems = Convert.ToInt32(tokens[5]);
                        siemensValues = new float[numItems];
                        uniSimValues = new float[numItems];
                        for (index = 6; index < tokens.Length; index++)
                        {
                            valTokens = tokens[index].Split(':');
                            if (valTokens != null)
                            {
                                siemensValues[index - 6] = Convert.ToSingle(valTokens[0]);
                                uniSimValues[index - 6] = Convert.ToSingle(valTokens[1]);
                            }
                        }
                        fbPoint.AOPoint = aoTag;
                        fbPoint.AOSlotIndex = aoSlotIndex;
                        fbPoint.AOChannel = aoChannel;
                        fbPoint.AOType = aoTagType;
                        fbPoint.AOLo = aoLo;
                        fbPoint.AOHi = aoHi;
                        fbPoint.AIPoint1 = aiTag1;
                        fbPoint.AIPoint2 = aiTag2;
                        fbPoint.AI1Channel = ai1Channel;
                        fbPoint.AI1SlotIndex = ai1SlotIndex;
                        fbPoint.AI2Channel = ai2Channel;
                        fbPoint.AI2SlotIndex = ai2SlotIndex;fbPoint.AI1Type = ai1TagType;
                        fbPoint.AI2Type = ai2TagType;
                        fbPoint.SiemensValues = siemensValues;
                        fbPoint.UniSimValues = uniSimValues;
                        _feedBackPoints.Add(aoTag, fbPoint);
                    }
                    else
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Invalid AO tag specified in feedback file: {aoTag}");
                    }
                }
            }
        }

        private void PopulateConversionInfo()
        {
            string line;
            string[] tokens;

            using (StreamReader sr = new StreamReader(_rangesFile))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.StartsWith("#"))
                    {
                        continue;
                    }
                    tokens = line.Split(',');

                    SppaSlotIODetails ioPoint = new SppaSlotIODetails();
                    ioPoint.euHi = Convert.ToSingle(tokens[1]);
                    ioPoint.euLo = Convert.ToSingle(tokens[2]);
                    switch (tokens[3].ToUpper())
                    {
                        case "0-20MA":
                            ioPoint.conversionType = 1;
                            ioPoint.maHi = 20.0F;
                            ioPoint.maLo = 0.0F;
                            break;
                        case "4-20MA":
                            ioPoint.conversionType = 2;
                            ioPoint.maHi = 20.0F;
                            ioPoint.maLo = 4.0F;
                            break;
                        case "FIELDVALUE":
                            ioPoint.conversionType = 3;
                            break;
                        case "FLASHINGDESKTILE":
                            ioPoint.conversionType = 4;
                            break;
                        case "-20MA-20MA":
                            ioPoint.conversionType = 5;
                            ioPoint.maHi = 20.0F;
                            ioPoint.maLo = -20.0F;
                            break;
                        default:
                            ioPoint.conversionType = 0;
                            break;
                    }
                    if (tokens[5].ToUpper() == "AI")
                    {
                        _aiConversionInfo.Add(tokens[4], ioPoint);
                    }
                    else
                    {
                        _aoConversionInfo.Add(tokens[4], ioPoint);
                    }
                }
            }
        }

        /// <summary>
        /// Read data from the DCS and send the values back to USO
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public override int ReadFromAdapter(out ArrayList values)
        {
            int index = 0;
            object oVal = new object();
            values = new ArrayList();
            ArrayList readValues = null;
            string errMsg = string.Empty;
            SppaComms.SppaCommandStatus status;
            SppaSlotIODetails ioPoint;
            GenericFrameworkAdapterTag inTag;

            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, "Received a read command");
            if (!AdapterIsInStubMode && (_sComms != null))
            {
                status = _sComms.SppaGetProcData(ref errMsg);
                if ((status != SppaComms.SppaCommandStatus.SocketTimeoutError) && (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR))
                {
                    errMsg = "ReadFromAdapter: ReadData error ->" + errMsg;
                    if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                        _sComms = null;
                        return 1;
                    }
                    else
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                    }
                }
            }

            // use whatever method we need to get data from the 3rd pary system if not in stub mode
            if (!AdapterIsInStubMode && (_sComms != null))
            {
                // Read from the DCS
                readValues = _sComms.ReadEmuConfigData();

                //lastReadValues = readValues;
                // Now put the values into the ArrayList
                foreach (GenericFrameworkAdapterTag tag in TagsFromDCS.Values)
                {
                    // If this is an invalid tag then skip it
                    if (_invalidReadIndices.Contains(index))
                    {
                        if (tag.TagType == eGenericFrameworkAdapterTagType.IntegerType)
                        {
                            values.Add(-999999);
                        }
                        else
                        {
                            values.Add(-9999.9999F);
                        }
                    }
                    //If this is a input being read as an output get the value then populate the tag
                    else if (_readInputSlotDetails.ContainsKey(tag.TagName))
                    {
                        ioPoint = _readInputSlotDetails[tag.TagName];
                        if (ioPoint.ChannelType == (int)ChannelType.Float)
                        {
                            try
                            {
                                if (TagsToDCS.TryGetValue(tag.TagName, out inTag))
                                {
                                    tag.FloatValue = inTag.FloatValue;
                                    values.Add(tag.FloatValue);  // Replace this with the value from the 3rd party system at this index
                                }
                                else
                                {
                                    tag.FloatValue = -9999.99F;
                                    values.Add(-9999.99F);
                                }
                            }
                            catch (Exception intEx1)
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Error1 converting {tag.TagName} to float.  Value = {oVal}.  Error = {intEx1.Message}");
                            }
                        }
                        else
                        {
                            try
                            {
                                if (TagsToDCS.TryGetValue(tag.TagName, out inTag))
                                {
                                    tag.IntegerValue = inTag.IntegerValue;
                                    values.Add(tag.IntegerValue);  // Replace this with the value from the 3rd party system at this index
                                }
                                else
                                {
                                    tag.IntegerValue = -9999;
                                    values.Add(-9999);
                                }
                            }
                            catch (Exception floatEx1)
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Error1 converting {tag.TagName} to int.  Value = {oVal}.  Error = {floatEx1.Message}");
                            }
                        }
                    }
                    // This is a normal tag
                    else
                    {
                        if (tag.TagType == eGenericFrameworkAdapterTagType.IntegerType)
                        {
                            try
                            {
                                tag.IntegerValue = Convert.ToInt32(readValues[index]);  // Replace this with the value from the 3rd party system at this index
                                values.Add(tag.IntegerValue);  // Replace this with the value from the 3rd party system at this index
                            }
                            catch (Exception intEx2)
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Error2 converting {tag.TagName} to int.  Value = {oVal}.  Error = {intEx2.Message}");
                            }
                        }
                        else if (tag.TagType == eGenericFrameworkAdapterTagType.FloatType)
                        {
                            try
                            {
                                tag.FloatValue = Convert.ToSingle(readValues[index]);  // Replace this with the value from the 3rd party system at this index
                                values.Add(tag.FloatValue);  // Replace this with the value from the 3rd party system at this index
                            }
                            catch (Exception floatEx2)
                            {
                                LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Error2 converting {tag.TagName} to float.  Value = {oVal}.  Error = {floatEx2.Message}");
                            }
                        }
                        index++;
                    }
                }
            }
            return 0;
        }
        /// <summary>
        /// Get the values to be written from USO and pass them on to the 3rd party system
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public override int WriteToAdapter(ArrayList values)
        {
            int index = 0;
            ArrayList writeVals = new ArrayList();
            string errMsg = string.Empty;
            SppaComms.SppaCommandStatus status;
            _lastWriteValues.Clear();

            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, "Received a write command");
            // Do whatever needs to be done to write values to the 3rd party system
            if (!AdapterIsInStubMode && (_sComms != null))
            {
                foreach (GenericFrameworkAdapterTag tag in TagsToDCS.Values)
                {
                    if (!_invalidWriteIndices.Contains(index))
                    {
                        if (tag.TagType == eGenericFrameworkAdapterTagType.IntegerType)
                        {
                            tag.IntegerValue = Convert.ToInt32(values[index]);
                            writeVals.Add(tag.IntegerValue);
                            _lastWriteValues.Add(tag.IntegerValue);
                        }
                        else if (tag.TagType == eGenericFrameworkAdapterTagType.FloatType)
                        {
                            tag.FloatValue = Convert.ToSingle(values[index]);
                            writeVals.Add(tag.FloatValue);
                            _lastWriteValues.Add(tag.FloatValue);
                        }
                    }
                    index++;
                }

                // Write to the 3rd party system
                status = _sComms.WriteEmuConfigData(CurrentSimulationTime, writeVals, ref errMsg);
                if ((status != SppaComms.SppaCommandStatus.SocketTimeoutError) && (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR))
                {
                    errMsg = "ReadWriteDataThread: WriteData error ->" + errMsg;
                    if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                        _sComms = null;
                    }
                    else
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                    }
                }
            }
            return 0;
        }
        /// <summary>
        /// If the 3rd party DCS supports single stepping then send the single step command, otherwise unfreeze and sleep for 
        /// the step time.
        /// </summary>
        /// <param name="numberOfSteps">This will always be 1</param>
        /// <returns></returns>
        public override int Step(int numberOfSteps)
        {
            if (!AdapterIsInStubMode && (_sComms != null))
            {
                // step the DCS.  If this is a DCS that does not support single stepping then we will run for the stepsize amount of time
                // if we are in freeze.  If we are running then simply return;
                if (IsFrozen)
                {
                    Unfreeze();
                    int sleepTime = Convert.ToInt32(AdapterStepSize * 1000);
                    LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Sleep time is {sleepTime}ms, Speed = {_adapterSpeed}");
                    if (_adapterSpeed >= 1.0)
                    {
                        sleepTime = (int)(sleepTime / _adapterSpeed); // Sleep for the step size based on current speed.
                    }
                    else
                    {
                        sleepTime = (int)(sleepTime * _adapterSpeed); // If slow time, sleep for the desired speed (i.e. 1/2 speed = 1/2 the step size, 1/10 speed = 1/10 the step size)
                    }
                    System.Threading.Thread.Sleep(sleepTime);
                    Freeze();
                }
                // The above logic does not apply if this supports single stepping.  If single stepping is supported
                // then issue the single step command here
            }
            return 0;
        }
        /// <summary>
        /// This is called after initialization when all of the I/O has been defined.  In this method we do the mapping to the 
        /// 3rd party system.  If we need to load a snapshot after initialization then we will do that here.
        /// </summary>
        /// <returns></returns>
        public override int FinishSetup()
        {
            // Set the Finished Initalization flag and allow the alive thread to start receiving packets
            _finishedInitialization = true;
            _skipAliveProcessing = false;

            return 0;
        }
        /// <summary>
        /// Send a freeze command to the 3rd party system
        /// </summary>
        /// <returns></returns>
        public override int Freeze()
        {
            string errMsg = string.Empty;
            string errMsg2 = string.Empty;
            SppaComms.SppaCommandStatus status;
            bool logAsInfo = false;

            if (_sComms != null)
            {
                status = _sComms.SppaFreeze(CurrentSimulationTime, ref errMsg);
                if (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    switch (status)
                    {
                        case SppaComms.SppaCommandStatus.FREEZE_PRJ_NOT_OPEN:
                            errMsg = "Freeze: No project version has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.FREEZE_PRJ_OFFLINE:
                            errMsg = "Freeze: Project version was opened offline.";
                            break;
                        case SppaComms.SppaCommandStatus.FREEZE_SIM_NOT_OPEN:
                            errMsg = "Freeze: No simulation has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.FREEZE_SIM_NOT_IN_RUN:
                            errMsg = "Freeze: Simulation is not in RUN state.";
                            logAsInfo = true;
                            break;
                        case SppaComms.SppaCommandStatus.FREEZE_COULDNT_FINISH:
                            errMsg = "Freeze: Error during command completion.";
                            break;
                        case SppaComms.SppaCommandStatus.FREEZE_UNFINISHED_CMD:
                            errMsg = "Freeze: A previous command hasn't been completed yet.";
                            break;
                        case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                            errMsg2 = errMsg;
                            errMsg = $"Freeze: {errMsg}";
                            break;
                        case SppaComms.SppaCommandStatus.SocketUnkownError:
                            errMsg2 = errMsg;
                            errMsg = $"Freeze: {errMsg}";
                            break;
                        default:
                            errMsg = $"Freeze: Unkown error code: {status.ToString()}.";
                            break;
                    }
                    if (logAsInfo)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Info, errMsg);
                    }
                    else
                    {
                        if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                            _sComms = null;
                        }
                        else
                        {
                            //_sComms.LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Severe, errMsg);
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                        }
                    }
                }
                else
                {
                }
            }

            // If we are initializing then suppress the alive thread
            if (_finishedInitialization)
            {
                _skipAliveProcessing = false;
            }
            _stopReadWrite = false;
            return base.Freeze();
        }
        /// <summary>
        /// Send a run command to the 3rd party system
        /// </summary>
        /// <returns></returns>
        public override int Unfreeze()
        {
            string errMsg = string.Empty;
            string errMsg2 = string.Empty;
            SppaComms.SppaCommandStatus status;
            bool logAsInfo = false;

            _stopReadWrite = true;
            base.Unfreeze();

            if (_adapterSpeed != 1.0F)
            {
                _calledFromRun = true;
                SpeedFactor = _adapterSpeed;
                return 0;
            }

            // We only need to send the run command if we are in real time
            if (_sComms != null)
            {
                status = _sComms.SppaRun(CurrentSimulationTime, ref errMsg);
                if (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    switch (status)
                    {
                        case SppaComms.SppaCommandStatus.RUN_PRJ_NOT_OPEN:
                            errMsg = "Run: No project version has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.RUN_PRJ_OFFLINE:
                            errMsg = "Run: Project version was opened offline.";
                            break;
                        case SppaComms.SppaCommandStatus.RUN_SIM_NOT_OPEN:
                            errMsg = "Run: No simulation has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.RUN_SIM_NOT_IN_FREEZE:
                            errMsg = "Run: Simulation is not in FREEZE state.";
                            logAsInfo = true;
                            break;
                        case SppaComms.SppaCommandStatus.RUN_COULDNT_FINISH:
                            errMsg = "Run: Error during command completion.";
                            break;
                        case SppaComms.SppaCommandStatus.RUN_UNFINISHED_CMD:
                            errMsg = "Run: A previous command hasn't been completed yet.";
                            break;
                        case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                            errMsg2 = errMsg;
                            errMsg = $"Run: {errMsg}";
                            break;
                        case SppaComms.SppaCommandStatus.SocketUnkownError:
                            errMsg2 = errMsg;
                            errMsg = $"Run: {errMsg}";
                            break;
                        default:
                            errMsg = $"Run: Unkown error code: {status.ToString()}.";
                            break;
                    }
                    if (logAsInfo)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Info, errMsg);
                    }
                    else
                    {
                        if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                            _sComms = null;
                        }
                        else
                        {
                            //_sComms.LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Severe, errMsg);
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                        }
                    }
                }
                else
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "Run completed successfully");
                }
            }

            // Start the Alive thread 
            _skipAliveProcessing = true;

            return 0;
        }
        public override double CurrentSimulationTime 
        { 
            get => base.CurrentSimulationTime;
            set
            {
                if (_sComms != null)
                {
                    _sComms.CurrentTime = value;
                }
                base.CurrentSimulationTime = value;
            }
        }
        /// <summary>
        /// Change the execution speed on the 3rd party system.  Only use this override if you are passing
        /// down a new speed to the 3rd party system.  Since the adapters use the SpeedFactor then make sure to
        /// update that value at the same time.
        /// </summary>
        public override float SpeedFactor
        {
            get { return base.SpeedFactor; }
            set
            {
                float speedFactor = value;
                bool wasRunning = !IsFrozen;
                UInt16 iSpeed;
                string errMsg = string.Empty;
                string errMsg2 = string.Empty;
                SppaComms.SppaCommandStatus status;

                speedFactor = value;

                if (_sComms == null)
                {
                    return;
                }
                // If we are in freeze then just store the factor and it will be applied when we run again
                if (wasRunning && !_calledFromRun)
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, "Speed: wasRunning = true");
                    Freeze();
                }
                if (value == 1.0F)
                {
                    _adapterSpeed = 1.0F;
                    if (wasRunning && !_calledFromRun)
                    {
                        Unfreeze();
                    }
                    return;
                }
                //else if (!_UseSpeedToRun)
                //{
                //    _UseSpeedToRun = true;
                //    _adapterSpeed = value;
                //    return;
                //}

                if (value == -1.0F)
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.Info, "Commanded speed was NO_WAIT.  Clamping at 4x.");
                    speedFactor = 4.0F;
                }

                if (speedFactor < 1.0F)
                {
                    if (speedFactor < 0.1F)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, "Commanded speed cannot be less than 1/100X.  Clamping at 1/100X");
                        speedFactor = 0.1F;
                    }
                    else if (speedFactor > 0.5F)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, "Commanded speed cannot be greater than 1/2X.  Clamping at 1/2X");
                        speedFactor = 0.5F;
                    }
                    iSpeed = (UInt16)(1.0F / speedFactor);

                    status = _sComms.SppaSlowTime(CurrentSimulationTime, iSpeed, ref errMsg);
                    if (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                    {
                        switch (status)
                        {
                            case SppaComms.SppaCommandStatus.SLOWMODE_INVALID_uintVal:
                                errMsg = "SlowMode: No project version has been opened.";
                                break;
                            case SppaComms.SppaCommandStatus.SLOWMODE_PRJ_OFFLINE:
                                errMsg = "SlowMode: Project version was opened offline.";
                                break;
                            case SppaComms.SppaCommandStatus.SLOWMODE_PRJ_NOT_OPEN:
                                errMsg = "SlowMode: No simulation has been opened.";
                                break;
                            case SppaComms.SppaCommandStatus.SLOWMODE_SIM_NOT_OPEN:
                                errMsg = "SlowMode: Simulation is not in FREEZE state.";
                                break;
                            case SppaComms.SppaCommandStatus.SLOWMODE_REPLAYING:
                                errMsg = "SlowMode: Project version was opened offline.";
                                break;
                            case SppaComms.SppaCommandStatus.SLOWMODE_SIM_NOT_IN_FREEZE:
                                errMsg = "SlowMode: No simulation has been opened.";
                                break;
                            case SppaComms.SppaCommandStatus.SLOWMODE_COULDNT_FINISH:
                                errMsg = "SlowMode: Error during command completion.";
                                break;
                            case SppaComms.SppaCommandStatus.SLOWMODE_UNFINISHED_CMD:
                                errMsg = "SlowMode: A previous command hasn't been completed yet.";
                                break;
                            case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                                errMsg2 = errMsg;
                                errMsg = $"SlowMode: {errMsg}";
                                break;
                            case SppaComms.SppaCommandStatus.SocketUnkownError:
                                errMsg2 = errMsg;
                                errMsg = $"SlowMode: {errMsg}";
                                break;
                            default:
                                errMsg = $"SlowMode: Unkown error code: {status.ToString()}.";
                                break;
                        }
                        if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                            _sComms = null;
                        }
                        else
                        {
                            //_sComms.LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Severe, errMsg);
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                        }
                    }
                    else
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "Run completed successfully");
                    }
                }
                else if (speedFactor > 1.0F)
                {
                    if (speedFactor > 4.0F)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, "Commanded speed cannot be greater than 4X.  Clamping at 4X");
                        speedFactor = 4.0F;
                    }
                    iSpeed = (UInt16)speedFactor;
                    status = _sComms.SppaFastTime(CurrentSimulationTime, iSpeed, ref errMsg);
                    if (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                    {
                        switch (status)
                        {
                            case SppaComms.SppaCommandStatus.FASTMODE_INVALID_uintVal:
                                errMsg = "FastMode: No project version has been opened.";
                                break;
                            case SppaComms.SppaCommandStatus.FASTMODE_PRJ_OFFLINE:
                                errMsg = "FastMode: Project version was opened offline.";
                                break;
                            case SppaComms.SppaCommandStatus.FASTMODE_PRJ_NOT_OPEN:
                                errMsg = "FastMode: No simulation has been opened.";
                                break;
                            case SppaComms.SppaCommandStatus.FASTMODE_SIM_NOT_OPEN:
                                errMsg = "FastMode: Simulation is not in FREEZE state.";
                                break;
                            case SppaComms.SppaCommandStatus.FASTMODE_REPLAYING:
                                errMsg = "FastMode: Project version was opened offline.";
                                break;
                            case SppaComms.SppaCommandStatus.FASTMODE_SIM_NOT_IN_FREEZE:
                                errMsg = "FastMode: No simulation has been opened.";
                                break;
                            case SppaComms.SppaCommandStatus.FASTMODE_COULDNT_FINISH:
                                errMsg = "FastMode: Error during command completion.";
                                break;
                            case SppaComms.SppaCommandStatus.FASTMODE_UNFINISHED_CMD:
                                errMsg = "FastMode: A previous command hasn't been completed yet.";
                                break;
                            case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                                errMsg2 = errMsg;
                                errMsg = $"FastMode: {errMsg}";
                                break;
                            case SppaComms.SppaCommandStatus.SocketUnkownError:
                                errMsg2 = errMsg;
                                errMsg = $"FastMode: {errMsg}";
                                break;
                            default:
                                errMsg = $"FastMode: Unkown error code: {status.ToString()}.";
                                break;
                        }
                        if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                            _sComms = null;
                        }
                        else
                        {
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                        }
                    }
                    else
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "Run completed successfully");
                    }
                }

                _adapterSpeed = speedFactor;
                if (!_UseSpeedToRun)
                {
                    _UseSpeedToRun = true;
                    _adapterSpeed = value;
                }
                if (wasRunning && !_calledFromRun)
                {
                    Unfreeze();
                }

                base.SpeedFactor = value;
            }
        }
        /// <summary>
        /// Save a snapshot on the 3rd party system, unless we need to save locally
        /// </summary>
        /// <param name="snapshotName">The full path and name of the snapshot</param>
        /// <param name="snapshotType">The snapshot type (sic - IC, aic - Overlay, bkt - Backtrack)</param>
        /// <returns></returns>
        public override int SaveSnapshot(string snapshotName, eGenericFrameworkAdapterSnapshotType snapshotType)
        {
            string tmpSnapName = string.Empty;
            string siemensSnapshotName;
            string errMsg = string.Empty;
            string errMsg2 = string.Empty;
            SppaComms.SppaCommandStatus status;

            base.SaveSnapshot(snapshotName, snapshotType);
            _snapshotOperationInProgress = true;
            Thread.Sleep(2000);

            PrepareSnapshotName(snapshotName, snapshotType, ref tmpSnapName);

            if (_sComms != null)
            {
                siemensSnapshotName = tmpSnapName;
                status = _sComms.SppaSaveSnapshot(siemensSnapshotName, snapshotType, CurrentSimulationTime, ref errMsg);
                if (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    switch (status)
                    {
                        case SppaComms.SppaCommandStatus.SAVESNAP_INVALID_NAME:
                            errMsg = $"SaveIC: Name of IC file is invalid.  Snapshot name = {siemensSnapshotName}.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_INVALID_EXTENSION:
                            errMsg = $"SaveIC: File extension is neither BC nor IC.  Snapshot name = {siemensSnapshotName}.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_PRJ_NOT_OPEN:
                            errMsg = "SaveIC: No project version has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_PRJ_OFFLINE:
                            errMsg = "SaveIC: Project version was opened offline.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_SIM_NOT_OPEN:
                            errMsg = "SaveIC: No simulation has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_COULDNT_CREATE_FILE:
                            errMsg = "SaveIC: IC file couldn’t be created. E.g. IC file with extension  IC already exists.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_COULDNT_WRITE_FILE:
                            errMsg = "SaveIC: IC file couldn’t be written.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_COULDNT_DELETE_FILE:
                            errMsg = "SaveIC: IC file with extension BC couldn't be deleted.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_COULDNT_FINISH:
                            errMsg = "SaveIC: Error during command completion.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_UNFINISHED_CMD:
                            errMsg = "SaveIC: A previous command hasn't been completed yet.";
                            break;
                        case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                            errMsg2 = errMsg;
                            errMsg = $"SaveIC: {errMsg}";
                            break;
                        case SppaComms.SppaCommandStatus.SocketUnkownError:
                            errMsg2 = errMsg;
                            errMsg = $"SaveIC: {errMsg}";
                            break;
                        default:
                            errMsg = $"SaveIC: Unkown error code: {status.ToString()}.";
                            break;
                    }
                    if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                        _sComms = null;
                    }
                    else
                    {
                        //_sComms.LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Severe, errMsg);
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                    }
                }
                else
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "SaveIC completed successfully");
                }
            }
            _snapshotOperationInProgress = false;

            return 1;
        }
        public string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.-]+", "", RegexOptions.Compiled);
        }
        private void PrepareSnapshotName(string snapshotName, eGenericFrameworkAdapterSnapshotType snapshotType, ref string tmpSnapName)
        {
            // First strip off the path
            int index = snapshotName.LastIndexOf('\\');
            tmpSnapName = snapshotName.Substring(index + 1);

            _snapshotOperationInProgress = true;
            tmpSnapName = RemoveSpecialCharacters(tmpSnapName);


            // Form the snapshot name into the format required by Siemens
            if (snapshotType == eGenericFrameworkAdapterSnapshotType.Backtrack)
            {
                tmpSnapName = tmpSnapName.Replace("-", "_");
                tmpSnapName = tmpSnapName.Replace(".", "_");
                // Now add the extension
                tmpSnapName += ".BC";
            }
            else if (snapshotType == eGenericFrameworkAdapterSnapshotType.Overlay)
            {
                tmpSnapName = tmpSnapName.Replace("-", "_");
                tmpSnapName = tmpSnapName.Replace(".", "_");
                // Now add the extension
                tmpSnapName += ".IC";
            }
            else
            {
                tmpSnapName = tmpSnapName.Replace("-", "_");
                tmpSnapName = tmpSnapName.Replace(".", "_");
                // Now add the extension
                tmpSnapName += ".IC";
            }

            _snapshotOperationInProgress = false;
        }

        /// <summary>
        /// Restore a snapshot on the 3rd party system, unless we need to restore locally
        /// </summary>
        /// <param name="snapshotName">The full path and name of the snapshot</param>
        /// <param name="snapshotType">The snapshot type (sic - IC, aic - Overlay, bkt - Backtrack)</param>
        /// <returns></returns>
        public override int RestoreSnapshot(string snapshotName, eGenericFrameworkAdapterSnapshotType snapshotType)
        {
            string tmpSnapName = string.Empty;
            string siemensSnapshotName;
            string errMsg = string.Empty;
            string errMsg2 = string.Empty;
            SppaComms.SppaCommandStatus status;

            _snapshotOperationInProgress = true;
            Thread.Sleep(2000);

            if (_sComms != null)
            {
                PrepareSnapshotName(snapshotName, snapshotType, ref tmpSnapName);

                siemensSnapshotName = tmpSnapName;
                status = _sComms.SppaLoadSnapshot(siemensSnapshotName, snapshotType, CurrentSimulationTime, ref errMsg);
                if (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    switch (status)
                    {
                        case SppaComms.SppaCommandStatus.LOADIC_INVALID_NAME:
                            errMsg = $"LoadIC: Name of IC file is invalid.  Snapshot name = {siemensSnapshotName}.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_INVALID_EXTENSION:
                            errMsg = $"LoadIC: File extension is neither BC nor IC.  Snapshot name = {siemensSnapshotName}.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_PRJ_NOT_OPEN:
                            errMsg = "LoadIC: No project version has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_SIM_NOT_OPEN:
                            errMsg = "LoadIC: No simulation has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_SIM_NOT_IN_FREEZE:
                            errMsg = "LoadIC: Simulation is not in FREEZE state.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_OBJ_NOT_FOUND:
                            errMsg = "LoadIC: IC file wasn’t found.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_COULDNT_READ_FILE:
                            errMsg = "LoadIC: IC file couldn’t be read.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_FILE_INCONSISTENT:
                            errMsg = "LoadIC: Content of IC file doesn’t match to expected data format.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_IC_MAINTENANCE_REQUIRED:
                            errMsg = "LoadIC: IC Maintenance is required.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_COULDNT_FINISH:
                            errMsg = "LoadIC: Error during command completion.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_UNFINISHED_CMD:
                            errMsg = "LoadIC: A previous command hasn't been completed yet.";
                            break;
                        case SppaComms.SppaCommandStatus.LOADIC_NO_LICENSE:
                            errMsg = "LoadIC: License for command execution is missing.";
                            break;
                        case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                            errMsg2 = errMsg;
                            errMsg = $"LoadIC: {errMsg}";
                            break;
                        case SppaComms.SppaCommandStatus.SocketUnkownError:
                            errMsg2 = errMsg;
                            errMsg = $"LoadIC: {errMsg}";
                            break;
                        default:
                            errMsg = $"LoadIC: Unkown error code: {status.ToString()}.";
                            break;
                    }
                    if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                        _sComms = null;
                    }
                    else
                    {
                        //_sComms.LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Severe, errMsg);
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                    }
                }
                else
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "LoadIC completed successfully");
                }
            }
            _snapshotOperationInProgress = false;


            return base.RestoreSnapshot(snapshotName, snapshotType);
        }
        /// <summary>
        /// Delete a snapshot on the 3rd party system.  We also delete our local copy
        /// </summary>
        /// <param name="snapshotName">The full path and name of the snapshot</param>
        /// <param name="snapshotType">The snapshot type (sic - IC, aic - Overlay, bkt - Backtrack)</param>
        /// <returns></returns>
        public override int DeleteSnapshot(string snapshotName, eGenericFrameworkAdapterSnapshotType snapshotType)
        {
            string tmpSnapName = string.Empty;
            string siemensSnapshotName;
            string errMsg = string.Empty;
            string errMsg2 = string.Empty;
            SppaComms.SppaCommandStatus status;

            _snapshotOperationInProgress = true;
            Thread.Sleep(2000);

            if (_sComms != null)
            {
                PrepareSnapshotName(snapshotName, snapshotType, ref tmpSnapName);

                siemensSnapshotName = tmpSnapName;
                status = _sComms.SppaDeleteSnapshot(siemensSnapshotName, snapshotType, CurrentSimulationTime, ref errMsg);
                if (status != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    switch (status)
                    {
                        case SppaComms.SppaCommandStatus.DELFILE_INVALID_NAME:
                            errMsg = $"DeleteIC: Name of IC file is invalid.  Snapshot name = {siemensSnapshotName}.";
                            break;
                        case SppaComms.SppaCommandStatus.DELFILE_INVALID_EXTENSION:
                            errMsg = $"DeleteIC: File extension is neither BC nor IC.  Snapshot name = {siemensSnapshotName}.";
                            break;
                        case SppaComms.SppaCommandStatus.DELFILE_PRJ_NOT_OPEN:
                            errMsg = "DeleteIC: No project version has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.DELFILE_COULDNT_DELETE_FILE:
                            errMsg = "DeleteIC: IC file with extension BC couldn't be deleted.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_COULDNT_FINISH:
                            errMsg = "DeleteIC: Error during command completion.";
                            break;
                        case SppaComms.SppaCommandStatus.SAVESNAP_UNFINISHED_CMD:
                            errMsg = "DeleteIC: A previous command hasn't been completed yet.";
                            break;
                        case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                            errMsg2 = errMsg;
                            errMsg = $"DeleteIC: {errMsg}";
                            break;
                        case SppaComms.SppaCommandStatus.SocketUnkownError:
                            errMsg2 = errMsg;
                            errMsg = $"DeleteIC: {errMsg}";
                            break;
                        default:
                            errMsg = $"DeleteIC: Unkown error code: {status.ToString()}.";
                            break;
                    }
                    if (status == SppaComms.SppaCommandStatus.SocketUnkownError)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                        _sComms = null;
                    }
                    else
                    {
                        //_sComms.LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Severe, errMsg);
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                    }
                }
                else
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "DeleteIC completed successfully");
                }
            }
            _snapshotOperationInProgress = false;
            // Let the base class delete our local copy
            return base.DeleteSnapshot(snapshotName, snapshotType);
        }
        public override int CloseAdapter()
        {
            string errMsg = string.Empty;
            string errMsg2 = string.Empty;
            SppaComms.SppaCommandStatus cmdStatus;

            // Shutdown the threads
            _shutdownAliveThread = true;
            _shutdownTimeSyncThread = true;
            _shutdownReadWriteThread = true;

            // Sleep for 2 steps to allow current threads to clean up
            Thread.Sleep((int)(AdapterStepSize * 2000));
            if (_aliveThread != null)
            {
                _aliveThread.Abort();
            }
            if (_syncTimeThread != null)
            {
                _syncTimeThread.Abort();
            }
            if (_readWriteThread != null)
            {
                _readWriteThread.Abort();
            }

            if (_sComms != null)
            {
                // Open the project file
                cmdStatus = _sComms.SppaCloseSimulation(0.0, ref errMsg);
                if (cmdStatus != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    switch (cmdStatus)
                    {
                        case SppaComms.SppaCommandStatus.CLOSESIM_PRJ_NOT_OPEN:
                            errMsg = "Close_Simulation: No project version has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.CLOSESIM_SIM_NOT_OPEN:
                            errMsg = "Close_Simulation: No simulation has been opened.";
                            break;
                        case SppaComms.SppaCommandStatus.CLOSESIM_COULDNT_FINISH:
                            errMsg = "Close_Simulation: Error during command completion.";
                            break;
                        case SppaComms.SppaCommandStatus.CLOSESIM_UNFINISHED_CMD:
                            errMsg = "Close_Simulation: Project wasn’t found.";
                            break;
                        case SppaComms.SppaCommandStatus.OPENSIM_COULDNT_FINISH:
                            errMsg = "Close_Simulation: A previous command hasn't been completed yet.";
                            break;
                        case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                            errMsg2 = errMsg;
                            errMsg = $"Close_Simulation: {errMsg}";
                            break;
                        case SppaComms.SppaCommandStatus.SocketUnkownError:
                            errMsg2 = errMsg;
                            errMsg = $"Close_Simulation: {errMsg}";
                            break;
                        default:
                            errMsg = $"Close_Simulation: Unkown error code: {cmdStatus.ToString()}.";
                            break;
                    }
                    if (cmdStatus == SppaComms.SppaCommandStatus.SocketUnkownError)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                        _sComms = null;
                    }
                    else
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                    }
                }
                else
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "CloseSimulation completed successfully");
                }

                // Open the project file
                cmdStatus = _sComms.SppaCloseProject(0.0, ref errMsg);
                if (cmdStatus != SppaComms.SppaCommandStatus.ANYCMD_NO_ERROR)
                {
                    switch (cmdStatus)
                    {
                        case SppaComms.SppaCommandStatus.CLOSEPRJ_PRJ_NOT_OPEN:
                            errMsg = "Close_Project: Project name is invalid.";
                            break;
                        case SppaComms.SppaCommandStatus.CLOSEPRJ_COULDNT_FINISH:
                            errMsg = "Close_Project: Error during command completion.";
                            break;
                        case SppaComms.SppaCommandStatus.CLOSEPRJ_UNFINISHED_CMD:
                            errMsg = "Close_Project: A previous command hasn't been completed yet.";
                            break;
                        case SppaComms.SppaCommandStatus.NotEnoughBytesSent:
                            errMsg2 = errMsg;
                            errMsg = $"Close_Project: {errMsg}";
                            break;
                        case SppaComms.SppaCommandStatus.SocketUnkownError:
                            errMsg2 = errMsg;
                            errMsg = $"Close_Project: {errMsg}";
                            break;
                        default:
                            errMsg = $"Close_Project: Unkown error code: {cmdStatus.ToString()}.";
                            break;
                    }
                    if (cmdStatus == SppaComms.SppaCommandStatus.SocketUnkownError)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Fatal, errMsg);
                        _sComms = null;
                    }
                    else
                    {
                        //_sComms.LogCommsMessage(eGenericFrameworkAdapterErrorLevel.Severe, errMsg);
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, errMsg);
                    }
                }
                else
                {
                    LogMessage(eGenericFrameworkAdapterErrorLevel.FullDebug, "CloseProject completed successfully");
                }
            }
            _sComms = null;
            return base.CloseAdapter();
        }
        public override int AddReadTags(List<GenericFrameworkAdapterTag> tags)
        {
            string tagName;
            int outSlotIndex;
            string tagSlotName;
            int slotIndex = 0;
            int tagType;
            string type;
            int typeIndex;

            base.AddReadTags(tags);

            if (_sComms != null && !AdapterIsInStubMode && (_sComms.EmuDefinition != null) && (tags.Count > 0))
            {
                // Loop through the list to make sure the tags are valid.  We will only add the valid tags
                for (int index = 0; index < tags.Count; index++)
                {
                    tagName = tags[index].TagName;
                    outSlotIndex = tagName.LastIndexOf('C');
                    slotIndex = 0;

                    // If this fully qualified name is found add it to the I/O list
                    if (_sComms.EmuDefinition.OutputApRackSlots.Contains(tagName))
                    {
                        // Verify the channel and tag type
                        tagSlotName = tagName.Substring(0, outSlotIndex);
                        typeIndex = tagName.Length - 1;
                        type = tagName.Substring(typeIndex);
                        tagType = Convert.ToInt32(type);
                        foreach (SppaSlotHeader sh in _sComms.EmuDefinition.OutputSlotHeaders)
                        {
                            if (tagSlotName == sh.SlotName)
                            {
                                if ((int)sh.ChannelType == tagType)
                                {
                                    _readSlotIndices.Add(slotIndex);
                                    _readTags.Add(tagName);
                                    break;
                                }
                            }
                            slotIndex++;
                        }
                    }
                    else
                    {
                        // The project team have stated that they want to read from the input channels for SCADA tags.  This is not supported by Siemens so we need
                        // implement a backdoor to get around this.
                        if (_sComms.EmuDefinition.InputApRackSlots.Contains(tagName))
                        {
                            int tagIndex = 18;
                            int channel = Convert.ToInt32(tagName.Substring(tagIndex, 2));
                            tagIndex = 21;
                            int ioType = Convert.ToInt32(tagName.Substring(tagIndex, 1));

                            tagSlotName = tagName.Substring(0, outSlotIndex);
                            typeIndex = tagName.Length - 1;
                            type = tagName.Substring(typeIndex);
                            tagType = Convert.ToInt32(type);
                            foreach (SppaSlotHeader sh in _sComms.EmuDefinition.InputSlotHeaders)
                            {
                                if (tagSlotName == sh.SlotName)
                                {
                                    if ((int)sh.ChannelType == tagType)
                                    {
                                        SppaSlotIODetails ioPoint = new SppaSlotIODetails();
                                        ioPoint.SlotIndex = slotIndex;
                                        ioPoint.ChannelIndex = channel;
                                        ioPoint.ChannelType = ioType;
                                        _readInputSlotDetails.Add(tagName, ioPoint);
                                        break;
                                    }
                                }
                                slotIndex++;
                            }
                        }
                        else
                        {
                            _invalidReadIndices.Add(index);
                            LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Invalid read tag name found: {tagName}. The tag name format is PCnnAPnnnRnnnSnnnCnnTn.\nPlease look at the HWConfig.txt file  under the \"Out:\" sectionto determine the proper tag name.");
                        }
                    }
                }
                if (_readTags.Count > 0)
                {
                    _sComms.DefineReadTags(_readTags, _readSlotIndices, _aoConversionInfo);
                }
            }
            else
            {
                if (!AdapterIsInStubMode)
                {
                    if (_sComms.EmuDefinition == null)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Severe, "Failed to parse HWConfig.dat.  Check the file name and path in the .dcsAdapter file.  No read tags added");
                    }
                }
            }
            return 0;
        }
        public override int AddWriteTags(List<GenericFrameworkAdapterTag> tags)
        {
            string tagName;
            int inSlotIndex;
            string tagSlotName;
            int slotIndex = 0;
            int tagType;
            string type;
            int typeIndex;

            base.AddWriteTags(tags);

            if (_sComms != null && !AdapterIsInStubMode && (_sComms.EmuDefinition != null) && (tags.Count > 0))
            {
                // Loop through the list to make sure the tags are valid.  We will only add the valid tags
                for (int index = 0; index < tags.Count; index++)
                {
                    tagName = tags[index].TagName;
                    inSlotIndex = tagName.LastIndexOf('C');
                    slotIndex = 0;

                    // If this fully qualified name is found add it to the I/O list
                    if (_sComms.EmuDefinition.InputApRackSlots.Contains(tagName))
                    {
                        // Verify the channel and tag type
                        tagSlotName = tagName.Substring(0, inSlotIndex);
                        typeIndex = tagName.Length - 1;
                        type = tagName.Substring(typeIndex);
                        tagType = Convert.ToInt32(type);
                        foreach (SppaSlotHeader sh in _sComms.EmuDefinition.InputSlotHeaders)
                        {
                            if (tagSlotName == sh.SlotName)
                            {
                                if ((int)sh.ChannelType == tagType)
                                {
                                    _writeSlotIndices.Add(slotIndex);
                                    _writeTags.Add(tagName);
                                    break;
                                }
                            }
                            slotIndex++;
                        }
                    }
                    else
                    {
                        _invalidWriteIndices.Add(index);
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Warning, $"Invalid write tag name found: {tagName}. The tag name format is PCnnAPnnnRnnnSnnnCnnTn.\nPlease look at the HWConfig.txt file under the \"In:\" section to determine the proper tag name.");
                    }
                }
                if (_writeSlotIndices.Count > 0)
                {
                    _sComms.DefineWriteTags(_writeTags, _writeSlotIndices, _aiConversionInfo);
                }
            }
            else
            {
                if (!AdapterIsInStubMode)
                {
                    if (_sComms.EmuDefinition == null)
                    {
                        LogMessage(eGenericFrameworkAdapterErrorLevel.Severe, "Failed to parse HWConfig.dat.  Check the file name and path in the .dcsAdapter file.  No write tags added");
                    }
                }
            }
            return 0;
        }
        #endregion
    }
}
