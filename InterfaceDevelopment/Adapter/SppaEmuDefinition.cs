﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericFrameworkAdapterInterface
{
    class SppaEmuDefinition
    {
        public UInt32 PCCount { get; set; }
        public List<SppaPCHeader> InputPCHeaders = new List<SppaPCHeader>();
        public List<SppaAPHeader> InputApHeaders = new List<SppaAPHeader>();
        public List<SppaRackHeader> InputRackHeaders = new List<SppaRackHeader>();
        public List<SppaSlotHeader> InputSlotHeaders = new List<SppaSlotHeader>();
        public List<SppaPCHeader> OutputPCHeaders = new List<SppaPCHeader>();
        public List<SppaAPHeader> OutputApHeaders = new List<SppaAPHeader>();
        public List<SppaRackHeader> OutputRackHeaders = new List<SppaRackHeader>();
        public List<SppaSlotHeader> OutputSlotHeaders = new List<SppaSlotHeader>();
        public List<UInt32> InputApNumbers = new List<uint>();
        public List<UInt32> InputRackNumbers = new List<uint>();
        public List<UInt32> InputSlotNumbers = new List<uint>();
        public List<UInt32> OutputApNumbers = new List<uint>();
        public List<UInt32> OutputRackNumbers = new List<uint>();
        public List<UInt32> OutputSlotNumbers = new List<uint>();
        public List<string> InputApRackSlots = new List<string>();
        public List<string> OutputApRackSlots = new List<string>();
        /// <summary>
        /// Constructor
        /// </summary>
        public SppaEmuDefinition()
        {
        }
    }
}
