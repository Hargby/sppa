﻿/////////////////////////////////////////////////////////////////////////////
//
//							  COPYRIGHT (c) 2020
//								HONEYWELL LTD.
//							  ALL RIGHTS RESERVED
//
//  This software is a copyrighted work and/or information protected as a
//  trade secret. Legal rights of Honeywell Ltd. in this software is distinct
//  from ownership of any medium in which the software is embodied. Copyright
//  or trade secret notices included must be reproduced in any copies
//  authorised by Honeywell Ltd.
//
/////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Windows.Forms;
using GenericFrameworkAdapterInterface;

namespace GenericFrameworkAdapter_Driver
{
    public partial class GenericFrameworkAdapterDriver : Form
    {
        private List<GenericFrameworkAdapterTag> _getList = new List<GenericFrameworkAdapterTag>();
        private List<GenericFrameworkAdapterTag> _setList = new List<GenericFrameworkAdapterTag>();
        private bool _dllLoaded = false;
        private bool _isFrozen = true;
        private bool _isRunning = true;
        private bool _snapshotNamePopulated = false;
        private bool _dllNamePopulated = false;
        private bool _dcsAdapterLoaded = false;
        private IGenericFrameworkAdapterAPI _myAdapter = null;
        private string _adapterName = string.Empty;
        private bool _snapshotPathPopulated = true;
        private Random _randomizer = new Random();
        private Timer _timer = new Timer();
        private TimeSpan _simulationTime = new TimeSpan(0);
        private float _stepSize;
        private List<KeyValuePair<string, string>> _configParams = null;

        public delegate void LogAdapterMEssage(string message, eGenericFrameworkAdapterErrorLevel severity);
        /// <summary>
        /// Initialize the driver
        /// </summary>
        public GenericFrameworkAdapterDriver()
        {
            InitializeComponent();
            _timer.Interval = 1000;
            _timer.Tick += new EventHandler(_timer_Tick);
            // Load up in real time
            _cmbSimulationSpeed.SelectedIndex = 2;
            _bnSnapshotFile.Enabled = false;
            _txtSnapshotName.Enabled = false;
            _stepSize = 1.0F;
            SetSensitivity();
        }
        /// <summary>
        /// Timer event that fires every second, or faster/slower depending on selected speed
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _timer_Tick(object sender, EventArgs e)
        {
            EventHandler invoker = new EventHandler(_bnSingleStep_Click);
            invoker.Invoke(_bnSingleStep, e);
        }
        /// <summary>
        /// Update the text box with the current simulation time
        /// </summary>
        private void UpdateSimulationTime()
        {
            _txtSimulationTime.Text = TimeSpanToSimTime(_simulationTime);
        }
        /// <summary>
        /// The bindings must be handled on the UI thread.  This generic method is used 
        /// by various items to handle a binding refresh so the values are properly 
        /// displayed to the user.
        /// </summary>
        private void ResetGridBindings()
        {
            _bindingSourceGet.ResetBindings(false);
            _bindingSourceSet.ResetBindings(false);
        }
        /// <summary>
        /// Called when the user selected the .DcsAdapter Browse button
        /// </summary>
        /// <remarks>
        /// .DcsAdapter files are used as input by UniSim Operations to define the 
        /// adapter.  For the COM Bridge, this XML file contains the computer name 
        /// and the ProgID of the COM object that is to be instantiated.
        /// </remarks>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnBrosweForDll_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _dllBrowser.ShowDialog())
            {
                _txtProjectDll.Text = _dllBrowser.FileName;
                _dllNamePopulated = true;
            }
        }
        /// <summary>
        /// Called when the user selected the .DcsAdapter Browse button
        /// </summary>
        /// <remarks>
        /// .DcsAdapter files are used as input by UniSim Operations to define the 
        /// adapter.  For the COM Bridge, this XML file contains the computer name 
        /// and the ProgID of the COM object that is to be instantiated.
        /// </remarks>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnDcsAdapterFile_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _adapterBrowser.ShowDialog())
            {
                _txtDcsAdapterFile.Text = _adapterBrowser.FileName;
                _dcsAdapterLoaded = true;
                ParseDcsAdapterFile(_adapterBrowser.FileName, out _adapterName, out _configParams);
            }
        }
        /// <summary>
        /// Called when the user selected the .DcsAdapter Browse button
        /// </summary>
        /// <remarks>
        /// .DcsAdapter files are used as input by UniSim Operations to define the 
        /// adapter.  For the COM Bridge, this XML file contains the computer name 
        /// and the ProgID of the COM object that is to be instantiated.
        /// </remarks>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _cmbSimulationSpeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_myAdapter != null)
            {
                float speed = Convert.ToSingle(_cmbSimulationSpeed.Text);
                _myAdapter.SpeedFactor = speed;
                //Change the timer interval to match the new simulation speed
                _timer.Interval = Convert.ToInt32(1000.0 / speed);
            }
        }
        /// <summary>
        /// Called when the user selected the .DcsAdapter Browse button
        /// </summary>
        /// <remarks>
        /// .DcsAdapter files are used as input by UniSim Operations to define the 
        /// adapter.
        /// </remarks>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnRefFileBrowser_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _refFileBrowser.ShowDialog())
            {
                _getList.Clear();
                _setList.Clear();
                _txtRefFile.Text = _refFileBrowser.FileName;
                ParseRefFile(_refFileBrowser.FileName);

                // After the lists are populated clear the bindings and redo them with the updated lists
                _bindingSourceSet.DataSource = null;
                _bindingSourceSet.DataSource = _setList;
                _bindingSourceGet.DataSource = null;
                _bindingSourceGet.DataSource = _getList;
            }
        }
        /// <summary>
        /// Parse the supplied ref file to populate the dummy I/O points for testing
        /// </summary>
        /// <param name="fileName">The ref file being parsed</param>
        private void ParseRefFile(string fileName)
        {
            //This reads in a UniSim Operations .REF file which contains the 
            //inter-model connection information for gets and puts
            using (StreamReader sr = new StreamReader(fileName))
            {
                //Read the header line
                string line = sr.ReadLine();

                while (line != null)
                {
                    ParseRefFileLine(line);
                    line = sr.ReadLine();
                }
            }
        }
        /// <summary>
        /// Parses a .REF file line
        /// </summary>
        /// <param name="refFileLine">The line to parse</param>
        public void ParseRefFileLine(string refFileLine)
        {
            refFileLine = refFileLine.TrimStart(' ', '\t');

            //Check to see if it is a blank line or a commented line
            if (refFileLine.Length == 0 || refFileLine[0] == '#')
            {
                return;
            }

            // Split the line based on spaces and tabs
            List<string> tokenizedRefFileLine = new List<string>(refFileLine.Split(' ', '\t'));
            for (int i = 0; i < tokenizedRefFileLine.Count; i++)
            {
                if (tokenizedRefFileLine[i].Length == 0)
                {
                    tokenizedRefFileLine.RemoveAt(i);
                    i--;
                }
            }

            // Set the type
            eGenericFrameworkAdapterTagType tagType = (tokenizedRefFileLine[1].ToUpper() == "F" ? eGenericFrameworkAdapterTagType.FloatType : eGenericFrameworkAdapterTagType.IntegerType);

            int tokenIndex = 2;     //Local tagname starts on column 2 (0 based)
            string destinationName;
            string sourceName;

            GenericFrameworkAdapterTag getItem = null;
            GenericFrameworkAdapterTag setItem = null;
            string directionLowercase = tokenizedRefFileLine[0].ToLower();

            // If this is a bi-directional point add to both lists
            if (directionLowercase.Contains("rw") || directionLowercase.Contains("wr"))
            {
                //It is both read and write!
                if (_chkLocalPoints.Checked)
                {
                    destinationName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                    sourceName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                }
                else
                {
                    sourceName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                    destinationName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                }

                // Create a new adapter I/O tag based on the data type
                if (tagType == eGenericFrameworkAdapterTagType.FloatType)
                {
                    getItem = new GenericFrameworkAdapterTag(sourceName, tagType, (_randomizer.NextDouble() * 1000.0F));
                    setItem = new GenericFrameworkAdapterTag(destinationName, tagType, _randomizer.NextDouble() * 1000.0F);
                }
                else
                {
                    getItem = new GenericFrameworkAdapterTag(sourceName, tagType, (_randomizer.Next() * 1000));
                    setItem = new GenericFrameworkAdapterTag(destinationName, tagType, _randomizer.Next() * 1000);
                }
            }
            else if (directionLowercase.Contains("r"))
            {
                //An "r" means that the "remote model" is writing to the "local model".
                if (_chkLocalPoints.Checked)
                {
                    sourceName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                    destinationName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                }
                else
                {
                    destinationName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                    sourceName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                }
                getItem = new GenericFrameworkAdapterTag(sourceName, tagType, _randomizer.NextDouble());
            }
            else if (directionLowercase.Contains("w"))
            {
                //An "w" means that the "local model" is writing to the "remote model".
                if (_chkLocalPoints.Checked)
                {
                    destinationName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                    sourceName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                }
                else
                {
                    sourceName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                    destinationName = GetTagnameFromRefFileLine(tokenizedRefFileLine, ref tokenIndex);
                }
                setItem = new GenericFrameworkAdapterTag(destinationName, tagType, _randomizer.NextDouble());
            }

            if (null != getItem) _getList.Add(getItem);
            if (null != setItem) _setList.Add(setItem);
        }
        /// <summary>
        /// Reads in a tag name from the .REF file line
        /// </summary>
        /// <remarks>
        /// Typically the tagname and the parameter will contain no spaces, however this is not
        /// guaranteed.  If the tagname or parameter has a space in it, then we expect the 
        /// tag.parameter to be encased in quotes.  i.e. "My Tag.My Parameter".  This method
        /// checks to see if the first character of the tagname is a quote.  If so, the method
        /// will continue to loop until it finds an end quote - denoting the end of the tag.param.
        /// </remarks>
        /// <param name="tokenizedRefFileLine">List containing all the individual line numbers</param>
        /// <param name="index">The index where the tag.parameter begins.</param>
        /// <returns>
        /// The tag.parameter (without the quotes)
        /// </returns>
        public string GetTagnameFromRefFileLine(List<string> tokenizedRefFileLine, ref int index)
        {
            string retVal = tokenizedRefFileLine[index++];

            //If the tag.parameter starts with a quote, then we want to find the end quote.
            if (retVal[0] == '"')
            {
                while (index < tokenizedRefFileLine.Count && !retVal.EndsWith("\""))
                {
                    retVal = retVal + " ";  //Put in the "space" character that was the delimiter in the tag name.
                    retVal = retVal + tokenizedRefFileLine[index++];
                }

                //Get rid of the quote characters since that isn't part of the tag name.
                retVal = retVal.Trim('"');
            }

            return retVal;
        }
        /// <summary>
        /// Called when the user selected the .DcsAdapter Browse button
        /// </summary>
        /// <remarks>
        /// .DcsAdapter files are used as input by UniSim Operations to define the 
        /// adapter.  For the COM Bridge, this XML file contains the computer name 
        /// and the ProgID of the COM object that is to be instantiated.
        /// </remarks>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnSnapshotFile_Click(object sender, EventArgs e)
        {
            if (_snapshotPathPopulated)
            {
                _snapshotFileBrowser.InitialDirectory = _txtSnapshotPath.Text;
                if (DialogResult.OK == _snapshotFileBrowser.ShowDialog())
                {
                    _txtSnapshotName.Text = _snapshotFileBrowser.SafeFileName;
                    _snapshotNamePopulated = true;
                    SetSensitivity();
                }
            }
        }
        /// <summary>
        /// This call will load the dll in the same manner as is done in the actual adapter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _bnLoadDll_Click(object sender, EventArgs e)
        {
            // Create the config parameters lists
            List<string> tokens = new List<string>();
            List<string> vals = new List<string>();

            if (_dllNamePopulated && _dcsAdapterLoaded)
            {
                // First load the dll
                Assembly myAssembly = null;
                try
                {
                    myAssembly = Assembly.LoadFrom(_txtProjectDll.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show(string.Format("'{0}' could not be found.  Install the interface dll before running again.", _txtProjectDll.Text));
                    return;
                }

                // Get the type and make sure that the assembly interface is of type IGenericFrameworkAdapterAPI
                Type[] types;
                try
                {
                    types = myAssembly.GetTypes();
                }

                // GetTypes fails if there's anything wrong with your interface. The error messages report why
                // the method failed - e.g. not implementing required interfaces ..
                catch (ReflectionTypeLoadException rlex)
                {
                    foreach (Exception ex in rlex.LoaderExceptions)
                    {
                        MessageBox.Show("GenericFrameworkAdapterDriver: Assembly Loader Exception", ex.Message);
                    }
                    types = rlex.Types;
                }
                for (int i = 0; i < types.Length; i++)
                {
                    if (types[i] != null && types[i].Name == _adapterName)
                    {
                        _myAdapter = (IGenericFrameworkAdapterAPI)myAssembly.CreateInstance(types[i].FullName);
                        break;
                    }
                }

                if (_myAdapter == null)
                {
                    string errorMessage = String.Format("Cannot create instance of Adapter '{0}'. (No corresponding class found in {1})", _adapterName, _txtProjectDll.Text);
                    MessageBox.Show(errorMessage);
                    return;
                }

                _dllLoaded = true;

                // Now fo through the initialization process
                _myAdapter.SpeedFactor = Convert.ToSingle(_cmbSimulationSpeed.Text);
                _myAdapter.MessageLogged += new EventHandler(MessageLoggedToUI);
                _myAdapter.OperatorAction += new EventHandler(OperatorActionCapture);
                _myAdapter.AdapterStepSize = _stepSize;

                foreach (KeyValuePair<string, string> thisTag in _configParams)
                {
                    tokens.Add(thisTag.Key);
                    vals.Add(thisTag.Value);
                }
                _myAdapter.ParseConfigurationData(tokens.ToArray(), vals.ToArray());
                _myAdapter.Freeze();
            }
            else
            {
                if (!_dllNamePopulated)
                {
                    MessageBox.Show("Specify an adapter dll before loading");
                }
                else
                {
                    MessageBox.Show("Specify a .dcsAdapter file before loading");
                }
            }
            SetSensitivity();
        }

        private void OperatorActionCapture(object sender, EventArgs e)
        {
            string msg;
            OperatorActionEventArgs oea = e as OperatorActionEventArgs;

            if (oea != null)
            {
                if (oea.oldval != null)
                {
                    if (oea.description != "")
                    {
                        msg = string.Format("OperatorActionCapture: Operator changed {0} from {1} to to {2}.  Description = {3}\n", oea.tagName, oea.oldval, oea.newval, oea.description);
                    }
                    else
                    {
                        msg = string.Format("OperatorActionCapture: Operator changed {0} from {1} to {2}\n", oea.tagName, oea.oldval, oea.newval);
                    }
                }
                else
                {
                    if (oea.description != "")
                    {
                        msg = string.Format("OperatorActionCapture: Operator changed {0} to {1}.  Description = {2}\n", oea.tagName, oea.newval, oea.description);
                    }
                    else
                    {
                        msg = string.Format("OperatorActionCapture: Operator changed {0} to {1}\n", oea.tagName, oea.newval);
                    }
                }
                _txtMessages.Text += msg;
            }
        }

        /// <summary>
        /// Get the LogMessage from the adapter and then display it in the text box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageLoggedToUI(object sender, EventArgs e)
        {
            string msg;

            if (InvokeRequired)
            {
                BeginInvoke(new EventHandler(MessageLoggedToUI), new object[] { sender, e });
                return;
            }

            GenericFrameworkAdapterEventArgs mea = e as GenericFrameworkAdapterEventArgs;
            if (mea != null)
            {
                switch (mea.severity)
                {
                    case eGenericFrameworkAdapterErrorLevel.Warning:
                        msg = $"Warning: {mea.message}";
                        _txtMessages.Text += msg;
                        break;
                    case eGenericFrameworkAdapterErrorLevel.Fatal:
                        msg = $"Fatal: {mea.message}";
                        _txtMessages.Text += msg;
                        break;
                    case eGenericFrameworkAdapterErrorLevel.Severe:
                        msg = $"Severe: {mea.message}";
                        _txtMessages.Text += msg;
                        break;
                    case eGenericFrameworkAdapterErrorLevel.Info:
                        msg = $"Info: {mea.message}";
                        _txtMessages.Text += msg;
                        break;
                    case eGenericFrameworkAdapterErrorLevel.DebugInfo:
                        msg = $"DebugInfo: {mea.message}";
                        _txtMessages.Text += msg;
                        break;
                    case eGenericFrameworkAdapterErrorLevel.FullDebug:
                        msg = $"FullDebug: {mea.message}";
                        _txtMessages.Text += msg;
                        break;
                    case eGenericFrameworkAdapterErrorLevel.Debug:
                        msg = $"Debug: {mea.message}";
                        _txtMessages.Text += msg;
                        break;
                    default:
                        break;
                }
                _txtMessages.Select(_txtMessages.Text.Length - 1, 0);
            }
        }

        /// <summary>
        /// Parses a .DcsAdapter file
        /// </summary>
        /// <remarks>
        /// Extracts the important information from the .DcsAdapter file
        /// </remarks>
        /// <param name="configFileName">The full file name to parse</param>
        /// <param name="adapterName">The adapter name that is contained in the file</param>
        /// <param name="configParams">The list of key-value items in the xml</param>
        /// <returns>
        /// true if the file was parsed
        /// false if the file could not be found or read
        /// </returns>
        private bool ParseDcsAdapterFile(string configFileName, out string adapterName, out List<KeyValuePair<string, string>> configParams)
        {
            bool fileRead = false;
            configParams = new List<KeyValuePair<string, string>>();
            adapterName = "";

            using (XmlTextReader reader = new XmlTextReader(configFileName))
            {
                fileRead = true;
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (!(reader.LocalName.ToUpper().Equals("ADAPTER") || reader.LocalName.ToUpper().Equals("PROPERTIES")))
                        {
                            if (reader.LocalName.ToUpper().Equals("ADAPTERNAME"))
                            {
                                // We want to skip this since it is not relevant
                            }
                            else if (reader.LocalName.ToUpper().Equals("PROJECT_SPECIFIC_ADAPTER"))
                            {
                                // Since this is a fully qualified name, take off the path and extension

                                adapterName = reader.ReadString();
                                string tmpName = adapterName.Substring(adapterName.LastIndexOf('\\') + 1);
                                adapterName = tmpName.Substring(0, tmpName.ToUpper().IndexOf(".DLL"));
                            }
                            // Get the step size for the UI and pass it to the adapter as well
                            else if (reader.LocalName.ToUpper().Equals("STEPSIZE"))
                            {
                                string value = reader.ReadString();
                                _stepSize = Convert.ToSingle(value);
                                KeyValuePair<string, string> pair = new KeyValuePair<string, string>(reader.LocalName, value);
                                configParams.Add(pair);
                            }
                            else
                            {
                                string value = reader.ReadString();
                                KeyValuePair<string, string> pair = new KeyValuePair<string, string>(reader.LocalName, value);
                                configParams.Add(pair);
                            }
                        }
                    }
                }
            }
            return fileRead;
        }
        /// <summary>
        /// Enable or disable buttons based on the current states
        /// </summary>
        private void SetSensitivity()
        {
            if (_dllLoaded)
            {
                _bnLoadDll.Enabled = false;
                _bnFinishInit.Enabled = true;
                _bnDcsAdapterFile.Enabled = false;
            }
            else
            {
                if (_txtProjectDll.Text != "")
                {
                    _bnLoadDll.Enabled = true;
                }
                else
                {
                    _bnLoadDll.Enabled = false;
                }
                _bnFinishInit.Enabled = false;
                _bnDcsAdapterFile.Enabled = true;
            }

            if (_isFrozen && _dllLoaded)
            {
                _bnRun.Enabled = true;
                _bnFreeze.Enabled = false;
                _bnSingleStep.Enabled = true;
                _bnGetData.Enabled = true;
                _bnSetData.Enabled = true;
                _cmbSimulationSpeed.Enabled = true;
            }
            else if (_isRunning && _dllLoaded)
            {
                _bnRun.Enabled = false;
                _bnFreeze.Enabled = true;
                _bnSingleStep.Enabled = false;
                _bnGetData.Enabled = false;
                _bnSetData.Enabled = false;
                _cmbSimulationSpeed.Enabled = false;
            }
            else
            {
                _bnRun.Enabled = false;
                _bnFreeze.Enabled = false;
                _bnSingleStep.Enabled = false;
                _bnGetData.Enabled = false;
                _bnSetData.Enabled = false;
            }

            if (_snapshotNamePopulated && _dllLoaded && _isFrozen)
            {
                _bnLoadSnapshot.Enabled = true;
                _bnDeleteSnapshot.Enabled = true;
                _bnSaveSnapshot.Enabled = true;
            }
            else
            {
                _bnLoadSnapshot.Enabled = false;
                _bnDeleteSnapshot.Enabled = false;
                _bnSaveSnapshot.Enabled = false;
            }
        }
        /// <summary>
        /// Send a Freeze command to the adapter
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnFreeze_Click(object sender, EventArgs e)
        {
            if (_myAdapter != null)
            {
                _myAdapter.Freeze();
            }
            _isFrozen = true;
            _isRunning = false;
            _timer.Stop();
            SetSensitivity();
        }
        /// <summary>
        /// Send a Unfreeze command to the adapter
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnRun_Click(object sender, EventArgs e)
        {
            if (_myAdapter != null)
            {
                _myAdapter.Unfreeze();
            }
            _isRunning = true;
            _isFrozen = false;
            _timer.Start();
            SetSensitivity();
        }
        /// <summary>
        /// Send a step command to the adapter
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnSingleStep_Click(object sender, EventArgs e)
        {
            if (_myAdapter != null)
            {
                // First write values to the adapter
                WriteValuesToAdapter();
                System.Threading.Thread.Sleep(100);

                // Now step
                _myAdapter.Step(1);
                System.Threading.Thread.Sleep(100);

                // finally read from the adapter
                ReadValuesFromAdapter();
                System.Threading.Thread.Sleep(100);

                // Update the simulation time
                _simulationTime = _simulationTime.Add(TimeSpan.FromMilliseconds(_stepSize * 1000));
                UpdateSimulationTime();

            }
        }
        /// <summary>
        /// Read the values from the adapter and update the data grid with the new values
        /// </summary>
        private void ReadValuesFromAdapter()
        {
            ArrayList values = new ArrayList();
            int index = 0;

            if (_getList.Count > 0)
            {
                values.Clear();
                values = null;
                _myAdapter.ReadFromAdapter(out values);
                if ((values != null) && (values.Count > 0))
                {
                    foreach (GenericFrameworkAdapterTag tag in _getList)
                    {
                        if (tag.TagType == eGenericFrameworkAdapterTagType.IntegerType)
                        {
                            tag.IntegerValue = Convert.ToInt32(values[index]);
                            // Also set the Float value for Data Grid display
                            tag.FloatValue = Convert.ToSingle(values[index]);
                        }
                        else
                        {
                            tag.FloatValue = Convert.ToSingle(values[index]);
                        }
                        index++;
                    }
                }
                int grid1Index = dataGridView1.FirstDisplayedScrollingRowIndex;
                dataGridView1.Refresh();
                dataGridView1.FirstDisplayedScrollingRowIndex = grid1Index;
            }
        }
        /// <summary>
        /// Write the dummy values to the adapter
        /// </summary>
        private void WriteValuesToAdapter()
        {
            ArrayList values = new ArrayList();

            if (_setList.Count > 0)
            {
                foreach (GenericFrameworkAdapterTag tag in _setList)
                {
                    // Increment the values to have "live" data.
                    if (tag.TagType == eGenericFrameworkAdapterTagType.IntegerType)
                    {
                        tag.IntegerValue = _randomizer.Next();
                        // Also set the Float value for Data Grid display
                        tag.FloatValue = Convert.ToSingle(tag.IntegerValue);
                    }
                    else
                    {
                        tag.FloatValue = Convert.ToSingle(_randomizer.NextDouble() * 100.0F);
                    }
                    values.Add(tag.TagType == eGenericFrameworkAdapterTagType.IntegerType ? tag.IntegerValue : tag.FloatValue);
                }
                _myAdapter.WriteToAdapter(values);
                int grid2Index = dataGridView2.FirstDisplayedScrollingRowIndex;
                dataGridView2.Refresh();
                dataGridView2.FirstDisplayedScrollingRowIndex = grid2Index;
            }
        }
        /// <summary>
        /// Send a Save Snapshot command to the adapter
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnSaveSnapshot_Click(object sender, EventArgs e)
        {
            string fileName = $"{_txtSnapshotPath.Text}\\{_txtSnapshotName.Text}";
            string extension = fileName.Substring(fileName.LastIndexOf(".") + 1).ToUpper();
            eGenericFrameworkAdapterSnapshotType snapType = eGenericFrameworkAdapterSnapshotType.InitialCondition;

            if (extension.Equals("SIC"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.InitialCondition;
            }
            else if (extension.Equals("AIC"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.Overlay;
            }
            else if (extension.Equals("BKT"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.Backtrack;
            }

            if (_myAdapter != null)
            {
                _myAdapter.SaveSnapshot(fileName, snapType);
            }
        }
        /// <summary>
        /// Send a Restore Snapshot command to the adapter
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnLoadSnapshot_Click(object sender, EventArgs e)
        {
            _simulationTime = TimeSpan.Zero;
            UpdateSimulationTime();

            string fileName = $"{_txtSnapshotPath.Text}\\{_txtSnapshotName.Text}";
            string extension = fileName.Substring(fileName.LastIndexOf(".") + 1).ToUpper();
            eGenericFrameworkAdapterSnapshotType snapType = eGenericFrameworkAdapterSnapshotType.InitialCondition;

            if (extension.Equals("SIC"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.InitialCondition;
            }
            else if (extension.Equals("AIC"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.Overlay;
            }
            else if (extension.Equals("BKT"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.Backtrack;
            }

            if (_myAdapter != null)
            {
                _myAdapter.RestoreSnapshot(fileName, snapType);
                _myAdapter.CurrentSimulationTime = _simulationTime.TotalMilliseconds;
            }
        }
        /// <summary>
        /// Send a Delete Snapshot command to the adapter
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnDeleteSnapshot_Click(object sender, EventArgs e)
        {
            string fileName = $"{_txtSnapshotPath.Text}\\{_txtSnapshotName.Text}";
            string extension = fileName.Substring(fileName.LastIndexOf(".") + 1).ToUpper();
            eGenericFrameworkAdapterSnapshotType snapType = eGenericFrameworkAdapterSnapshotType.InitialCondition;

            if (extension.Equals("SIC"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.InitialCondition;
            }
            else if (extension.Equals("AIC"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.Overlay;
            }
            else if (extension.Equals("BKT"))
            {
                snapType = eGenericFrameworkAdapterSnapshotType.Backtrack;
            }

            if (_myAdapter != null)
            {
                _myAdapter.DeleteSnapshot(fileName, snapType);
            }
        }
        /// <summary>
        /// Takes a TimeSpan and converts it into a string of format 999:59:59.999
        /// </summary>
        /// <remarks>
        /// The time span is converted to a string.  Any days are multiplied by 24 hours
        /// so that the returned string shows totalhours:min:sec.  If the timespan has
        /// milliseconds, the milliseconds will be appended, otherwise the string is 
        /// returned without any milliseconds.
        /// </remarks>
        /// <param name="ts">The TimeSpan to convert.</param>
        /// <returns>a string in the form of 999:59:59.999</returns>
        private string TimeSpanToSimTime(TimeSpan ts)
        {
            int hours = (ts.Days * 24) + ts.Hours;
            string simTime = string.Format("{0}:{1}:{2}", hours.ToString("00"), ts.Minutes.ToString("00"), ts.Seconds.ToString("00"));
            if (ts.Milliseconds > 0)
            {
                double msec = (double)ts.Milliseconds / 1000.0;
                simTime += msec.ToString(".000");
            }

            return simTime;
        }
        /// <summary>
        /// Get values from the adapter
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnGetData_Click(object sender, EventArgs e)
        {
            ReadValuesFromAdapter();
        }
        /// <summary>
        /// Send values to the adapter
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnSetData_Click(object sender, EventArgs e)
        {
            WriteValuesToAdapter();
        }
        /// <summary>
        /// Set the snapshot path so that the user only has to type in names in the snapshot name textbox
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnSnapshotPath_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _snapshotPathBrowser.ShowDialog())
            {
                _txtSnapshotPath.Text = _snapshotPathBrowser.SelectedPath;
                _bnSnapshotFile.Enabled = true;
                _txtSnapshotName.Enabled = true;
                _snapshotPathPopulated = true;
            }
        }
        /// <summary>
        /// Send a AddReadTags / AddWriteTags / updateSimulationTime / FinishSetup to mimic what happens
        /// during a load of the initial session in USO
        /// </summary>
        /// <param name="sender">The object making the call</param>
        /// <param name="e">The arguments of this event</param>
        private void _bnFinishInit_Click(object sender, EventArgs e)
        {
            // Setup the read group
            if ((_myAdapter != null) && (_getList.Count > 0))
            {
                _myAdapter.AddReadTags(_getList);
            }
            // Setup the write group
            if ((_myAdapter != null) && (_setList.Count > 0))
            {
                _myAdapter.AddWriteTags(_setList);
            }

            _simulationTime = TimeSpan.Zero;
            UpdateSimulationTime();

            // Finally final setup
            _myAdapter.FinishSetup();
            _bnFinishInit.Enabled = false;
        }
        /// <summary>
        /// Close the session and do cleanup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _bnCloseModel_Click(object sender, EventArgs e)
        {
            if (_myAdapter != null)
            {
                _myAdapter.CloseAdapter();
                _myAdapter.MessageLogged -= MessageLoggedToUI;
                _myAdapter = null;
            }
            System.Windows.Forms.Application.Exit();
        }
        /// <summary>
        /// Catch the closing of the form to clean up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GenericFrameworkAdapterDriver_FormClosing(object sender, FormClosingEventArgs e)
        {
            EventHandler invoker = new EventHandler(_bnCloseModel_Click);
            invoker.Invoke(_bnCloseModel, e);
        }

        private void _txtProjectDll_TextChanged(object sender, EventArgs e)
        {
            SetSensitivity();
        }

        private void _txtSnapshotName_TextChanged(object sender, EventArgs e)
        {
            _snapshotNamePopulated = true;
            SetSensitivity();
        }
    }
}
