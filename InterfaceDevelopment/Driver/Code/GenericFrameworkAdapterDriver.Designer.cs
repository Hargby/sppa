﻿namespace GenericFrameworkAdapter_Driver
{
    partial class GenericFrameworkAdapterDriver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._txtProjectDll = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._bnLoadDll = new System.Windows.Forms.Button();
            this._bnBrosweForDll = new System.Windows.Forms.Button();
            this._adapterBrowser = new System.Windows.Forms.OpenFileDialog();
            this._bnDcsAdapterFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this._txtDcsAdapterFile = new System.Windows.Forms.TextBox();
            this._dllBrowser = new System.Windows.Forms.OpenFileDialog();
            this._txtSnapshotPath = new System.Windows.Forms.TextBox();
            this._bnDeleteSnapshot = new System.Windows.Forms.Button();
            this._bnLoadSnapshot = new System.Windows.Forms.Button();
            this._bnSaveSnapshot = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._bnSnapshotPath = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this._txtSnapshotName = new System.Windows.Forms.TextBox();
            this._bnSnapshotFile = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._bnCloseModel = new System.Windows.Forms.Button();
            this._bnFinishInit = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._txtSimulationTime = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this._bnSingleStep = new System.Windows.Forms.Button();
            this._cmbSimulationSpeed = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this._bnRun = new System.Windows.Forms.Button();
            this._bnFreeze = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._bnSetData = new System.Windows.Forms.Button();
            this._bnGetData = new System.Windows.Forms.Button();
            this._chkLocalPoints = new System.Windows.Forms.CheckBox();
            this._bnRefFileBrowser = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this._txtRefFile = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tagNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tagTypeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.integerValueDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.floatValueDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._bindingSourceGet = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tagNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tagTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.integerValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.floatValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._bindingSourceSet = new System.Windows.Forms.BindingSource(this.components);
            this._refFileBrowser = new System.Windows.Forms.OpenFileDialog();
            this._snapshotFileBrowser = new System.Windows.Forms.OpenFileDialog();
            this._snapshotPathBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._txtMessages = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSourceGet)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSourceSet)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // _txtProjectDll
            // 
            this._txtProjectDll.Location = new System.Drawing.Point(6, 38);
            this._txtProjectDll.Name = "_txtProjectDll";
            this._txtProjectDll.ReadOnly = true;
            this._txtProjectDll.Size = new System.Drawing.Size(252, 20);
            this._txtProjectDll.TabIndex = 0;
            this._txtProjectDll.TextChanged += new System.EventHandler(this._txtProjectDll_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Adapter to load";
            // 
            // _bnLoadDll
            // 
            this._bnLoadDll.Location = new System.Drawing.Point(7, 109);
            this._bnLoadDll.Name = "_bnLoadDll";
            this._bnLoadDll.Size = new System.Drawing.Size(54, 23);
            this._bnLoadDll.TabIndex = 2;
            this._bnLoadDll.Text = "Load Dll";
            this._bnLoadDll.UseVisualStyleBackColor = true;
            this._bnLoadDll.Click += new System.EventHandler(this._bnLoadDll_Click);
            // 
            // _bnBrosweForDll
            // 
            this._bnBrosweForDll.Location = new System.Drawing.Point(254, 36);
            this._bnBrosweForDll.Name = "_bnBrosweForDll";
            this._bnBrosweForDll.Size = new System.Drawing.Size(25, 23);
            this._bnBrosweForDll.TabIndex = 3;
            this._bnBrosweForDll.Text = "...";
            this._bnBrosweForDll.UseVisualStyleBackColor = true;
            this._bnBrosweForDll.Click += new System.EventHandler(this._bnBrosweForDll_Click);
            // 
            // _adapterBrowser
            // 
            this._adapterBrowser.Filter = "DcsAdapter file|*.dcsAdapter";
            this._adapterBrowser.Title = "Project Team Adapter .dcsAdapter To Use";
            // 
            // _bnDcsAdapterFile
            // 
            this._bnDcsAdapterFile.Location = new System.Drawing.Point(254, 74);
            this._bnDcsAdapterFile.Name = "_bnDcsAdapterFile";
            this._bnDcsAdapterFile.Size = new System.Drawing.Size(25, 23);
            this._bnDcsAdapterFile.TabIndex = 6;
            this._bnDcsAdapterFile.Text = "...";
            this._bnDcsAdapterFile.UseVisualStyleBackColor = true;
            this._bnDcsAdapterFile.Click += new System.EventHandler(this._bnDcsAdapterFile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "dcsAdapter File";
            // 
            // _txtDcsAdapterFile
            // 
            this._txtDcsAdapterFile.Location = new System.Drawing.Point(22, 90);
            this._txtDcsAdapterFile.Name = "_txtDcsAdapterFile";
            this._txtDcsAdapterFile.ReadOnly = true;
            this._txtDcsAdapterFile.Size = new System.Drawing.Size(251, 20);
            this._txtDcsAdapterFile.TabIndex = 4;
            // 
            // _dllBrowser
            // 
            this._dllBrowser.Filter = "dll file|*.dll";
            this._dllBrowser.Title = "Project Team Adapter Dll To Use";
            // 
            // _txtSnapshotPath
            // 
            this._txtSnapshotPath.Location = new System.Drawing.Point(6, 32);
            this._txtSnapshotPath.Name = "_txtSnapshotPath";
            this._txtSnapshotPath.ReadOnly = true;
            this._txtSnapshotPath.Size = new System.Drawing.Size(202, 20);
            this._txtSnapshotPath.TabIndex = 8;
            // 
            // _bnDeleteSnapshot
            // 
            this._bnDeleteSnapshot.Location = new System.Drawing.Point(154, 103);
            this._bnDeleteSnapshot.Name = "_bnDeleteSnapshot";
            this._bnDeleteSnapshot.Size = new System.Drawing.Size(54, 23);
            this._bnDeleteSnapshot.TabIndex = 9;
            this._bnDeleteSnapshot.Text = "Delete";
            this._bnDeleteSnapshot.UseVisualStyleBackColor = true;
            this._bnDeleteSnapshot.Click += new System.EventHandler(this._bnDeleteSnapshot_Click);
            // 
            // _bnLoadSnapshot
            // 
            this._bnLoadSnapshot.Location = new System.Drawing.Point(75, 103);
            this._bnLoadSnapshot.Name = "_bnLoadSnapshot";
            this._bnLoadSnapshot.Size = new System.Drawing.Size(54, 23);
            this._bnLoadSnapshot.TabIndex = 10;
            this._bnLoadSnapshot.Text = "Restore";
            this._bnLoadSnapshot.UseVisualStyleBackColor = true;
            this._bnLoadSnapshot.Click += new System.EventHandler(this._bnLoadSnapshot_Click);
            // 
            // _bnSaveSnapshot
            // 
            this._bnSaveSnapshot.Location = new System.Drawing.Point(6, 103);
            this._bnSaveSnapshot.Name = "_bnSaveSnapshot";
            this._bnSaveSnapshot.Size = new System.Drawing.Size(54, 23);
            this._bnSaveSnapshot.TabIndex = 11;
            this._bnSaveSnapshot.Text = "Save";
            this._bnSaveSnapshot.UseVisualStyleBackColor = true;
            this._bnSaveSnapshot.Click += new System.EventHandler(this._bnSaveSnapshot_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._bnSnapshotPath);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this._txtSnapshotName);
            this.groupBox1.Controls.Add(this._bnSnapshotFile);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._txtSnapshotPath);
            this.groupBox1.Controls.Add(this._bnSaveSnapshot);
            this.groupBox1.Controls.Add(this._bnDeleteSnapshot);
            this.groupBox1.Controls.Add(this._bnLoadSnapshot);
            this.groupBox1.Location = new System.Drawing.Point(325, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 132);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Snapshot Control";
            // 
            // _bnSnapshotPath
            // 
            this._bnSnapshotPath.Location = new System.Drawing.Point(214, 30);
            this._bnSnapshotPath.Name = "_bnSnapshotPath";
            this._bnSnapshotPath.Size = new System.Drawing.Size(25, 23);
            this._bnSnapshotPath.TabIndex = 16;
            this._bnSnapshotPath.Text = "...";
            this._bnSnapshotPath.UseVisualStyleBackColor = true;
            this._bnSnapshotPath.Click += new System.EventHandler(this._bnSnapshotPath_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Snapshot Name";
            // 
            // _txtSnapshotName
            // 
            this._txtSnapshotName.Location = new System.Drawing.Point(6, 71);
            this._txtSnapshotName.Name = "_txtSnapshotName";
            this._txtSnapshotName.Size = new System.Drawing.Size(202, 20);
            this._txtSnapshotName.TabIndex = 14;
            this._txtSnapshotName.TextChanged += new System.EventHandler(this._txtSnapshotName_TextChanged);
            // 
            // _bnSnapshotFile
            // 
            this._bnSnapshotFile.Location = new System.Drawing.Point(214, 69);
            this._bnSnapshotFile.Name = "_bnSnapshotFile";
            this._bnSnapshotFile.Size = new System.Drawing.Size(25, 23);
            this._bnSnapshotFile.TabIndex = 5;
            this._bnSnapshotFile.Text = "...";
            this._bnSnapshotFile.UseVisualStyleBackColor = true;
            this._bnSnapshotFile.Click += new System.EventHandler(this._bnSnapshotFile_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Snapshot Path";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._bnCloseModel);
            this.groupBox2.Controls.Add(this._bnFinishInit);
            this.groupBox2.Controls.Add(this._txtProjectDll);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this._bnLoadDll);
            this.groupBox2.Controls.Add(this._bnBrosweForDll);
            this.groupBox2.Controls.Add(this._bnDcsAdapterFile);
            this.groupBox2.Location = new System.Drawing.Point(15, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(301, 138);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Initialization";
            // 
            // _bnCloseModel
            // 
            this._bnCloseModel.Location = new System.Drawing.Point(204, 109);
            this._bnCloseModel.Name = "_bnCloseModel";
            this._bnCloseModel.Size = new System.Drawing.Size(54, 23);
            this._bnCloseModel.TabIndex = 7;
            this._bnCloseModel.Text = "Close Model";
            this._bnCloseModel.UseVisualStyleBackColor = true;
            this._bnCloseModel.Click += new System.EventHandler(this._bnCloseModel_Click);
            // 
            // _bnFinishInit
            // 
            this._bnFinishInit.Location = new System.Drawing.Point(76, 109);
            this._bnFinishInit.Name = "_bnFinishInit";
            this._bnFinishInit.Size = new System.Drawing.Size(112, 23);
            this._bnFinishInit.TabIndex = 4;
            this._bnFinishInit.Text = "Finish Initialization";
            this._bnFinishInit.UseVisualStyleBackColor = true;
            this._bnFinishInit.Click += new System.EventHandler(this._bnFinishInit_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._txtSimulationTime);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._bnSingleStep);
            this.groupBox3.Controls.Add(this._cmbSimulationSpeed);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this._bnRun);
            this.groupBox3.Controls.Add(this._bnFreeze);
            this.groupBox3.Location = new System.Drawing.Point(15, 151);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(308, 166);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Simulation Control";
            // 
            // _txtSimulationTime
            // 
            this._txtSimulationTime.Location = new System.Drawing.Point(116, 115);
            this._txtSimulationTime.Name = "_txtSimulationTime";
            this._txtSimulationTime.Size = new System.Drawing.Size(121, 20);
            this._txtSimulationTime.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Simulation Time";
            // 
            // _bnSingleStep
            // 
            this._bnSingleStep.Location = new System.Drawing.Point(189, 19);
            this._bnSingleStep.Name = "_bnSingleStep";
            this._bnSingleStep.Size = new System.Drawing.Size(80, 23);
            this._bnSingleStep.TabIndex = 21;
            this._bnSingleStep.Text = "Single Step";
            this._bnSingleStep.UseVisualStyleBackColor = true;
            this._bnSingleStep.Click += new System.EventHandler(this._bnSingleStep_Click);
            // 
            // _cmbSimulationSpeed
            // 
            this._cmbSimulationSpeed.FormattingEnabled = true;
            this._cmbSimulationSpeed.Items.AddRange(new object[] {
            "0.25",
            "0.5",
            "1",
            "2",
            "3",
            "4",
            "5",
            "10"});
            this._cmbSimulationSpeed.Location = new System.Drawing.Point(116, 62);
            this._cmbSimulationSpeed.Name = "_cmbSimulationSpeed";
            this._cmbSimulationSpeed.Size = new System.Drawing.Size(121, 21);
            this._cmbSimulationSpeed.TabIndex = 3;
            this._cmbSimulationSpeed.SelectedIndexChanged += new System.EventHandler(this._cmbSimulationSpeed_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Simulation Speed";
            // 
            // _bnRun
            // 
            this._bnRun.Location = new System.Drawing.Point(88, 19);
            this._bnRun.Name = "_bnRun";
            this._bnRun.Size = new System.Drawing.Size(75, 23);
            this._bnRun.TabIndex = 1;
            this._bnRun.Text = "Unfreeze";
            this._bnRun.UseVisualStyleBackColor = true;
            this._bnRun.Click += new System.EventHandler(this._bnRun_Click);
            // 
            // _bnFreeze
            // 
            this._bnFreeze.Location = new System.Drawing.Point(7, 20);
            this._bnFreeze.Name = "_bnFreeze";
            this._bnFreeze.Size = new System.Drawing.Size(75, 23);
            this._bnFreeze.TabIndex = 0;
            this._bnFreeze.Text = "Freeze";
            this._bnFreeze.UseVisualStyleBackColor = true;
            this._bnFreeze.Click += new System.EventHandler(this._bnFreeze_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._bnSetData);
            this.groupBox4.Controls.Add(this._bnGetData);
            this.groupBox4.Controls.Add(this._chkLocalPoints);
            this.groupBox4.Controls.Add(this._bnRefFileBrowser);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this._txtRefFile);
            this.groupBox4.Location = new System.Drawing.Point(652, 14);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(249, 137);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data Communications";
            // 
            // _bnSetData
            // 
            this._bnSetData.Location = new System.Drawing.Point(94, 99);
            this._bnSetData.Name = "_bnSetData";
            this._bnSetData.Size = new System.Drawing.Size(67, 23);
            this._bnSetData.TabIndex = 20;
            this._bnSetData.Text = "Set Data";
            this._bnSetData.UseVisualStyleBackColor = true;
            this._bnSetData.Click += new System.EventHandler(this._bnSetData_Click);
            // 
            // _bnGetData
            // 
            this._bnGetData.Location = new System.Drawing.Point(9, 99);
            this._bnGetData.Name = "_bnGetData";
            this._bnGetData.Size = new System.Drawing.Size(72, 23);
            this._bnGetData.TabIndex = 4;
            this._bnGetData.Text = "Get Data";
            this._bnGetData.UseVisualStyleBackColor = true;
            this._bnGetData.Click += new System.EventHandler(this._bnGetData_Click);
            // 
            // _chkLocalPoints
            // 
            this._chkLocalPoints.AutoSize = true;
            this._chkLocalPoints.Location = new System.Drawing.Point(9, 76);
            this._chkLocalPoints.Name = "_chkLocalPoints";
            this._chkLocalPoints.Size = new System.Drawing.Size(204, 17);
            this._chkLocalPoints.TabIndex = 19;
            this._chkLocalPoints.Text = "Adapter Points in the ref file are local?";
            this._chkLocalPoints.UseVisualStyleBackColor = true;
            // 
            // _bnRefFileBrowser
            // 
            this._bnRefFileBrowser.Location = new System.Drawing.Point(199, 41);
            this._bnRefFileBrowser.Name = "_bnRefFileBrowser";
            this._bnRefFileBrowser.Size = new System.Drawing.Size(25, 23);
            this._bnRefFileBrowser.TabIndex = 18;
            this._bnRefFileBrowser.Text = "...";
            this._bnRefFileBrowser.UseVisualStyleBackColor = true;
            this._bnRefFileBrowser.Click += new System.EventHandler(this._bnRefFileBrowser_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Ref file to load";
            // 
            // _txtRefFile
            // 
            this._txtRefFile.Location = new System.Drawing.Point(9, 41);
            this._txtRefFile.Name = "_txtRefFile";
            this._txtRefFile.ReadOnly = true;
            this._txtRefFile.Size = new System.Drawing.Size(193, 20);
            this._txtRefFile.TabIndex = 16;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dataGridView1);
            this.groupBox5.Location = new System.Drawing.Point(14, 323);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(460, 326);
            this.groupBox5.TabIndex = 17;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Get Values";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tagNameDataGridViewTextBoxColumn1,
            this.tagTypeDataGridViewTextBoxColumn1,
            this.integerValueDataGridViewTextBoxColumn1,
            this.floatValueDataGridViewTextBoxColumn1});
            this.dataGridView1.DataSource = this._bindingSourceGet;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 16);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(454, 307);
            this.dataGridView1.TabIndex = 0;
            // 
            // tagNameDataGridViewTextBoxColumn1
            // 
            this.tagNameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tagNameDataGridViewTextBoxColumn1.DataPropertyName = "TagName";
            this.tagNameDataGridViewTextBoxColumn1.HeaderText = "TagName";
            this.tagNameDataGridViewTextBoxColumn1.Name = "tagNameDataGridViewTextBoxColumn1";
            this.tagNameDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // tagTypeDataGridViewTextBoxColumn1
            // 
            this.tagTypeDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tagTypeDataGridViewTextBoxColumn1.DataPropertyName = "TagType";
            this.tagTypeDataGridViewTextBoxColumn1.HeaderText = "TagType";
            this.tagTypeDataGridViewTextBoxColumn1.Name = "tagTypeDataGridViewTextBoxColumn1";
            this.tagTypeDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // integerValueDataGridViewTextBoxColumn1
            // 
            this.integerValueDataGridViewTextBoxColumn1.DataPropertyName = "IntegerValue";
            this.integerValueDataGridViewTextBoxColumn1.HeaderText = "IntegerValue";
            this.integerValueDataGridViewTextBoxColumn1.Name = "integerValueDataGridViewTextBoxColumn1";
            this.integerValueDataGridViewTextBoxColumn1.ReadOnly = true;
            this.integerValueDataGridViewTextBoxColumn1.Visible = false;
            // 
            // floatValueDataGridViewTextBoxColumn1
            // 
            this.floatValueDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.floatValueDataGridViewTextBoxColumn1.DataPropertyName = "FloatValue";
            this.floatValueDataGridViewTextBoxColumn1.HeaderText = "FloatValue";
            this.floatValueDataGridViewTextBoxColumn1.Name = "floatValueDataGridViewTextBoxColumn1";
            this.floatValueDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // _bindingSourceGet
            // 
            this._bindingSourceGet.DataSource = typeof(GenericFrameworkAdapterInterface.GenericFrameworkAdapterTag);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dataGridView2);
            this.groupBox6.Location = new System.Drawing.Point(480, 323);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(460, 323);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Set Values";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tagNameDataGridViewTextBoxColumn,
            this.tagTypeDataGridViewTextBoxColumn,
            this.integerValueDataGridViewTextBoxColumn,
            this.floatValueDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this._bindingSourceSet;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 16);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(454, 304);
            this.dataGridView2.TabIndex = 0;
            // 
            // tagNameDataGridViewTextBoxColumn
            // 
            this.tagNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tagNameDataGridViewTextBoxColumn.DataPropertyName = "TagName";
            this.tagNameDataGridViewTextBoxColumn.HeaderText = "TagName";
            this.tagNameDataGridViewTextBoxColumn.Name = "tagNameDataGridViewTextBoxColumn";
            this.tagNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tagTypeDataGridViewTextBoxColumn
            // 
            this.tagTypeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tagTypeDataGridViewTextBoxColumn.DataPropertyName = "TagType";
            this.tagTypeDataGridViewTextBoxColumn.HeaderText = "TagType";
            this.tagTypeDataGridViewTextBoxColumn.Name = "tagTypeDataGridViewTextBoxColumn";
            this.tagTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // integerValueDataGridViewTextBoxColumn
            // 
            this.integerValueDataGridViewTextBoxColumn.DataPropertyName = "IntegerValue";
            this.integerValueDataGridViewTextBoxColumn.HeaderText = "IntegerValue";
            this.integerValueDataGridViewTextBoxColumn.Name = "integerValueDataGridViewTextBoxColumn";
            this.integerValueDataGridViewTextBoxColumn.ReadOnly = true;
            this.integerValueDataGridViewTextBoxColumn.Visible = false;
            // 
            // floatValueDataGridViewTextBoxColumn
            // 
            this.floatValueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.floatValueDataGridViewTextBoxColumn.DataPropertyName = "FloatValue";
            this.floatValueDataGridViewTextBoxColumn.HeaderText = "FloatValue";
            this.floatValueDataGridViewTextBoxColumn.Name = "floatValueDataGridViewTextBoxColumn";
            this.floatValueDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // _bindingSourceSet
            // 
            this._bindingSourceSet.DataSource = typeof(GenericFrameworkAdapterInterface.GenericFrameworkAdapterTag);
            // 
            // _refFileBrowser
            // 
            this._refFileBrowser.FileName = "openFileDialog1";
            this._refFileBrowser.Filter = "Ref files|*.ref";
            // 
            // _snapshotFileBrowser
            // 
            this._snapshotFileBrowser.FileName = "openFileDialog1";
            this._snapshotFileBrowser.Filter = "Snapshot Files|**.sic;*.aic;*.bkt";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._txtMessages);
            this.groupBox7.Location = new System.Drawing.Point(325, 151);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(570, 160);
            this.groupBox7.TabIndex = 19;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Logged Messages";
            // 
            // _txtMessages
            // 
            this._txtMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtMessages.Location = new System.Drawing.Point(3, 16);
            this._txtMessages.Name = "_txtMessages";
            this._txtMessages.Size = new System.Drawing.Size(564, 141);
            this._txtMessages.TabIndex = 0;
            this._txtMessages.Text = "";
            // 
            // GenericFrameworkAdapterDriver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 661);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._txtDcsAdapterFile);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "GenericFrameworkAdapterDriver";
            this.Text = "Generic Framework Adapter Driver";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GenericFrameworkAdapterDriver_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSourceGet)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSourceSet)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _txtProjectDll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _bnLoadDll;
        private System.Windows.Forms.Button _bnBrosweForDll;
        private System.Windows.Forms.OpenFileDialog _adapterBrowser;
        private System.Windows.Forms.Button _bnDcsAdapterFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _txtDcsAdapterFile;
        private System.Windows.Forms.OpenFileDialog _dllBrowser;
        private System.Windows.Forms.TextBox _txtSnapshotPath;
        private System.Windows.Forms.Button _bnDeleteSnapshot;
        private System.Windows.Forms.Button _bnLoadSnapshot;
        private System.Windows.Forms.Button _bnSaveSnapshot;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _cmbSimulationSpeed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button _bnRun;
        private System.Windows.Forms.Button _bnFreeze;
        private System.Windows.Forms.Button _bnFinishInit;
        private System.Windows.Forms.Button _bnSingleStep;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button _bnSetData;
        private System.Windows.Forms.Button _bnGetData;
        private System.Windows.Forms.CheckBox _chkLocalPoints;
        private System.Windows.Forms.Button _bnRefFileBrowser;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _txtRefFile;
        private System.Windows.Forms.BindingSource _bindingSourceGet;
        private System.Windows.Forms.BindingSource _bindingSourceSet;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button _bnSnapshotFile;
        private System.Windows.Forms.OpenFileDialog _refFileBrowser;
        private System.Windows.Forms.TextBox _txtSimulationTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox _txtSnapshotName;
        private System.Windows.Forms.Button _bnSnapshotPath;
        private System.Windows.Forms.OpenFileDialog _snapshotFileBrowser;
        private System.Windows.Forms.FolderBrowserDialog _snapshotPathBrowser;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RichTextBox _txtMessages;
        private System.Windows.Forms.Button _bnCloseModel;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagTypeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn integerValueDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn floatValueDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn integerValueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn floatValueDataGridViewTextBoxColumn;
    }
}

